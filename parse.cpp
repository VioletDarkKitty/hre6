#define __STDC_LIMIT_MACROS
#include <map>
#include <string>
#include <iostream>
#include "functions.h"
#include "globals.h"
#include <cstdlib> // ¬
#include <cstdio> //  for system() exit() etc
#include <ctype.h>
#include "parseLine.h"
#include "ExceptionWithStack.h"
#include <cstring>
#include <math.h>
#include "md5.h"
#ifdef _WIN32
    #include <windows.h>
    #include <direct.h>
#endif
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <unistd.h>
#include <algorithm>
#include "sha256.h"
#include "decimal.h"
#include <chrono>
#include <random>
using namespace dec;
#define prealDecimalCount 12
using namespace std;

int brace_level=0;
int line_number = 1;
int new_while_position=0;
string function_return_temp;
string function_return_type;
#include "parser/parser.h"

// define some consts...
const string parse_return_jump = "**CHANGE_WHILE_LOCATION**";
const string parse_return_die = "**DIE**";
const string parse_return_end = "**END**";
const string parse_return_break = "**BREAK**";
const string parse_return_continue = "**CONTINUE**";

string parse_return_states(string &parse_return, int &location)
{
    if ((parse_return==parse_return_die)||(parse_return==parse_return_end))
    {
        return parse_return;
    }
    else if (parse_return==parse_return_jump)
    {
        location=new_while_position;
    }
    return "";
}

void hotfix_27(int &brace_begin, int &brace_level, int line_number, string filename)
{
    if (brace_begin!=brace_level){
        // Checker for issue #27 (https://github.com/VioletDarkKitty/hre6/issues/27)
        parse_warn("The brace level at the beginning and end of the if block do not match!",line_number,filename);
        if (find(forcedFixes.begin(), forcedFixes.end(), 27) == forcedFixes.end()) { // element does not exist...
            cout << "Fix the incorrect brace? Run with `-f 27` to force this fix [y/N]: ";
            string ask = getInput();
            if (StringUpper(ask)=="Y")
            {
                cout << "Set the brace level to '" << brace_begin << "' from '" << brace_level << "'" << endl;
                brace_level=brace_begin;
            }
        }
        else
        {
            DisplayOnConsole("yellow","Forcing hotfix for #27");
            brace_level=brace_begin;
        }
    }
}

#define PI 3.14159265
double degreesToRadians(double x)
{
    return x * PI / 180.0;
}

string parse(string in, int &line_number, readFile &Ifile, bool runningIF=false, string filename="", bool fileIsInclude=false,
string loop_type="", const vector<string>& while_stack=vector<string>(), int while_begin=0, bool runningFunction=false)
{
    if (while_begin < 0){while_begin=0;}
    if (show_debug)
    {
        // dump the while stack...
        if (while_stack.size()!=0)
        {
            cout << "Dumping while stack..." << endl;
            cout << "Starting at " << while_begin << endl;
            for (int unsigned i=while_begin; i!=while_stack.size(); i++)
            {
                cout << "[" << i << "]: '" << while_stack[i] << "';" << endl;
            }
            cout << "Dump end." << endl;
        }
        //cout << "Parse recieved file: '" << Ifile.whatFile() << "'..." << endl;
    }
	vector<string>line = parseLine(in,line_number,filename);
	string command=line[0];
    //cout<<endl<<line_number<<" "<<__FILE__<<":" <<__LINE__;
    //DisplayOnConsole("red","<"+command+">");
	if(command.length()==0) {
		// An empty line, just output a blank line
	} else if ( (atPlace(command,0)=="!") || (atPlace(command,0)=="#") || (atPlace(command,0)==";") || (atPlace(command,0)=="/") ){
        if (atPlace(command,0)=="/")
        {
            if (command.length() > 1)
            {
                if (atPlace(command,1)!="/")
                {
                    parse_error("Error: Missing '/' character for comment line or stray comment character!",line_number,filename);
                }
            }
            else
            {
                parse_error("Error: Incomplete comment line or stray '/' character!",line_number,filename);
            }
        }
	} else if(command=="{") {
		brace_level++; // add to the brace level, default is 0
		if (show_debug){cout << "BL is now " << brace_level << endl;}
	} else if(command=="}") {
		if (brace_level-1 < 0)
		{
            parse_error("Error: Brace level mismatch, a closing brace was found before\nthe brace level was elevated from 0!",line_number,filename);
		}
        varmap.cleanScope(brace_level,line_number,filename);
		brace_level--; // remove one from the brace level.
        if (show_debug){cout << "BL is now " << brace_level << endl;}
	} else if(command=="stat") {
        parse_arg_check(line,line_number,2,command,filename);
        if (varmap.exists(line[1]))
        {
            cout << "STAT for '" << line[1] << "': " << endl
            << "Type: " << varmap.typeof_(line[1]) << endl
            << "Value: " << varmap.value(line[1]) << endl
            << "Global? " << varmap.isGlobal(line[1]) << endl
            << "Const? " << varmap.isConst(line[1]) << endl
            << "Declaration brace: " << varmap.getDeclaredBrace(line[1]) << endl
            << "Declaration information: " << varmap.getDefineLocation(line[1]) << endl;
        }
        else
        {
            parse_error("Error: '"+line[1]+"' is not a variable!",line_number,filename);
        }
    } else if(command=="destroy") {
        parse_arg_check(line,line_number,2,command,filename);
        line[1]=remove_quotes(line[1]);
        if (show_debug){
            cout << "Destroying '" << line[1] << "'" << endl;
        }

        if (varmap.exists(line[1]))
        {
            varmap.destroy(line[1],line_number,filename);
        }
        else
        {
            parse_error("Error: The variable '"+line[1]+"' does not exist and therefore cannot be destroyed!",line_number,filename);
        }
	} else if((command=="cout") || (command=="cout_l") || (command=="cout_r") || (command=="cout_rl")) {
        parse_arg_check_atleast(line,line_number,2,command,filename);
        parse_quote_check(line[1],line_number,filename);
        if ((command=="cout") || command=="cout_l")
        {
            string output(replaceAll(line[1],"\\\"","\""));
            if (line.size() > 2)
            {
                // loop to get all arguments joined together...
                string temp_string;
                varmap.expectType(output,varmap._stringI|varmap._stringIliteral,line_number,filename);
                if (varmap.exists(output))
                {
                    output=varmap.value(output);
                }
                else
                {
                    if (parse_is_quoted(output)){output=remove_quotes(output);}
                }
                temp_string=output;
                for(int current_line_pos=2; (unsigned)current_line_pos != line.size(); current_line_pos++)
                {
                    if ((current_line_pos % 2) != 0)
                    {
                        if (line[current_line_pos]!="+")
                        {
                            varmap.expectType(line[current_line_pos],varmap._stringI|varmap._stringIliteral,line_number,filename);
                            if (varmap.exists(line[current_line_pos]))
                            {
                                line[current_line_pos]=varmap.value(line[current_line_pos]);
                            }
                            else
                            {
                                if (parse_is_quoted(line[current_line_pos])){line[current_line_pos]=remove_quotes(line[current_line_pos]);}
                            }
                            temp_string+=line[current_line_pos];
                        }
                        else
                        {
                            parse_error("Error: Unexpected '+' character at argument '"+IntToString(current_line_pos)+"'!",line_number,filename);
                        }
                    }
                    else
                    {
                        //even, look for a + sign...
                        if (line[current_line_pos]!="+")
                        {
                            parse_error("Error: Expected a '+' character at argument '"+IntToString(current_line_pos)+"!",line_number,filename);
                        }
                    }
                }
                parse(command+" \""+replaceAll(temp_string,"\"","\\\"")+"\"",line_number, Ifile, false,"Internal cout parse.");
            }
            else
            {
                varmap.expectType(output,varmap._stringI|varmap._stringIliteral,line_number,filename);
                if (command=="cout")
                {
                    if (varmap.exists(output))
                    {
                        char* outC = StringToChar(varmap.value(output));
                        printf("%s\n",outC);
                        delete[] outC;
                    }
                    else
                    {
                        char* outC = StringToChar(remove_quotes(output));
                        printf("%s\n",outC);
                        delete[] outC;
                    }
                    //returnValue+="printf(\"%s\\n\","+output+");";
                }
                else
                {
                    if (varmap.exists(output))
                    {
                        char* outC = StringToChar(varmap.value(output));
                        printf("%s",outC); fflush(stdout);
                        delete[] outC;
                    }
                    else
                    {
                        char* outC = StringToChar(remove_quotes(output));
                        printf("%s",outC); fflush(stdout);
                        delete[] outC;
                    }
                    //returnValue+="printf(\"%s\","+output+"); fflush(stdout);";
                }
            }
        }
        else
        {
            if (varmap.exists(line[1]))
            {
                if (!varmap.isType(line[1],varmap._realI,line_number,filename))
                {
                    if (!varmap.isType(line[1],varmap._prealI,line_number,filename))
                    {
                        parse_error("Error: Expected '"+line[1]+"' to be a real!",line_number,filename);
                    }
                }
                cout << varmap.value(line[1]);
            }
            else
            {
                if (isNumber(line[1]))
                {
                    cout << line[1];
                }
            }

            if (command!="cout_rl")
            {
                cout << endl;
            }
        }
        cin.clear();
    } else if (command=="cret") {
        // cret
        parse_arg_check(line,line_number,1,command,filename);
        #ifdef _WIN32
            cout << '\r';
        #else
            printf("\r");
        #endif
	} else if (command=="new") {
        // new [*global?] [type] [name]
		parse_arg_check_atleast(line,line_number,3,command,filename);
        string returnValueGlobals("");

        int offset = 0;
        if (line.size() > 3)
        {
            parse_arg_check(line,line_number,4,command,filename);
            if (line[1] != "global")
            {
                parse_error("Error: Expected keyword 'global'!",line_number,filename);
            }
            offset = 1;
        }

        if (varmap.exists(line[offset+2]))
        {
            parse_error("Error: Duplicate define of '"+line[offset+2]+"'!\n"+
            "Define information: "+varmap.getDefineLocation(line[offset+2]),line_number,filename);
        }

        if (!parseIsLegalVarDeclare(line[offset+2]))
        {
            parse_error("Error: Illegal variable name '"+line[offset+2]+"'!",line_number,filename);
        }

        string defineInformation="ln: "+IntToString(line_number)+"; fn: '"+filename+"'";
        if (line[offset+1]=="string")
        {
            if ((parseHasBeginQuote(line[offset+2])) || (parseHasEndQuote(line[offset+2])))
            {
                parse_error("Error: Found a quote where a variable was expected!",line_number,filename);
            }
            if (offset!=0)
            {
                varmap.create(line[offset+2],"string","",true,brace_level,false,defineInformation);
            }
            else
            {
                varmap.create(line[offset+2],"string","",false,brace_level,false,defineInformation);
            }
        }
        else if (line[offset+1]=="real")
        {
            if ((parseHasBeginQuote(line[offset+2])) || (parseHasEndQuote(line[offset+2])))
            {
                parse_error("Error: Found a quote where a variable was expected!",line_number,filename);
            }
            if (offset!=0)
            {
                varmap.create(line[offset+2],"real","0",true,brace_level,false,defineInformation);
            }
            else
            {
                varmap.create(line[offset+2],"real","0",false,brace_level,false,defineInformation);
            }
        }
        else if (line[offset+1]=="preal")
        {
            if ((parseHasBeginQuote(line[offset+2])) || (parseHasEndQuote(line[offset+2])))
            {
                parse_error("Error: Found a quote where a variable was expected!",line_number,filename);
            }
            if (offset!=0)
            {
                varmap.create(line[offset+2],"preal","0",true,brace_level,false,defineInformation);
            }
            else
            {
                varmap.create(line[offset+2],"preal","0",false,brace_level,false,defineInformation);
            }
        }
        else if (line[offset+1]=="bool")
        {
            if ((parseHasBeginQuote(line[offset+2])) || (parseHasEndQuote(line[offset+2])))
            {
                parse_error("Error: Found a quote where a variable was expected!",line_number,filename);
            }
            if (offset!=0)
            {
                varmap.create(line[offset+2],"bool","false",true,brace_level,false,defineInformation);
            }
            else
            {
                varmap.create(line[offset+2],"bool","false",false,brace_level,false,defineInformation);
            }
        }
        else if (line[offset+1]=="file")
        {
            if ((parseHasBeginQuote(line[offset+2])) || (parseHasEndQuote(line[offset+2])))
            {
                parse_error("Error: Found a quote where a variable was expected!",line_number,filename);
            }
            if (offset!=0)
            {
                varmap.create(line[offset+2],"file",line[offset+2],true,brace_level,false,defineInformation);
            }
            else
            {
                varmap.create(line[offset+2],"file",line[offset+2],false,brace_level,false,defineInformation);
            }
            // add the file to the map...
            varmap.parse_files[line[offset+2]]=new readFile;
        }
        else if (line[offset+1]=="function")
        {
            if (offset!=0)
            {
                parse_error("Error: Functions cannot be cast as global!",line_number,filename);
            }
            if ((parseHasBeginQuote(line[offset+2])) || (parseHasEndQuote(line[offset+2])))
            {
                parse_error("Error: Found a quote where a variable was expected!",line_number,filename);
            }
            varmap.create(line[offset+2],"function",line[offset+2],true,brace_level,true,defineInformation);
            // read the next line...
            int old_brace_level;
            vector<string> new_input;
            string temp = Ifile.readLine();
            line_number++;
            new_input = parseLine(temp,line_number,filename);
            if (new_input[0]!="{")
            {
                parse_error("Error: Expected a { instead of '"+new_input[0]+"'!",line_number,filename);
            }
            else
            {
                old_brace_level=brace_level;
                brace_level++;
            }
            // loop until the end of the function is reached...
            varmap.parse_functions_add(line[offset+2],"{");
            varmap.parse_functions_setInfo(line[offset+2],line_number,filename);
            while(old_brace_level!=brace_level)
            {
                temp = Ifile.readLine();
                new_input = parseLine(temp,line_number,filename);
                if (new_input[0]=="{"){brace_level++;}
                if (new_input[0]=="}"){brace_level--;}
                varmap.parse_functions_add(line[offset+2],temp);
                line_number++;
            }
        }
        else
        {
            // error
            parse_error("Unknown variable type '"+line[offset+1]+"'!\
            \nTypes are: 'string', 'real', 'preal', 'bool', 'file' or 'function'",line_number,filename);
        }
    } else if(command=="join") {
    	// Example join "Hello" + var1 var2
    	parse_arg_check_atleast(line,line_number,6,command,filename);
        string buffer;
        varmap.expectType(line[3],varmap._stringI|varmap._stringIliteral,line_number,filename);
        if (line[4]=="+")
        {
            // join "hello" + var1 + var2 -> var3
            // join "hello" + var1 + var2 + var3 -> var4
            // join the 2 initial variables into the buffer...
            varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
            varmap.expectType(line[3],varmap._stringI|varmap._stringIliteral,line_number,filename);
            varmap.stripLiteralQuotes(line[1]);
            varmap.stripLiteralQuotes(line[3]);
            buffer+=line[1] + line[3];
            int add_pos=4;
            //cout << "before loop" << endl;
            while( (unsigned)(add_pos+2) < (line.size()) )
            {
                //cout << "loop" << endl;

                if (line[add_pos]!="+")
                    break;

                parse_arg_check_atleast(line,line_number,add_pos+4,command,filename);
                //cout << "Looking at " << add_pos << endl;
                //cout << "LS: " << line.size() << endl;
                //cout << "LS-1: " << line.size()-1 << endl;
                if ( ((unsigned)add_pos+2) > (line.size()-1) )
                {
                    parse_error("Error: the position of the + character is greater than the line size! ("+IntToString(add_pos+2)+" > "+IntToString(line.size()-1)+")",line_number,filename);
                }


                if (line[add_pos+2]!="+")
                {
                    // check that line[add_pos+3] is a string variable and exists
                    varmap.expectType(line[add_pos+3],varmap._stringI|varmap._stringIliteral,line_number,filename);
                }

                // join the next variable into the buffer
                //cout << "Writing join command" << endl;
                varmap.stripLiteralQuotes(line[add_pos+1]);
                buffer+=line[add_pos+1];
                add_pos+=2;
                //cout << "Added to add_pos" << endl;
                //cout << "new add_pos is " << add_pos << endl;
                //cout << "line size is " << line.size()-1 << endl;

                if ( ( (unsigned)(add_pos) > (line.size()-1) ) || ( (unsigned)(add_pos) == (line.size()-1) ) )
                {
                    //cout << "broken loop" << endl;
                    break;
                }

                //cout << "end of loop" << endl;
            }
            // copy the buffer into the destination variable
            // check that -> is present in position 4
            if (line[add_pos]!="->")
            {
                parse_error("Error: Expected '->' at position "+IntToString(add_pos)+"!",line_number,filename);
            }
            if (varmap.isConst(line[add_pos+1]))
            {
                parse_error("Error: Unable to set '"+line[add_pos+1]+"' as it has been declared as a constant!",line_number,filename);
            }
            varmap.set(line[add_pos+1],buffer);
            return "";
        }
        else
        {
            // join "Hello" + var1 -> var2
            // check that -> is present in position 4
            if (line[4]!="->")
            {
                parse_error("Error: Expected '->' at position 4!",line_number,filename);
            }
            varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
            varmap.expectType(line[3],varmap._stringI|varmap._stringIliteral,line_number,filename);
            if (varmap.exists(line[1])){line[1]=varmap.value(line[1]);}else{line[1]=remove_quotes(line[1]);}
            if (varmap.exists(line[3])){line[3]=varmap.value(line[3]);}else{line[3]=remove_quotes(line[3]);}

            if (varmap.isConst(line[5]))
            {
                parse_error("Error: Unable to set '"+line[5]+"' as it has been declared as a constant!",line_number,filename);
            }
            varmap.set(line[5],line[1]+line[3]);
            return "";
        }
    } else if(command=="set") {
    	parse_arg_check(line,line_number,3,command,filename);
        parse_quote_check(line[2],line_number,filename);
        string var_type(varmap.typeof_(line[1]));
        string alloc_type("");
        if (varmap.isConst(line[1]))
        {
            parse_error("Error: Unable to set '"+line[1]+"' as it has been declared as a constant!",line_number,filename);
        }
        if (varmap.exists(line[2]))
        {
            alloc_type = varmap.typeof_(line[2]);
        }
        else if (isNumber(line[2]))
        {
            alloc_type = "real";
        }
        else if (line[2].at(0) == '"')
        {
            alloc_type = "string";
        }
        else if ((line[2]=="true") || (line[2]=="false"))
        {
            alloc_type = "bool";
        }
        else
        {
            parse_error("Error: Unknown allocation type '"+alloc_type+"' for '"+line[2]+"'!",line_number,filename);
        }
        bool isPreal=false;
        if (var_type=="preal"){var_type="real"; isPreal=true;}
        if (var_type != alloc_type)
        {
            // error
            parse_error("Error: Unable to set for variable type '"+var_type+"' and allocator type '"+alloc_type+"'!\
            \nData type mismatch! ("+line[1]+" -> "+line[2]+")",line_number,filename);
        }
        if (varmap.exists(line[2]))
        {
            line[2]=varmap.value(line[2]);
        }
        if (var_type == "string")
        {
            varmap.set(line[1],remove_quotes(line[2]));
        }
        else if (var_type == "real")
        {
            if (isPreal)
            {
                // container can contain precice data...
                decimal<prealDecimalCount> value(StringToInt(line[2]));
                line[2]=toString(value);
            }
            varmap.set(line[1],line[2]);
        }
        else if (var_type == "bool")
        {
            varmap.set(line[1],line[2]);
        }
        else
        {
            // error
            parse_error("Error: Unknown data type '"+var_type+"'!\
            \nExpected 'string', 'real', 'preal' or 'bool'",line_number,filename);
        }
    } else if (command=="die") {
        parse_arg_check(line,line_number,1,command,filename);
        return "**DIE**";
	} else if (command=="end") {
        parse_arg_check(line,line_number,1,command,filename);
        return "**END**";
    } else if (command=="cin") {
        parse_arg_check(line,line_number,2,command,filename);
        if (varmap.isConst(line[1]))
        {
            parse_error("Error: Unable to set '"+line[1]+"' as it has been declared as a constant!",line_number,filename);
        }
        varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
        /*
        // IMPORTANT!
        // Don't change this to cin because it will not read the whole of the input buffer correctly!
        int declare_length = CHAR_MAX;
        char input_temp[declare_length];
        fgets(input_temp,declare_length,stdin); strncpy(strstr(input_temp,"\n"),"",2);
        varmap.set(line[1],CharToString(input_temp));
        // IMPORTANT!
        cin.clear();*/
        string buf;
        if (show_debug){
            cout << "Waiting for input..." << endl;
        }
        buf=getInput();
        //buf=replaceAll(buf,"\"","\\\"");
        if (show_debug){
            cout << "Got: '" << buf << "'" << endl;
        }
        varmap.set(line[1],buf);
	} else if (command=="clear") {
        parse_arg_check(line,line_number,2,command,filename);
        if (varmap.isConst(line[1]))
        {
            parse_error("Error: Unable to set '"+line[1]+"' as it has been declared as a constant!",line_number,filename);
        }
        varmap.set(line[1],"");
        //returnValue="memset(&"+line[1]+"[0], 0, sizeof("+line[1]+"));";
	} else if (command=="pause") {
        parse_arg_check(line,line_number,1,command,filename);
        cout << "";
        con_pause();
        return "";
    } else if ((command=="else") || (command=="elif")) {
        // this should not be by itself so throw an error...
        if (command=="else") { parse_arg_check(line,line_number,1,command,filename);}else{parse_arg_check_atleast(line,line_number,4,command,filename);}
        parse_error("Error: Lone '"+command+"' outside of an if block!",line_number,filename);
	} else if ((command=="if") || (command=="unless")) {
        if (show_debug){
            DisplayOnConsole("normal","Begining an if block...");
        }
        parse_arg_check_atleast(line,line_number,4,command,filename);
        parse_quote_check(line[1],line_number,filename);
        parse_quote_check(line[3],line_number,filename);

        vector<string> condition;
        for (unsigned int i=1; i!=line.size(); i++){condition.push_back(line[i]);}
        ::conditional cond;
        bool run_block=cond.check(condition,line_number,filename,command);

        if (runningFunction){if (while_begin>0){while_begin--;}}
        if ((loop_type=="while")||(runningFunction)){while_begin++;}
        //cout << "Beginning if loop at while begin '" << while_begin << "';" << endl;
        // run the execute for the block...
        vector <string> new_input;
        if (run_block)
        {
            // continue the block...
            if (show_debug)
            {
                DisplayOnConsole("normal","Executing if block...");
            }

            int old_brace_level;
            string temp;
            if ((loop_type=="while")||(runningFunction))
            {
                // load the value from the while_stack vector instead
                temp = while_stack[while_begin];
                new_input = parseLine(temp,line_number,filename);
                if ((new_input[0]!="{")&&(new_input[0]!="}")){if (runningIF){while_begin--; temp = while_stack[while_begin];}}
                if (show_debug){
                    cout << "While begin is " << while_begin << endl;
                    cout << "Value is " << temp << endl;
                }
                while_begin++;
            }
            else
            {
                temp = Ifile.readLine();
                line_number++;
            }
            new_input = parseLine(temp,line_number,filename);
            if (new_input[0]!="{")
            {
                parse_error("Error: Expected a { instead of '"+new_input[0]+"'!",line_number,filename);
            }
            else
            {
                old_brace_level=brace_level;
                brace_level++;
                if (show_debug){cout << "IF changed BL to " << brace_level << endl;}
            }

            string input_line;
            while(old_brace_level!=brace_level)
            {
                if ((loop_type=="while")||(runningFunction))
                {
                    // load the value from the while_stack vector instead
                    temp = while_stack[while_begin];
                    if (show_debug){
                        cout << "While begin is " << while_begin << endl;
                        cout << "Value is " << temp << endl;
                    }
                    while_begin++;
                }
                else
                {
                    temp = Ifile.readLine();
                    line_number++;
                }
                new_input = parseLine(temp,line_number,filename);
                input_line=new_input[0];
                string parse_return = parse(temp,line_number,Ifile,true,filename,false,loop_type,while_stack,while_begin,runningFunction);
                if (parse_return_states(parse_return,while_begin)!=""){return parse_return;}
            }

            input_line="elif";
            while(input_line=="elif")
            {
                if ((loop_type=="while")||(runningFunction))
                {
                    // load the value from the while_stack vector instead
                    temp = while_stack[while_begin];
                    while_begin++;
                }
                else
                {
                    temp = Ifile.readLine();
                    line_number++;
                }
                new_input = parseLine(temp,line_number,filename);
                input_line=new_input[0];
                if ((input_line=="else") || (input_line=="elif"))
                {
                    if (runningFunction){if(input_line=="elif"){while_begin++;}}
                    //cout<<__FILE__<<" "<<__LINE__;
                    if ((loop_type=="while")||(runningFunction))
                    {
                        // load the value from the while_stack vector instead
                        if ((runningFunction)&&(input_line!="else")){while_begin--;}
                        temp = while_stack[while_begin];
                        while_begin++;
                    }
                    else
                    {

                        temp = Ifile.readLine();
                        line_number++;
                    }
                    new_input = parseLine(temp,line_number,filename);
                    if (new_input[0]!="{")
                    {
                        parse_error("Error: Expected a { instead of '"+new_input[0]+"'!",line_number,filename);
                    }
                    else
                    {
                        old_brace_level=brace_level;
                        brace_level++;
                        if (show_debug){cout << "IF changed BL to " << brace_level << endl;}
                    }

                    while(old_brace_level!=brace_level)
                    {
                        if ((loop_type=="while")||(runningFunction))
                        {
                             // load the value from the while_stack vector instead
                            temp = while_stack[while_begin];
                            while_begin++;
                        }
                        else
                        {
                            temp = Ifile.readLine();
                            line_number++;
                        }
                        new_input = parseLine(temp,line_number,filename);
                        if (new_input[0]=="{")
                        {
                            brace_level++;
                            if (show_debug){cout << "IF changed BL to " << brace_level << endl;}

                        }
                        if (new_input[0]=="}")
                        {
                            brace_level--;
                            if (show_debug){cout << "IF changed BL to " << brace_level << endl;}
                        }

                        if (Ifile.eof())
                        {
                            parse_error("Error: Unexpected end of input! No closing brace was found.",line_number,filename);
                            break;
                        }
                    }
                }
                else
                {
                    bool executeElse=true;

                    if ((runningIF)&&(runningFunction))
                    {
                        new_input = parseLine(temp,line_number,filename);
                        if (new_input[0]=="}") // this fixes problems where an if statement inside another statement inside a function causes a brace mismatch.
                        {
                            executeElse=false;
                        }
                    }

                    if (executeElse)
                    {
                        if (show_debug)
                        {
                            DisplayOnConsole("normal","No else block was found, executing the line instead...");
                        }
                        string parse_return = parse(temp,line_number,Ifile,true,filename,false,loop_type,while_stack,while_begin,runningFunction);
                        if (parse_return_states(parse_return,while_begin)!=""){return parse_return;}
                    }
                }
            }
            if (runningFunction){while_begin--;}
            new_while_position=while_begin;
            return parse_return_jump;
        }
        else
        {
            // stop the block and search for an else...
            if (show_debug)
            {
                DisplayOnConsole("normal","Skipping if block...");
            }

            string input_line;
            string temp;
            if ((loop_type=="while")||(runningFunction))
            {
                // load the value from the while_stack vector instead
                temp = while_stack[while_begin];
                if (show_debug){
                    cout << "While begin is " << while_begin << endl;
                    cout << "Value is " << temp << endl;
                }
                new_input = parseLine(temp,line_number,filename);
                if ((new_input[0]!="{")&&(new_input[0]!="}")){if (runningIF){while_begin--; temp = while_stack[while_begin];}}
                while_begin++;
            }
            else
            {
                temp = Ifile.readLine();
                line_number++;
            }
            int old_brace_level;
            new_input = parseLine(temp,line_number,filename);
            if (new_input[0]!="{")
            {
                parse_error("Error: Expected a { instead of '"+new_input[0]+"'!",line_number,filename);
            }
            else
            {
                old_brace_level=brace_level;
                brace_level++;
            }

            while(old_brace_level!=brace_level)
            {
                if ((loop_type=="while")||(runningFunction))
                {
                    // load the value from the while_stack vector instead
                    temp = while_stack[while_begin];
                    while_begin++;
                }
                else
                {
                    temp = Ifile.readLine();
                    line_number++;
                }
                new_input = parseLine(temp,line_number,filename);
                if (new_input[0]=="{")
                {
                    brace_level++;
                    if (show_debug){cout << "IF changed BL to " << brace_level << endl;}
                }
                if (new_input[0]=="}")
                {
                    brace_level--;
                    if (show_debug){cout << "IF changed BL to " << brace_level << endl;}
                }

                if ((loop_type=="while")||(runningFunction))
                {
                    if (while_stack.size()==(unsigned)while_begin)
                    {
                        parse_error("Error: Unexpected end of input! No closing brace was found.",line_number,filename);
                        break;
                    }
                }
                if (Ifile.eof())
                {
                    parse_error("Error: Unexpected end of input! No closing brace was found.",line_number,filename);
                    break;
                }
            }

            if ((loop_type=="while")||(runningFunction))
            {
                // load the value from the while_stack vector instead
                temp = while_stack[while_begin];
                while_begin++;
            }
            else
            {
                temp = Ifile.readLine();
                line_number++;
            }
            new_input = parseLine(temp,line_number,filename);
            input_line=new_input[0];
            if ((input_line=="else") || (input_line=="elif"))
            {
                if (runningFunction){if(input_line=="elif"){while_begin++;}}
                //cout<<__FILE__<<" "<<__LINE__;
                if (input_line=="elif")
                {
                    temp=temp.replace(0,5,"if ");
                    if (while_begin > 0){while_begin--;}
                    string parse_return = parse(temp,line_number,Ifile,true,filename,fileIsInclude,loop_type,while_stack,while_begin,runningFunction);
                    if (parse_return_states(parse_return,while_begin)!=""){return parse_return;}
                    new_while_position=while_begin;
                    return parse_return_jump;
                }
                else
                {
                    // execute all of the next block...
                    if (show_debug)
                    {
                        DisplayOnConsole("normal","Executing else block...");
                    }

                    if ((loop_type=="while")||(runningFunction))
                    {
                        // load the value from the while_stack vector instead
                        temp = while_stack[while_begin];
                        while_begin++;
                    }
                    else
                    {
                        temp = Ifile.readLine();
                        line_number++;
                    }
                    new_input = parseLine(temp,line_number,filename);
                    input_line=new_input[0];
                    int old_brace_level=brace_level;
                    if (input_line!="{")
                    {
                        parse_error("Error: Expected a '{' instead of '"+input_line+"'!",line_number,filename);
                    }
                    else
                    {
                        brace_level++;
                        if (show_debug){cout << "IF changed BL to " << brace_level << endl;}
                    }

                    while(old_brace_level!=brace_level)
                    {
                        string finput;
                        if ((loop_type=="while")||(runningFunction))
                        {
                            // load the value from the while_stack vector instead
                            finput = while_stack[while_begin];
                            while_begin++;
                        }
                        else
                        {
                            finput = Ifile.readLine();
                            line_number++;
                        }
                        new_input = parseLine(finput,line_number,filename);
                        input_line=new_input[0];
                        string parse_return = parse(finput,line_number,Ifile,true,filename,false,loop_type,while_stack,while_begin);
                        if (parse_return_states(parse_return,while_begin)!=""){return parse_return;}

                        if (Ifile.eof())
                        {
                            parse_error("Error: Unexpected end of input! No closing brace was found.",line_number,filename);
                            break;
                        }
                    }

                    if (show_debug)
                    {
                        DisplayOnConsole("normal","Completed else block...");
                    }
                    if (runningFunction){if(input_line=="else"){while_begin--;}}
                    new_while_position=while_begin;
                    return parse_return_jump;
                }
            }
            else
            {
                if (show_debug)
                {
                    DisplayOnConsole("normal","No else block was found, executing the line instead...");
                }
                string parse_return = parse(temp,line_number,Ifile,true,filename,false,loop_type,while_stack,while_begin,runningFunction);
                if (parse_return_states(parse_return,while_begin)!=""){return parse_return;}
                if (runningFunction){while_begin--;}
                new_while_position=while_begin;
                return parse_return_jump;
            }
        }
	} else if (command=="while") {
        // while [condition]
        parse_arg_check(line,line_number,4,command,filename);

        vector<string> condition;
        for (unsigned int i=1; i!=line.size(); i++){condition.push_back(line[i]);}

        vector <string> new_input;
        vector <string> while_block;
        int old_brace_level;
        int pos;
        bool while_constructed=false;
        if ((loop_type!="while") || (runningFunction))
        {
            // import data into while_block
            if (!runningFunction)
            {
                if (show_debug){
                    cout << "Reading from: '" << Ifile.whatFile() << "'..." << endl;
                }
                string temp = Ifile.readLine();
                line_number++;
                new_input = parseLine(temp,line_number,filename);
                if (new_input[0]!="{")
                {
                    parse_error("Error: Expected a { instead of '"+new_input[0]+"'!",line_number,filename);
                }
                else
                {
                    while_block.push_back("{");
                    old_brace_level=brace_level;
                    brace_level++;
                }

                string input_line;
                while(old_brace_level!=brace_level)
                {
                    temp = Ifile.readLine();
                    line_number++;
                    temp = removeLeadingWhitespace(temp);
                    temp = removeTrailingWhitespace(temp);
                    //cout << "'" << temp << "'" << endl;
                    while_block.push_back(temp);
                    new_input = parseLine(temp,line_number,filename);
                    input_line=new_input[0];
                    if (input_line=="{"){brace_level++;}
                    if (input_line=="}"){brace_level--;}
                }
            }
            else
            {
                if (show_debug){
                    cout << "Constructing while block from function vector..." << endl;
                }
                pos=while_begin;
                while_begin=0;
                string temp=while_stack[pos];
                temp = removeLeadingWhitespace(temp);
                temp = removeTrailingWhitespace(temp);
                new_input = parseLine(temp,line_number,filename);
                if (new_input[0]!="{")
                {
                    parse_error("Error: Expected a { instead of '"+new_input[0]+"'!",line_number,filename);
                }
                else
                {
                    //while_block.push_back("{");
                    old_brace_level=brace_level; // because we need to ignore the construction braces of functions
                    brace_level++;
                }

                string input_line;
                int checkLevel=old_brace_level;
                bool removeAtFirstBrace=true;
                while(checkLevel!=brace_level)
                {
                    //cout << "comparing " << old_brace_level << " to " << brace_level << endl;
                    temp = while_stack[pos];
                    pos++;
                    temp = removeLeadingWhitespace(temp);
                    temp = removeTrailingWhitespace(temp);
                    if (show_debug) {cout << "Pushing " << temp << " into the function vector..." << endl;}
                    while_block.push_back(temp);
                    //DisplayOnConsole("red",temp,true);
                    new_input = parseLine(temp,line_number,filename);
                    input_line=new_input[0];
                    if (input_line=="{"){if (removeAtFirstBrace) {checkLevel++; removeAtFirstBrace=false;} brace_level++;}
                    if (input_line=="}"){brace_level--;}
                }
                brace_level--;

                // dump the constructed stack...
                if (show_debug){
                    cout << "Dumping new stack..." << endl;
                    cout << "Starting at " << while_begin << endl;
                    for (int unsigned i=while_begin; i!=while_block.size(); i++)
                    {
                        cout << "[" << i << "]: '" << while_block[i] << "';" << endl;
                    }
                    cout << "Dump end." << endl;
                }
            }
        }
        else
        {
            if (show_debug){
                cout << "Constructing while block from while vector..." << endl;
            }
            while_begin++;
            pos=while_begin;
            while_begin=0;
            string temp=while_stack[pos];
            temp = removeLeadingWhitespace(temp);
            temp = removeTrailingWhitespace(temp);
            new_input = parseLine(temp,line_number,filename);
            if (new_input[0]!="{")
            {
                parse_error("Error: Expected a { instead of '"+new_input[0]+"'!",line_number,filename);
            }
            else
            {
                //while_block.push_back("{");
                old_brace_level=brace_level; // because we need to ignore the construction braces of functions
                brace_level++;
            }

            string input_line;
            int checkLevel=old_brace_level;
            bool removeAtFirstBrace=true;
            while(checkLevel!=brace_level)
            {
                //cout << "comparing " << old_brace_level << " to " << brace_level << endl;
                temp = while_stack[pos];
                pos++;
                temp = removeLeadingWhitespace(temp);
                temp = removeTrailingWhitespace(temp);
                if (show_debug) {cout << "Pushing " << temp << " into the while vector..." << endl;}
                while_block.push_back(temp);
                //DisplayOnConsole("red",temp,true);
                new_input = parseLine(temp,line_number,filename);
                input_line=new_input[0];
                if (input_line=="{"){if (removeAtFirstBrace) {checkLevel++; removeAtFirstBrace=false;} brace_level++;}
                if (input_line=="}"){brace_level--;}
            }
            brace_level++;

            // dump the constructed stack...
            if (show_debug){
                cout << "Dumping new stack..." << endl;
                cout << "Starting at " << while_begin << endl;
                for (int unsigned i=while_begin; i!=while_block.size(); i++)
                {
                    cout << "[" << i << "]: '" << while_block[i] << "';" << endl;
                }
                cout << "Dump end." << endl;
            }
            while_constructed=true;
        }

        ::conditional cond;
        bool run_block = cond.check(condition,line_number,filename,command);
        //cout << condition[0] << "" << condition[1] << " " << condition[2] << " > " << run_block << endl;
        if (show_debug){if (run_block){cout << "Executing while loop..." << endl;}else{cout << "Skipping while loop..." << endl;}}
        //if (loop_type=="while"){while_begin++;}
        while(run_block)
        {
            //cout << "while loop BL: " << brace_level << endl;
            // run the block code...
            // cout << "Beginning loop" << endl;
            string while_action("");
            for (unsigned int i=while_begin; i!=while_block.size(); i++)
            {
                //cout << "AA: " << i << endl;
                //cout << while_block[i] << endl;
                string parse_return = parse(while_block[i],line_number,Ifile,runningIF,filename,fileIsInclude,"while",while_block,i);
                //cout << parse_return << endl;
                if ((parse_return==parse_return_die)||(parse_return==parse_return_end))
                {
                    brace_level = old_brace_level;
                    return parse_return;
                }
                else if (parse_return==parse_return_break)
                {
                    brace_level=old_brace_level+1;
                    parse("}",line_number,Ifile,false,filename,false);
                    brace_level = old_brace_level;
                    while_action="break";
                    break;
                }
                else if (parse_return==parse_return_continue)
                {
                    brace_level=old_brace_level+1;
                    parse("}",line_number,Ifile,false,filename,false);
                    brace_level = old_brace_level;
                    while_action="continue";
                    break;
                }
                else if (parse_return==parse_return_jump)
                {
                    if (show_debug){cout << "Changing while position to " << new_while_position-1 << endl;}
                    i=new_while_position-1;
                    //cout << "'" << new_while_position << "'" << endl;
                    new_while_position=0;
                }

                ::conditional cond;
                run_block = cond.check(condition,line_number,filename,command);
                if (!run_block)
                {
                    // do some cleanup...
                    brace_level=old_brace_level+1;
                    parse("}",line_number,Ifile,false,filename,false);
                    while_action="break";
                    break;
                }
                if (show_debug){
                cout << "While has looped..." << endl;}
                //cout << "BB: " << i << endl;
                //cout << "LOOP" << endl;
            }
            //cout << "while loop BL: " << brace_level << endl;
            if (show_debug){
            cout << "End of while loop" << endl;}
            if ((show_debug) && (while_action!=""))
            {
                cout << "While action: " << while_action << endl;
            }
            if (while_action=="break")
            {
                new_while_position=(while_block.size()-1)-(while_block.size()-pos);
                //cout << "telling to change while pos to " << new_while_position << endl;
                if (while_constructed){new_while_position++;}
                return parse_return_jump;
            }
            else if (while_action=="continue")
            {
                /*if (runningFunction){
                    return "**CONTINUE**";
                }*/
                continue;
            }
        }
        if (show_debug){cout << "End of while section..." << endl;}
        return "";
	} else if ((command=="math")||(command=="maths")) {
        // math [n1] [operation] [n2] -> [n3]
        // sets n3 to n1 with the operation applied using n2
        parse_arg_check_atleast(line,line_number,6,command,filename);

        vector<string> MathStack;
        // push all of the operations to the stack
        for (unsigned int i=1; i!=line.size(); i++)
        {
            MathStack.push_back(line[i]);
        }

        // find if there is more than 1 operation
        if (MathStack.size() < 6)
        {
            // 1 operator
            if (show_debug){cout << "Only 1 operator..." << endl;}

            // check they are valid...
            if (!isValidMathsOperator(MathStack[1]))
            {
                parse_error("Unknown math operation '"+MathStack[1]+"'!\
                \nOperations are: '+', '-', '*', '/'",line_number,filename);
            }
            // do the op...
            if (varmap.isConst(MathStack[4]))
            {
                parse_error("Error: Unable to set '"+MathStack[4]+"' as it has been declared as a constant!",line_number,filename);
            }

            varmap.expectType(MathStack[4],varmap._realI|varmap._prealI,line_number,filename);
            if (parse_is_variable(MathStack[0],line_number,filename))
            {
                varmap.expectType(MathStack[0],varmap._realI|varmap._prealI,line_number,filename);
            }
            else
            {
                if (!parseIsBracketedExpression(MathStack[0],line_number,filename))
                {
                    if (!isNumber(MathStack[0]))
                    {
                        parse_error("Error: '"+MathStack[0]+"' is not a real!",line_number,filename);
                    }
                }
            }

            if (parse_is_variable(MathStack[2],line_number,filename))
            {
                varmap.expectType(MathStack[2],varmap._realI|varmap._prealI,line_number,filename);
            }
            else
            {
                if (!parseIsBracketedExpression(MathStack[2],line_number,filename))
                {
                    if (!isNumber(MathStack[2]))
                    {
                        parse_error("Error: '"+MathStack[2]+"' is not a real!",line_number,filename);
                    }
                }
            }

            if (MathStack[3]!="->")
            {
                parse_error("Error: Expected '->' as directional indicator!",line_number,filename);
            }

            decimal<prealDecimalCount> temp(0);
            if (varmap.exists(MathStack[0])){MathStack[0]=varmap.value(MathStack[0]);}
            else if(parseIsBracketedExpression(MathStack[0],line_number,filename)){
                // do bracketed stuff...
                MathStack[0]=expandBracketedExpression(MathStack[0],line_number,filename);
            }
            if (varmap.exists(MathStack[2])){MathStack[2]=varmap.value(MathStack[2]);}
            else if(parseIsBracketedExpression(MathStack[0],line_number,filename)){
                // do bracketed stuff...
                MathStack[1]=expandBracketedExpression(MathStack[1],line_number,filename);
            }
            if (MathStack[1]=="+")
            {
                double res = StringToDouble(MathStack[0])+StringToDouble(MathStack[2]);
                temp.setAsDouble(res);
            }
            else if (MathStack[1]=="-")
            {
                double res = StringToDouble(MathStack[0])-StringToDouble(MathStack[2]);
                temp.setAsDouble(res);
            }
            else if (MathStack[1]=="*")
            {
                double res = StringToDouble(MathStack[0])*StringToDouble(MathStack[2]);
                temp.setAsDouble(res);
            }
            else if (MathStack[1]=="/")
            {
                if (StringToFloat(MathStack[2])==0){parse_error("Error: DIV 0",line_number,filename);}
                double res = StringToDouble(MathStack[0])/StringToDouble(MathStack[2]);
                temp.setAsDouble(res);
            }
            else
            {
                parse_error("Unknown math operation '"+MathStack[2]+"'!\
                \nOperations are: '+', '-', '*', '/'",line_number,filename);
            }

            if (varmap.isType(MathStack[4],varmap._prealI,line_number,filename))
            {
                varmap.set(MathStack[4],toString(temp));
            }
            else
            {
                varmap.set(MathStack[4],FloatToString(StringToFloat(toString(temp))));
            }
        }
        else
        {
            // more than 1, find the first * or /
            if (show_debug){cout<<"More than 1 operator..."<<endl;}
            while(MathStack.size()!=0)
            {
                bool foundPriorityOp=false;
                bool foundBracket=false;
                unsigned int priorityPos=0;
                unsigned int workingPos=0;
                //cout << "beginning loop" << endl;
                for (unsigned int i=0; i!=MathStack.size()-1; i++)
                {
                    if ((foundPriorityOp)||(foundBracket)) {break;}
                    if (parseIsBracketedExpression(MathStack[i],line_number,filename))
                    {
                        workingPos=i;
                        foundBracket=true;
                    }
                    else if ((MathStack[i]=="*")||(MathStack[i]=="/"))
                    {
                        priorityPos=i;
                        foundPriorityOp=true;
                    }
                    else if ((MathStack[i]=="+")||(MathStack[i]=="-"))
                    {
                        workingPos=i;
                    }
                }
                if (foundPriorityOp)
                {
                    // there is at least 1 / or *
                    if (show_debug){cout << "P op" << endl;}
                    if ((priorityPos-1<0)||(priorityPos+1>MathStack.size()))
                    {
                        parse_error("Malformed expression!",line_number,filename);
                    }
                    decimal<prealDecimalCount> temp(0);
                    if (varmap.exists(MathStack[priorityPos-1])){MathStack[priorityPos-1]=varmap.value(MathStack[priorityPos-1]);}
                    if (varmap.exists(MathStack[priorityPos+1])){MathStack[priorityPos+1]=varmap.value(MathStack[priorityPos+1]);}

                    if (MathStack[priorityPos]=="*")
                    {
                        double res = StringToDouble(MathStack[priorityPos-1])*StringToDouble(MathStack[priorityPos+1]);
                        temp.setAsDouble(res);
                    }
                    else if (MathStack[priorityPos]=="/")
                    {
                        if (StringToFloat(MathStack[priorityPos+1])==0){parse_error("Error: DIV 0",line_number,filename);}
                        double res = StringToDouble(MathStack[priorityPos-1])/StringToDouble(MathStack[priorityPos+1]);
                        temp.setAsDouble(res);
                    }
                    // cleanup...
                    /*for (unsigned int t; t!=MathStack.size(); t++)
                    {
                        cout << "[" << t << "]: " << MathStack[t] << endl;
                    }*/
                    MathStack[priorityPos-1]=toString(temp);
                    MathStack.erase(MathStack.begin()+(priorityPos),MathStack.begin()+priorityPos+2);
                    priorityPos=0;
                    foundPriorityOp=false;
                    /*for (unsigned int t; t!=MathStack.size(); t++)
                    {
                        cout << "[" << t << "]: " << MathStack[t] << endl;
                    }*/
                }
                else if (foundBracket)
                {
                    // do bracketed expression...
                    if (show_debug){cout<<"Expanding bracketed expression..."<<endl;}
                    string expanded=expandBracketedExpression(MathStack[workingPos],line_number,filename);
                    // do some cleanup...
                    MathStack[workingPos]=expanded;
                    workingPos=0;
                    foundBracket=false;
                }
                else
                {
                    // no * or /, do L -> R...
                    if (show_debug){cout<<"L->R mode..."<<endl;}
                    if (MathStack.size() < 5){
                            parse_error("Error: Malformed expression!",line_number,filename);
                        }
                    if (isValidMathsOperator(MathStack[workingPos]))
                    {
                        decimal<prealDecimalCount> temp(0);
                        if (varmap.exists(MathStack[workingPos-1])){MathStack[workingPos-1]=varmap.value(MathStack[workingPos-1]);}
                        if (varmap.exists(MathStack[workingPos+1])){MathStack[workingPos+1]=varmap.value(MathStack[workingPos+1]);}
                        if (MathStack[workingPos]=="+")
                        {
                            double res = StringToDouble(MathStack[workingPos-1])+StringToDouble(MathStack[workingPos+1]);
                            temp.setAsDouble(temp.getAsDouble()+res);
                        }
                        else if (MathStack[workingPos]=="-")
                        {
                            double res = StringToDouble(MathStack[workingPos-1])-StringToDouble(MathStack[workingPos+1]);
                            temp.setAsDouble(temp.getAsDouble()+res);
                        }
                        // do some cleanup...
                        /*for (unsigned int t; t!=MathStack.size(); t++)
                        {
                            cout << "[" << t << "]: " << MathStack[t] << endl;
                        }*/
                        MathStack[workingPos-1]=toString(temp);
                        MathStack.erase(MathStack.begin()+(workingPos),MathStack.begin()+workingPos+2);
                        /*for (unsigned int t; t!=MathStack.size(); t++)
                        {
                            cout << "[" << t << "]: " << MathStack[t] << endl;
                        }*/
                    }
                    else
                    {
                        parse_error("Unknown math operation '"+MathStack[2]+"'!\
                        \nOperations are: '+', '-', '*', '/'",line_number,filename);
                    }
                }
                if (MathStack.size()==3)
                {
                    // no ops left, push into -> []
                    isValidMathsOperator(MathStack[1]);
                    if (varmap.isConst(MathStack[2]))
                    {
                        parse_error("Error: Unable to set '"+MathStack[2]+"' as it has been declared as a constant!",line_number,filename);
                    }
                    decimal<prealDecimalCount> temp(0);
                    temp.setAsDouble(StringToDouble(MathStack[0]));
                    if (varmap.isType(MathStack[2],varmap._prealI,line_number,filename))
                    {
                        varmap.set(MathStack[2],toString(temp));
                    }
                    else
                    {
                        varmap.set(MathStack[2],FloatToString(StringToFloat(toString(temp))));
                    }
                    MathStack.clear();
                }
            }
        }
	} else if (command=="tostring") {
        // tostring [real (float)] [string (char)]
        parse_arg_check(line,line_number,3,command,filename);
        varmap.expectType(line[2],varmap._stringI,line_number,filename);
        varmap.expectType(line[1],varmap._realI|varmap._prealI|varmap._numericalLiteral,line_number,filename);
        if (varmap.isConst(line[2]))
        {
            parse_error("Error: Unable to set '"+line[2]+"' as it has been declared as a constant!",line_number,filename);
        }
        if (varmap.exists(line[1])){line[1]=varmap.value(line[1]);}
        varmap.set(line[2],line[1]);
        //returnValue="snprintf("+line[2]+", sizeof("+line[2]+"), \"%f\", "+line[1]+");";
	} else if (command == "toreal") {
        // toreal [string (char)] [real (float)]
        parse_arg_check(line,line_number,3,command,filename);
        parse_quote_check(line[1], line_number,filename);
        varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
        varmap.expectType(line[2],varmap._realI|varmap._prealI,line_number,filename);
        if (varmap.isConst(line[2]))
        {
            parse_error("Error: Unable to set '"+line[2]+"' as it has been declared as a constant!",line_number,filename);
        }
        if (varmap.exists(line[1])){line[1]=varmap.value(line[1]);}
        varmap.set(line[2],line[1]);
        //returnValue = line[2]+" = atof("+line[1]+");";
	} else if (command=="conclear") {
        parse_arg_check(line,line_number,1,command,filename);
        // clear the screen
        con_clear();
	} else if (command=="include") {
        parse_arg_check(line,line_number,2,command,filename);
        // include [file path, relitive to the run dir!]
        // writes a ham file into the program, the file is parsed then added

        // standerdise dir sep values to / in the include statements.
        #ifdef _WIN32
            line[1] = replaceAll(line[1],"/","\\");
        #else
            line[1] = replaceAll(line[1],"\\","/");
        #endif

        parse_quote_check(line[1],line_number,filename);
        line[1]=remove_quotes(line[1]);

        //cout << "got include" << endl;

        string load_dir("");
        if (CharToString(line[1].at(0))=="<")
        {
            line[1].replace(0,1,"");
            if (CharToString(line[1].at(line[1].length()-1))==">")
            {
                line[1].replace(line[1].length()-1,line[1].length(),"");
                load_dir=lib_store+directory_separator;
            }
            else
            {
                parse_error("Error: An open < was found but a closing > was not!",line_number,filename);
            }
        }
        else
        {
            load_dir=GetPath(Ifile.whatFile())+directory_separator;
        }

        if (file_exists(load_dir+line[1]))
        {
            // load the file
            readFile includeF;
            includeF.openFile(load_dir+line[1],readFile__read);
            if (show_debug){
                cout << "Reading from: '" << includeF.whatFile() << "'..." << endl;
            }
            string buffer;
            string parse_return;
            int old_line_number = line_number;
            line_number=1;
            bool skip_code=false;
            while(!includeF.eof())
            {
                buffer=includeF.readLine();
                buffer=removeLeadingWhitespace(buffer);
                buffer=removeTrailingWhitespace(buffer);
                if ((buffer.length() != 0))
                {
                    if (((CharToString(buffer.at(0)) == "~")||(buffer=="~")) || ((hasEnding(buffer,"~")) && (skip_code)))
                    {
                        if (skip_code == false) { skip_code = true;
                            if (show_debug){
                                DisplayOnConsole("yellow","Begining to skip code...");
                            }
                        }
                        else
                        { skip_code = false;  buffer = includeF.readLine(); line_number = line_number+1;
                            if (show_debug){
                                DisplayOnConsole("yellow","Finished skipping code");
                            }
                        }
                    }
                }

                if (skip_code != true)
                {
                    parse_return=parse(buffer,line_number,includeF,runningIF,filename+" -> "+GetFileName(line[1]),true);
                    if ((parse_return=="**DIE**")||(parse_return=="**END**"))
                    {
                        return parse_return;
                    }
                    line_number++;
                }
            }
            includeF.close();
            line_number = old_line_number;
            return "";
        }
        else
        {
            parse_error("Error: The file '"+load_dir+line[1]+"' does not exist!",line_number,filename);
        }
	} else if (command=="contitle") {
        parse_arg_check(line,line_number,2,command,filename);
        parse_quote_check(line[1],line_number,filename);
        if (parse_is_variable(line[1],line_number,filename))
        {
            setTitle(varmap.value(line[1]));
        }
        else
        {
            setTitle(remove_quotes(line[1]));
        }
	} else if ((command=="exit") || (command=="break")) {
        // exit a loop
        parse_arg_check(line,line_number,1,command,filename);
        if (loop_type!="while")
        {
            parse_error("Error: Cannot break loop because there is no loop!",line_number,filename);
        }
        return "**BREAK**";
    } else if (command=="continue") {
        // skip over a loop
        parse_arg_check(line,line_number,1,command,filename);
        if (loop_type!="while")
        {
            parse_error("Error: Cannot continue loop because there is no loop!",line_number,filename);
        }
        return "**CONTINUE**";
	} else if (command=="rand") {
        // random number
        // rand [max number (int)] [into]
        parse_arg_check(line,line_number,3,command,filename);
        // into must be a variable and it also must be a real...
        varmap.expectType(line[1],varmap._realI|varmap._numericalLiteral,line_number,filename);
        varmap.expectType(line[2],varmap._realI|varmap._prealI,line_number,filename);
        varmap.stripLiteralQuotes(line[1]);

        std::default_random_engine generator((unsigned)std::chrono::system_clock::now().time_since_epoch().count());
        std::uniform_int_distribution<int> distribution(1,StringToInt(line[1]));
        int random = distribution(generator);

        varmap.set(line[2],IntToString(random));
	} else if (command=="concol") {
        // set the console colour
        // concol [colour name]
        parse_quote_check(line[1],line_number,filename);
        varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
        parse_arg_check(line,line_number,2,command,filename);
        varmap.stripLiteralQuotes(line[1]);
        ChangeConsoleColour(line[1]);
    } else if (command=="conclear") {
        // clear the screen
        con_clear();
    } else if (command=="throw") {
        // throw an exception
        // syntax: throw (fatal) [error text (string)]
        if (line.size()==3)
        {
            varmap.expectType(line[2],varmap._stringI|varmap._stringIliteral,line_number,filename);
            parse_quote_check(line[2],line_number,filename);
            line[2]=remove_quotes(line[2]);
            if (line[1] == "fatal")
            {
                parse_error("FATAL: "+line[2],line_number,filename);
            }
            else if (line[1] == "warning")
            {
                parse_warn("WARNING: "+line[2],line_number,filename);
            }
            else
            {
                parse_error("Error: expected keyword 'fatal' or 'warning'!",line_number,filename);
            }
        }
        else if(line.size()==2)
        {
            varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
            parse_quote_check(line[1],line_number,filename);

            DisplayOnConsole("red",remove_quotes(line[1]));
            return "";
        }
        else
        {
            parse_arg_check(line,line_number,2,command,filename);
        }
	} else if (command=="require") {
        // require [what? kernel/version] [value]
        parse_arg_check_atleast(line,line_number,2,command,filename);
        if (line[1] == "version")
        {
            parse_arg_check_atleast(line,line_number,3,command,filename);
            float check_temp = StringToFloat(line[2]);
            if (check_temp > version)
            {
                parse_error("The program requires runtime version "+line[2]+" or above to run!",line_number,filename);
            }
        }
        else if (line[1] == "kernel")
        {
            parse_arg_check_atleast(line,line_number,3,command,filename);
            string check_temp(StringUpper(line[2]));
            if (check_temp != KERNEL_TYPE)
            {
                parse_error("The program requires the "+StringLower(check_temp)+"kernel to run! This computer is using the "+StringLower(KERNEL_TYPE)+" kernel.",line_number,filename);
            }
        }
        else if (line[1]=="include")
        {
            if (!fileIsInclude)
            {
                parse_error("Error: The program must be ran as an include!",line_number,filename);
            }
        }
        else
        {
            parse_error("Error: Unexpected requirement type '"+line[1]+"'!\
            \nThe expected types are: 'version', 'kernel' or 'include'",line_number,filename);
        }
    } else if (command=="fopen") {
        // fopen [filename] [mode (read/write)] [pointer]
        parse_arg_check(line,line_number,4,command,filename);
        if (line[2]!="read")
        {
            if (line[2]!="write")
            {
                parse_error("Error: unknown file access mode '"+line[2]+"'!",line_number,filename);
            }
        }
        parse_quote_check(line[1],line_number,filename);

        if (parse_is_variable(line[1],line_number,filename))
        {
            if (!varmap.isType(line[1],varmap._stringI,line_number,filename))
            {
                parse_error("Error: Expected '"+line[1]+"' to be a string variable!",line_number,filename);
            }
            line[1] = varmap.value(line[1]);
        }

        if (varmap.isType(line[3],varmap._fileI,line_number,filename))
        {
            if (line[2]=="read")
            {
                readFile *ptr;
                ptr=varmap.parse_files[line[3]];
                ptr->openFile(line[1],readFile__read);
                varmap.parse_files[line[3]]=ptr;
            }
            else
            {
                readFile *ptr;
                ptr=varmap.parse_files[line[3]];
                ptr->openFile(line[1],readFile__write);
                varmap.parse_files[line[3]]=ptr;
            }
        }
        else
        {
            parse_error("Error: '"+line[3]+"' is not a file pointer!",line_number,filename);
        }
    } else if (command=="fclose") {
        // fclose [pointer]
        parse_arg_check(line,line_number,2,command,filename);
        if (varmap.isType(line[1],varmap._fileI,line_number,filename))
        {
            readFile *ptr;
            ptr=varmap.parse_files[line[1]];
            ptr->close();
            varmap.parse_files[line[1]]=ptr;
        }
        else
        {
            parse_error("Error: '"+line[1]+"' is not a file pointer!",line_number,filename);
        }
    } else if (command=="fout") {
        // fout [pointer] [data]
        parse_arg_check(line,line_number,3,command,filename);
        if (varmap.isType(line[1],varmap._fileI,line_number,filename))
        {
            if (!varmap.isType(line[2],varmap._stringI,line_number,filename))
            {
                if (!parse_is_quoted(line[2]))
                {
                    if (!isNumber(line[2]))
                    {
                        if ((!varmap.isType(line[2],varmap._realI,line_number,filename))||
                        (!varmap.isType(line[2],varmap._realI,line_number,filename)))
                        {
                            parse_error("Error: '"+line[2]+"' is not a writeable object!",line_number,filename);
                        }
                    }
                }
                else
                {
                    line[2] = remove_quotes(line[2]);
                }
            }
            if (varmap.exists(line[2])){line[2]=varmap.value(line[2]);}
            readFile *ptr;
            ptr=varmap.parse_files[line[1]];
            ptr->writeLine(line[2]);
            varmap.parse_files[line[1]]=ptr;
        }
        else
        {
            parse_error("Error: '"+line[1]+"' is not a file pointer!",line_number,filename);
        }
    } else if (command=="fin") {
        // fin [pointer] [holder]
        parse_arg_check(line,line_number,3,command,filename);
        if (varmap.isType(line[1],varmap._fileI,line_number,filename))
        {
            varmap.expectType(line[2],varmap._stringI,line_number,filename);
            readFile *ptr;
            ptr=varmap.parse_files[line[1]];
            string input;
            input=ptr->readLine();
            varmap.parse_files[line[1]]=ptr;
            varmap.set(line[2],input);
        }
        else
        {
            parse_error("Error: '"+line[1]+"' is not a file pointer!",line_number,filename);
        }
    } else if (command=="fwl") {
        // fwl [pointer]
        // write the correct endline chars to the file...
        parse_arg_check(line,line_number,2,command,filename);
        if (varmap.isType(line[1],varmap._fileI,line_number,filename))
        {
            readFile *ptr;
            ptr=varmap.parse_files[line[1]];
            ptr->writeLine("");
            varmap.parse_files[line[1]]=ptr;
        }
        else
        {
            parse_error("Error: '"+line[1]+"' is not a file pointer!",line_number,filename);
        }
    } else if (command=="eof") {
        // eof [pointer] -> [bool]
        // check for eof state of a file pointer
        parse_arg_check(line,line_number,4,command,filename);
        if (varmap.isType(line[1],varmap._fileI,line_number,filename))
        {
            varmap.expectDirectional(line[2],line_number,filename);
            varmap.expectType(line[3],varmap._boolI,line_number,filename);
            readFile *ptr;
            ptr=varmap.parse_files[line[1]];
            varmap.set(line[3],(ptr->eof()) ? "true" : "false");
        }
        else
        {
            parse_error("Error: '"+line[1]+"' is not a file pointer!",line_number,filename);
        }
    } else if (command=="mkdir") {
        // mkdir [path]
        // make a new directory
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
        varmap.stripLiteralQuotes(line[1]);
        DirectoryCreate(line[1]);
    } else if (command=="delete") {
        // delete [path]
        // delete a directory or a folder
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
        if (varmap.exists(line[1])){line[1]=varmap.value(line[1]);}else{line[1]=remove_quotes(line[1]);}
        directory_remove(line[1]);
        //returnValue+="if (remove("+line[1]+") == -1) { puts(\"Error deleting file or folder '"+line[1]+"'!\"); }\n";
    } else if ((command=="capitalize") || (command=="capitalise")) {
        // capitalize or calitalise [string]
        // use both because of american spelling.
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._stringI,line_number,filename);
        string temp=varmap.value(line[1]);
        string new_value="";
        if (temp.length()>0){
            if (temp.at(0)==' '){new_value+=" ";}else{
                new_value+=StringUpper(CharToString(temp.at(0)));
            }
                if (temp.length()>1){
                    for(unsigned int i=1; i!=temp.length(); i++){
                        if (temp.at(i)==' '){new_value+=" ";}else{
                            new_value+=StringLower(CharToString(temp.at(i)));
                        }
                }
            }
            varmap.set(line[1],new_value);
        }
    } else if (command=="lower") {
        // lower [string]
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._stringI,line_number,filename);
        varmap.set(line[1],StringLower(varmap.value(line[1])));
    } else if (command=="upper") {
        // upper [string]
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._stringI,line_number,filename);
        varmap.set(line[1],StringUpper(varmap.value(line[1])));
    } else if (command=="floor") {
        // floor [int]
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._realI,line_number,filename);

        if (StringToFloat(varmap.value(line[1])) > 0) {
            varmap.set(line[1],IntToString((int)StringToFloat(varmap.value(line[1]))));
        }
        else
        {
            varmap.set(line[1],IntToString(floor(StringToFloat(varmap.value(line[1])))));
        }
    } else if (command=="ceil") {
        // ceil [int]
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._realI,line_number,filename);

        if (StringToFloat(varmap.value(line[1])) > 0) {
            //varmap.set(line[1],IntToString((int)(StringToFloat(varmap.value(line[1]))+1)));
            varmap.set(line[1],IntToString(ceil(StringToFloat(varmap.value(line[1])))));
        }
        else
        {
            varmap.set(line[1],IntToString(ceil(StringToFloat(varmap.value(line[1])))));
        }
    } else if (command=="round") {
        // round [int]
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._realI|varmap._prealI,line_number,filename);
        varmap.set(line[1],IntToString(roundf(StringToFloat(varmap.value(line[1])))));
    } else if (command=="replacechar") {
        // replacechar [string] [pos] [replacement char]
        parse_arg_check(line,line_number,4,command,filename);
        varmap.expectType(line[1],varmap._stringI,line_number,filename);
        varmap.expectType(line[2],varmap._realI|varmap._numericalLiteral,line_number,filename);
        varmap.expectType(line[3],varmap._stringI|varmap._stringIliteral,line_number,filename);

        if (varmap.exists(line[2])){line[2]=varmap.value(line[2]);}
        if (varmap.exists(line[3])){line[3]=varmap.value(line[3]);}else{line[3]=remove_quotes(line[3]);}
        if ((unsigned)StringToInt(line[2]) > varmap.value(line[1]).length())
        {
            parse_error("Error: The replace position ("+line[2]+") is greater than the declared length ("+IntToString(varmap.value(line[1]).length())+") of the string!",line_number,filename);
        }
        string result=varmap.value(line[1]);
        result.replace(StringToInt(line[2]),StringToInt(line[2])+1,line[3]);
        varmap.set(line[1],result);
    } else if (command=="getchar") {
        // getchar [input string] [pos] -> [output string]
        parse_arg_check(line,line_number,5,command,filename);
        varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
        varmap.expectType(line[2],varmap._realI|varmap._numericalLiteral,line_number,filename);
        varmap.expectDirectional(line[3],line_number,filename);
        varmap.expectType(line[4],varmap._stringI,line_number,filename);

        varmap.stripLiteralQuotes(line[1]);
        varmap.stripLiteralQuotes(line[2]);

        if ((unsigned)StringToInt(line[2]) > varmap.value(line[1]).length()-1)
        {
            parse_error("Error: The get position ("+line[2]+") is greater than the declared length ("+IntToString(varmap.value(line[1]).length()-1)+") of the string!",line_number,filename);
        }

        string result=atPlace(line[1],StringToInt(line[2]));
        varmap.set(line[4],result);
    } else if (command=="substr") {
        // substr [string] [posA] [posB] -> [string]
        parse_arg_check(line,line_number,6,command,filename);
        varmap.expectType(line[1],varmap._stringI,line_number,filename);
        varmap.expectType(line[2],varmap._realI|varmap._prealI|varmap._numericalLiteral,line_number,filename);
        varmap.expectType(line[3],varmap._realI|varmap._prealI|varmap._numericalLiteral,line_number,filename);
        varmap.expectDirectional(line[4],line_number,filename);
        varmap.expectType(line[5],varmap._stringI,line_number,filename);

        string input = varmap.value(line[1]);
        int A,B,len; A = StringToInt(varmap.value(line[2]));
        B = StringToInt(varmap.value(line[3]));
        if (A < 0) {parse_error("Error: initial position is less than 0!",line_number,filename);}
        if ((unsigned)B > input.length()) {parse_error("Error: final position is greater than the defined length!",line_number,filename);}
        if (B < A) {parse_error("Error: Initial position is greater than the final position!",line_number,filename);}
        len = B - A;

        varmap.set(line[5],input.substr(A,len));
    } else if (command=="getseconds") {
        // getseconds [real]
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._realI,line_number,filename);
        time_t rawtime;
        struct tm *timeinfo;
        time(&rawtime);
        timeinfo = localtime(&rawtime);\
        varmap.set(line[1],IntToString((int)timeinfo->tm_sec));
    } else if (command=="getminutes") {
        // getminutes [real]
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._realI,line_number,filename);
        time_t rawtime;
        struct tm *timeinfo;
        time(&rawtime);
        timeinfo = localtime(&rawtime);\
        varmap.set(line[1],IntToString((int)timeinfo->tm_min));
    } else if (command=="gethours") {
        // gethours [real]
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._realI,line_number,filename);
        time_t rawtime;
        struct tm *timeinfo;
        time(&rawtime);
        timeinfo = localtime(&rawtime);\
        varmap.set(line[1],IntToString((int)timeinfo->tm_hour));
    } else if (command=="call") {
        // call [function name] (-> [string])
        parse_arg_check_atleast(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._functionI,line_number,filename);

        if (line.size() > 2){
            // check for ->
            parse_arg_check_atleast(line,line_number,4,command,filename);
            if (line[2]!="->"){
                parse_error("Error: Expected directional operator!",line_number,filename);
            }
            parse("call "+line[1],line_number,Ifile,false,"Internal -> for call",fileIsInclude,loop_type,while_stack,while_begin,runningFunction);
            parse("grab "+line[3],line_number,Ifile,false,"Internal -> for call",fileIsInclude,loop_type,while_stack,while_begin,runningFunction);
        }
        else
        {
            // loop over the whole map for the function to be executed.
            ham_vars::FunctionsStorageMap::const_iterator itr = varmap.parse_functions.find(line[1]);
            //cout << "OK" << endl;
            if(itr != varmap.parse_functions.end())
            {
                vector <string> func_vector = itr->second.first;
                map <string, string> func_info = itr->second.second;
                int max_loop=func_vector.size();
                int function_begin=0;
                int brace_begin = brace_level;
                int func_declare_location = StringToInt(func_info["ln"]);
                string func_declare_filename = func_info["fn"];

                for (int i=function_begin; i!=max_loop; i++)
                {
                    //cout << "`i` is " << i << endl;
                    string &value = func_vector[i];
                    //cout << value << endl;
                    int current_line = (func_declare_location+i);
                    // pretend to be a while loop but give the function vector instead
                    string parse_return = parse(value,current_line,Ifile,false,func_declare_filename,false,"while",func_vector,i+1,true);
                    if ((parse_return=="**DIE**")||(parse_return=="**END**"))
                    {
                        return parse_return;
                    }
                    else if (parse_return==parse_return_jump)
                    {
                        if(show_debug){cout << "Changing function position to " << new_while_position << endl;}
                        i=new_while_position;
                        //cout << func_vector[i+1] << endl;
                        new_while_position=0;
                    }
                    else if (parse_return==parse_return_break)
                    {
                        parse("}",line_number,Ifile,false,filename,false);
                        break;
                    }
                }
                if (brace_begin!=brace_level){
                    // Checker for issue #28 (https://github.com/VioletDarkKitty/hre6/issues/28)
                    parse_warn("The brace level at the beginning and end of the function do not match!",line_number,filename);
                    if (find(forcedFixes.begin(), forcedFixes.end(), 28) == forcedFixes.end()) { // element does not exist...
                        cout << "Fix the incorrect brace? Run with `-f 28` to force this fix [y/N]: ";
                        string ask = getInput();
                        if (StringUpper(ask)=="Y")
                        {
                            cout << "Set the brace level to '" << brace_begin << "' from '" << brace_level << "'" << endl;
                            brace_level=brace_begin;
                        }
                    }
                    else
                    {
                        DisplayOnConsole("yellow","Forcing hotfix for #28");
                        brace_level=brace_begin;
                    }
                }
            }
            return "";
        }
    } else if (command=="return") {
        // return [return var]
        function_return_type = "";
        if (varmap.exists(line[1]))
        {
            if ((varmap.typeof_(line[1])=="string")||
            (varmap.typeof_(line[1])=="real")||(varmap.typeof_(line[1])=="preal")){
            function_return_type=varmap.typeof_(line[1]);
            line[1]=varmap.value(line[1]);
            }
            else
            {
                parse_error("Error: unsupported return type '"+varmap.typeof_(line[1])+"'!",line_number,filename);
            }
        }
        else
        {
            if (isNumber(line[1])){
                function_return_type="real";
            }
            else if (parse_is_quoted(line[1]))
            {
                line[1]=remove_quotes(line[1]);
                function_return_type="string";
            }
            else
            {
                parse_error("Error: unsupported return type!",line_number,filename);
            }
        }
        function_return_temp = line[1];
        //cout << "RET: " << function_return_type << "   " << function_return_temp << endl;
        return "";
    } else if (command=="grab") {
        // grab [target string]
        parse_is_variable(line[1],line_number,filename);
        bool match=true;
        if (((varmap.typeof_(line[1])=="real")||varmap.typeof_(line[1])=="preal")&&(function_return_type!="real"))
        {
            match=false;
        }
        else if ((varmap.typeof_(line[1])=="string")&&(function_return_type!="string"))
        {
            match=false;
        }
        if (!match)
        {
            parse_error("Error: The function returned a value of different type to the container!\
            \n('"+varmap.typeof_(line[1])+"' != '"+function_return_type+"')",line_number,filename);
        }
        varmap.set(line[1],function_return_temp);
        function_return_temp = "";
        function_return_type = "";
        return "";
    } else if ((command=="wait")||(command=="sleep")) {
        // wait|sleep [ms]
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._realI|varmap._numericalLiteral,line_number,filename);
        varmap.stripLiteralQuotes(line[1]);
        #ifdef _WIN32
            Sleep(StringToInt(line[1]));
        #else
            sleep((StringToInt(line[1])/1000));
        #endif
        return "";
    } else if (command=="sizeof") {
        // sizeof [string] -> [int]
        parse_arg_check(line,line_number,4,command,filename);
        varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
        varmap.expectDirectional(line[2],line_number,filename);
        varmap.expectType(line[3],varmap._realI,line_number,filename);
        varmap.set(line[3],IntToString(varmap.value(line[1]).length()));
    } else if (command=="md5") {
        // md5 [string] -> [string]
        // make an md5 of the first string and store it in the second string
        parse_arg_check(line,line_number,4,command,filename);
        varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
        varmap.expectDirectional(line[2],line_number,filename);
        varmap.expectType(line[3],varmap._stringI,line_number,filename);
        if (varmap.exists(line[1])){line[1]=varmap.value(line[1]);}else{line[1]=remove_quotes(line[1]);}
        MD5 temp_md5;
        temp_md5.update(line[1]);
        temp_md5.finalize();
        varmap.set(line[3],temp_md5.hexdigest());
    } else if (command=="sha256") {
        //sha2 [string] -> [string]
        // make an sha256 hash
        parse_arg_check(line,line_number,4,command,filename);
        varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
        varmap.expectDirectional(line[2],line_number,filename);
        varmap.expectType(line[3],varmap._stringI|varmap._stringIliteral,line_number,filename);
        varmap.stripLiteralQuotes(line[1]);
        varmap.set(line[3],sha256(line[1]));
    } else if (command=="find") {
        // find [string] [string2] [int] -> [int]
        // find the first instance of string2 in string starting at int
        parse_arg_check(line,line_number,6,command,filename);
        varmap.expectType(line[1],varmap._stringI|varmap._stringIliteral,line_number,filename);
        varmap.expectType(line[2],varmap._stringI|varmap._stringIliteral,line_number,filename);
        varmap.expectType(line[3],varmap._realI|varmap._numericalLiteral,line_number,filename);
        varmap.expectDirectional(line[4],line_number,filename);
        varmap.expectType(line[5],varmap._realI,line_number,filename);

        if (varmap.exists(line[1])){line[1]=varmap.value(line[1]);}
        if (varmap.exists(line[2])){line[2]=varmap.value(line[2]);}
        if (varmap.exists(line[3])){line[3]=varmap.value(line[3]);}

        int start=StringToInt(line[3]);
        if (start < 0){
            parse_error("Error: The start position cannot be less than 0!",line_number,filename);
        }
        if ((unsigned) start > line[1].length()){ // the result of string.length() will ALWAYS return an unsigned value!!!
            parse_error("Error: The start position cannot exceed the length of the search string!",line_number,filename);
        }
        if (line[2].length() > line[1].length()){
            parse_error("Error: The length of the search instance cannot exceed the length of the search string!",line_number,filename);
        }
        size_t pos=line[1].find_first_of(line[2],StringToInt(line[3]));
        if (pos==string::npos){pos=-1;}
        varmap.set(line[5],IntToString(pos));
    } else if (command=="sqrt") {
        // sqrt [real] -> [real]
        parse_arg_check(line,line_number,4,command,filename);
        varmap.expectType(line[1],varmap._realI|varmap._numericalLiteral,line_number,filename);
        varmap.expectDirectional(line[2],line_number,filename);
        if (parse_is_variable(line[3],line_number,filename))
        {
            if (!varmap.isType(line[3],varmap._realI,line_number,filename))
            {
                if (!varmap.isType(line[3],varmap._prealI,line_number,filename))
                {
                    parse_error("Error: '"+line[3]+"' is not a real variable!",line_number,filename);
                }
            }
        }
        else
        {
            if (!parseIsBracketedExpression(line[3],line_number,filename))
            {
                if (!isNumber(line[3]))
                {
                    parse_error("Error: '"+line[3]+"' is not a real!",line_number,filename);
                }
            }
        }

        if (varmap.exists(line[1])){line[1]=varmap.value(line[1]);}

        decimal<prealDecimalCount> temp;
        temp.setAsDouble(sqrt(StringToDouble(line[1])));
        if (varmap.isType(line[3],varmap._prealI,line_number,filename))
        {
            varmap.set(line[3],toString(temp));
        }
        else
        {
            varmap.set(line[3],FloatToString(StringToFloat(toString(temp))));
        }
    } else if (command=="pow") {
        // pow x power -> storage
        parse_arg_check(line,line_number,5,command,filename);
        varmap.expectType(line[1],varmap._realI|varmap._numericalLiteral,line_number,filename);
        varmap.expectType(line[2],varmap._realI|varmap._numericalLiteral,line_number,filename);
        varmap.expectDirectional(line[3],line_number,filename);
        varmap.expectType(line[4],varmap._realI,line_number,filename);
        if (varmap.exists(line[1])){line[1]=varmap.value(line[1]);}
        if (varmap.exists(line[2])){line[2]=varmap.value(line[2]);}

        varmap.set(line[4],DoubleToString(pow(StringToDouble(line[1]),StringToDouble(line[2]))));
    } else if (command=="getclock") {
        // getclock [real]
        parse_arg_check(line,line_number,2,command,filename);
        varmap.expectType(line[1],varmap._realI,line_number,filename);

        varmap.set(line[1],IntToString(clock()/CLOCKS_PER_SEC));
    } else if (command=="typeof") {
        // typeof [var] -> [string]
        parse_arg_check(line,line_number,4,command,filename);
        if(!parse_is_variable(line[1],line_number,filename)){parse_error("'"+line[1]+"' should be a variable name!",line_number,filename);}
        varmap.expectDirectional(line[2],line_number,filename);
        varmap.expectType(line[3],varmap._stringI,line_number,filename);

        varmap.set(line[3],varmap.typeof_(line[1]));
    } else if (command=="makeConst") {
        // makeConst [varname]
        parse_arg_check(line,line_number,2,command,filename);
        if(!parse_is_variable(line[1],line_number,filename)){parse_error("'"+line[1]+"' should be a variable name!",line_number,filename);}
        string type = varmap.typeof_(line[1]);
        if ((type!="string")&&(type!="real")&&(type!="preal")&&(type!="bool"))
        {
            parse_error("Error: The type '"+type+"' cannot be made const!",line_number,filename);
        }

        varmap.setConst(line[1],true);
    } else if (command=="mutate") {
        // mutate [var] [new type]
        parse_arg_check(line,line_number,3,command,filename);
        if (!parse_is_variable(line[1],line_number,filename)){parse_error("'"+line[1]+"' should be a variable name!",line_number,filename);}
        varmap.expectType(line[2],varmap._stringI|varmap._stringIliteral,line_number,filename);
        if (parse_is_variable(line[2],line_number,filename)){line[2]=varmap.value(line[2]);}else{line[2]=remove_quotes(line[2]);}
        if (!isValidDataType(line[2])){parse_error("'"+line[2]+"' is not a valid data type!",line_number,filename);}
        if ((line[2]=="function")||(line[2]=="file")){parse_error("'"+line[2]+"' is not a valid mutatable type!",line_number,filename);}
        if (varmap.typeof_(line[1])==line[2]){parse_error("Cannot mutate to original type!",line_number,filename);}
        if ((varmap.typeof_(line[1])=="function")||(varmap.typeof_(line[1])=="file")){parse_error("The input variable is not mutatable!",line_number,filename);}
        if (varmap.isConst(line[1])){parse_error("Cannot mutate a const variable!",line_number,filename);}

        if (varmap.typeof_(line[1])=="string")
        {
            if (line[2]=="real"){varmap.setType(line[1],"real");
            varmap.set(line[1],IntToString(StringToInt(varmap.value(line[1]))));}

            if (line[2]=="preal"){varmap.setType(line[1],"preal");
            decimal<prealDecimalCount> temp; temp.setAsDouble(StringToDouble(varmap.value(line[1])));
            varmap.set(line[1],toString(temp));}

            if (line[2]=="bool"){varmap.setType(line[1],"bool");
            varmap.setType(line[1],(StringToInt(varmap.value(line[1]))==1)? "true":"false");}
        }
        else if (varmap.typeof_(line[1])=="real")
        {
            if (line[2]=="string"){varmap.setType(line[1],"string");}

            if (line[2]=="preal"){varmap.setType(line[1],"preal");
            decimal<prealDecimalCount> temp; temp.setAsDouble(StringToDouble(varmap.value(line[1])));
            varmap.set(line[1],toString(temp));}

            if (line[2]=="bool"){varmap.setType(line[1],"bool");
            varmap.setType(line[1],(StringToInt(varmap.value(line[1]))==1)? "true":"false");}
        }
        else if (varmap.typeof_(line[1])=="preal")
        {
            if (line[2]=="string"){varmap.setType(line[1],"string");}

            if (line[2]=="real"){varmap.setType(line[1],"preal");
            decimal<prealDecimalCount> temp; temp.setAsDouble(StringToDouble(varmap.value(line[1])));
            varmap.set(line[1],IntToString(StringToInt(toString(temp))));}

            if (line[2]=="bool"){varmap.setType(line[1],"bool");
            varmap.setType(line[1],(StringToInt(varmap.value(line[1]))==1)? "true":"false");}
        }
        else if (varmap.typeof_(line[1])=="bool")
        {
            if (line[2]=="string"){varmap.setType(line[1],"string");
            varmap.set(line[1],(StringToInt(varmap.value(line[1]))==1)? "true":"false");}

            if (line[2]=="real"){varmap.setType(line[1],"preal");
            decimal<prealDecimalCount> temp; temp.setAsDouble(StringToDouble(varmap.value(line[1])));
            varmap.set(line[1],IntToString(StringToInt(toString(temp))));}

            if (line[2]=="preal"){varmap.setType(line[1],"preal");
            decimal<prealDecimalCount> temp; temp.setAsDouble(StringToDouble(varmap.value(line[1])));
            varmap.set(line[1],toString(temp));}
        }
    } else if (command=="sin") {
        // sin [x] -> [real/preal]
        parse_arg_check(line,line_number,4,command,filename);
        varmap.expectType(line[1],varmap._realI|varmap._prealI|varmap._numericalLiteral,line_number,filename);
        varmap.expectDirectional(line[2],line_number,filename);
        varmap.expectType(line[3],varmap._realI|varmap._prealI,line_number,filename);

        if(parse_is_variable(line[1],line_number,filename)){line[1]=varmap.value(line[1]);}

        decimal<prealDecimalCount> temp; temp.setAsDouble(sin(degreesToRadians(StringToDouble(line[1]))));
        varmap.set(line[3],DoubleToString(temp.getAsDouble()));
    } else if (command=="cos") {
        // cos [x] -> [real/preal]
        parse_arg_check(line,line_number,4,command,filename);
        varmap.expectType(line[1],varmap._realI|varmap._prealI|varmap._numericalLiteral,line_number,filename);
        varmap.expectDirectional(line[2],line_number,filename);
        varmap.expectType(line[3],varmap._realI|varmap._prealI,line_number,filename);

        if(parse_is_variable(line[1],line_number,filename)){line[1]=varmap.value(line[1]);}

        decimal<prealDecimalCount> temp; temp.setAsDouble(cos(degreesToRadians(StringToDouble(line[1]))));
        varmap.set(line[3],DoubleToString(temp.getAsDouble()));
    } else if (command=="tan") {
        // tan [x] -> [real/preal]
        parse_arg_check(line,line_number,4,command,filename);
        varmap.expectType(line[1],varmap._realI|varmap._prealI|varmap._numericalLiteral,line_number,filename);
        varmap.expectDirectional(line[2],line_number,filename);
        varmap.expectType(line[3],varmap._realI|varmap._prealI,line_number,filename);

        if(parse_is_variable(line[1],line_number,filename)){line[1]=varmap.value(line[1]);}

        decimal<prealDecimalCount> temp; temp.setAsDouble(tan(degreesToRadians(StringToDouble(line[1]))));
        varmap.set(line[3],DoubleToString(temp.getAsDouble()));
    } else {
		// Something unexpected - dump to console
		//cout<<"Unhandled code: '"<< in <<"'"<<endl;
        parse_error("Error in parsing; unknown function: '"+command+"'",line_number,filename);
	}

    if (show_debug){
        cout << "Parse has cycled." << endl;
    }
    return "";
}
