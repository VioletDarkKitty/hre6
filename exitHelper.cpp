#include "exitHelper.h"

exitHelper::exitHelper()
{
    //ctor
}

exitHelper::~exitHelper()
{
    //dtor
}

int exitHelper::atexit(exitHelper::func_type f) {
	exitFunctions.push_back(f);
	return 0;
}

void exitHelper::call() {
	for(std::vector<exitHelper::func_type>::iterator it = exitFunctions.begin(); it != exitFunctions.end(); ++it) {
		(*it)();
	}
}

void exitHelper::exit(int error) {
	throw exit_exception(error);
}
