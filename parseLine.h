#ifndef __parseLine_h
#define __parseLine_h

#include <string>
#include <vector>
using namespace std;

vector <string> parseLine(string line, int line_number, string filename, bool allowError=true);

#endif
