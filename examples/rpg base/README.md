rpg base
========

A base project for building an rpg-like game.

Building
========

The project should be built into a .HA file before distributing, this ensures that all dependancies will be checked.
`ham -B [archive name] -B [path to the project root folder]`

Dependancies
------------

dir_sep >= 3.2
