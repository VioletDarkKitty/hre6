#!/bin/bash

BINDIR=$(dirname "$(readlink -fn "$0")")
cd "$BINDIR"

gcc -c library.c -fPIC -o library.o
gcc -shared -fPIC -o library.so library.o
