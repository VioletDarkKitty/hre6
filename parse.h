#ifndef PARSE_H_INCLUDED
#define PARSE_H_INCLUDED

void print_usage();
string parse(string in, int &line_number, readFile &Ifile, bool runningIF=true, string filename="", bool fileIsInclude=false,string loop_type="",
const vector<string>& while_stack=vector<string>(), int while_begin=0, bool runningFunction=false);
void parse_error(string error, int line_number, string filename);

extern int line_number;
extern int new_while_position;

#endif // PARSE_H_INCLUDED
