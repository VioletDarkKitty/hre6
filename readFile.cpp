#include "readFile.h"
#include "functions.h"
#include <cstring>

readFile::readFile()
{
    //ctor
}

readFile::~readFile()
{
    //dtor
    if (file.is_open()) file.close();
}

bool readFile::openFile(const char* fName, int mode)
{
    // Returns true if the file has been opened ok, false otherwise
    string filename = fName;
    switch(mode) {
    case readFile__read:
        file.open(fName, fstream::in);
        openFileName=filename;
        return file.is_open();
        break;
    case readFile__write:
        file.open(fName, fstream::out);
        openFileName=filename;
        return file.is_open();
        break;
    case readFile__append:
        file.open(fName, fstream::app|fstream::out);
        openFileName=filename;
        return file.is_open();
        break;
    case readFile__binWrite:
        if (file_exists(fName))
        {
            file_delete(fName);
        }
        file.open(fName, std::ios_base::out|std::ios_base::binary);
        openFileName=filename;
        return file.is_open();
        break;
    default:
        return false;
    }
}

bool readFile::openFile(string fName, int mode)
{
    char* path = StringToChar(fName);
    bool result = readFile::openFile(path, mode);
    delete[] path;
    return result;
}

// from http://stackoverflow.com/questions/6089231/getting-std-ifstream-to-handle-lf-cr-and-crlf
std::istream& safeGetline(std::istream& is, std::string& t)
{
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case EOF:
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(std::ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }
}

string readFile::readLine()
{
    // renamed to avoid confusion with getline
    // This is where you return a string.
    // If at EOF, perhaps return an empty string
    if (readFile::eof() == true) {return "";}
    string buf("");
    safeGetline(file,buf);
    //getline(file,buf); // POSIX complient.
    return buf;
}

bool readFile::eof()
{
    return file.eof();
}

void readFile::writeLine(string data)
{
    file << data << "\n"; // \n gets converted to \r\n on windows unless open in mode 3
}

void readFile::close()
{
    file.close();
    openFileName="";
}

string readFile::whatFile()
{
    return openFileName;
}
