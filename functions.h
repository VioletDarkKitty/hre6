#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED
#include "readFile.h"
#include <vector>

void con_pause(void);
bool file_exists(std::string filename);
int file_delete(std::string f);
bool directory_exists(std::string dirname);
void setTitle(std::string titleMsg);
std::string GetFileExtension(std::string Fpath);
std::string GetFileName(std::string Fpath);
std::string GetPath(std::string Fpath);
std::string ChangeFileExtention(std::string str,std::string RPL);
std::string StringUpper(std::string s);
std::string StringLower(std::string s);
char* StringToChar(std::string s);
std::string CharToString(char c);
std::string CharToString(char* c);
std::string IntToString(int n);
std::string FloatToString(float n);
std::string DoubleToString(double n);
int StringToInt(std::string str);
float StringToFloat(std::string str);
double StringToDouble(const std::string& s);
double IntToDouble(int a);
void ChangeConsoleColour(std::string col);
void DirectoryCreate(std::string dir);
int directory_remove(std::string d);
void con_clear();
void wait(int ms);
void execute_program(char* programPath, bool finish_wait);
bool directory_delete_recursive(std::string dir);
std::string replaceAll(std::string subject, const std::string& search, const std::string& replace);
string replaceString(string subject, const string& search, const string& replace);
string replaceString(string subject, const unsigned int pos1, const unsigned int pos2, const string& replace);
bool hasEnding (std::string const &fullString, std::string const &ending);
void pack_file(readFile &FilePointer, string IncludeFile);
int unpack_file(readFile &FilePointer, string OutputFile);
string atPlace(string &inputString, int place);
bool isAnyOf(string input, vector<string> &check);
bool isNumber(string in);
void DisplayOnConsole(string colour, string text, bool withNewline=true);
string removeLeadingWhitespace(string Iline);
string removeTrailingWhitespace(string Iline);
#ifdef _WIN32
    bool WinIs64();
    bool IsWow64();
#endif
void system_call(string __command);
void file_copy(string source, string dest);
int GetCount(string s, string of);
string getInput();
void StringExplode(string str, string separator, vector<string>* results);
void StringExplode(string str, string separator, vector<string>& results);
string StringImplode(vector<string> input, string seperator);
bool dirEmpty(string path);

#ifdef _WIN32
    char const directory_separator = '\\';
#else
    char const directory_separator = '/';
#endif // WIN32


#endif // FUNCTIONS_H_INCLUDED
