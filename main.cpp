#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "globals.h"
#include "functions.h"
// http://stackoverflow.com/questions/3956636/c-on-linux-not-recognizing-commands-like-exit-and-printf
#include <cstdlib>
#include <cstdio>
#include <iomanip>
#include "readFile.h" // readFile class
#include "loader.h"
#include "md5.h"

#include "parseLine.h"

#ifdef _WIN32
    #include <windows.h>
    #include <time.h>
    #include <tchar.h>
    #include "shlobj.h"
#endif

#include <cstring>
#include <signal.h>
#include "ExceptionWithStack.h"
#include <unistd.h>
#include "readConfig.h"

#ifdef __linux__
    #include <pwd.h>
#endif
#include "exitHelper.h"
using namespace std;

std::string current_program; // the path to the program which is currently being loaded
std::string current_program_orig; // differs from current_program only if the program is an archive
std::string program_directory;
std::string temp_directory;
std::string program_storage;
std::string current_program_dir;
std::string program_open_dir;
std::string runtime_system;
std::string error_store;
std::string lib_store;
bool prog_open=false;
bool allow_cleanup=true;
bool local_temp=true;
bool fail_pause=true; // Defaulted to on because it will close while setting up for the first time otherwise!
bool allow_console_colour=true; // Do not change, if there is no settings file then it should be defaulted to on.
                                // Else, it is whatever is in the settings file.
bool verbose_include_output=false;
bool verbose_parse_output=false;
bool show_debug=false;
bool show_include_text=false;
vector<int> forcedFixes;
exitHelper quit;

string getProgramDirectory(string argv0)
{
    char cCurrentPath[FILENAME_MAX];
    #ifdef _WIN32
        /*GetCurrentDirectory(FILENAME_MAX, cCurrentPath); // this will get the working path from the commandline not the program storage dir!
        return CharToString(cCurrentPath);*/

        // https://msdn.microsoft.com/en-us/library/windows/desktop/ms683197(v=vs.85).aspx
        if (GetModuleFileName(NULL, cCurrentPath, FILENAME_MAX)!=0) {
                    return GetPath(CharToString(cCurrentPath));
        }

        cerr << GetLastError() << endl;
        quit.exit(EXIT_FAILURE);
        return ""; // included so that the compiler does not complain
    #else
        char* path = StringToChar(argv0);
        realpath(path,cCurrentPath);
        delete[] path;

        //cout << GetPath(CharToString(cCurrentPath)) << endl;
        return GetPath(CharToString(cCurrentPath));
    #endif
}

void CleanupTemp()
{
   if ((prog_open) && (allow_cleanup))
   {
        if (show_debug){
        DisplayOnConsole("purple","Cleaning up TEMP directory...",true);}
        bool delete_return;
        delete_return = directory_delete_recursive(current_program_dir);
        if (!delete_return)
        {
            DisplayOnConsole("red","Error deleting temp directory!",true);
        }
        else
        {
            directory_remove(current_program_dir);
            if (show_debug){DisplayOnConsole("purple","Cleanup complete!",true);}
        }
    }
}

int make_settings(string settings_path)
{
    readConfig settings;
    if (!settings.setOpenFile(settings_path,"w"))
    {
        DisplayOnConsole("red","Failed to open settings file for writing!");
        return -1;
    }
    settings.addPair("Version",FloatToString(settings_file_version));
    settings.addPair("ShowDebug","no");
    settings.addPair("ConsoleColour","no");
    settings.addPair("AllowCleanup","yes");
    settings.addPair("UseLocalTemp","yes");
    settings.addPair("FailPause","no");
    settings.addPair("VerboseIncludes","no");
    settings.addPair("VerboseParsing","no");
    settings.addPair("ShowIncludeText","no");

    if (!settings.saveConfig())
    {
        DisplayOnConsole("red","Failed to save settings file!");
        return -1;
    }
    settings.closeOpenFile();

    return 0;
}

int fix_settings()
{
    DisplayOnConsole("cyan","Settings fixer begin:",true);

    // Create a new settings file in the temp directory and compare it to the existing one...
    /*int fix_make_return = make_settings(temp_directory+directory_separator+"fixer settings.txt");
    if (fix_make_return == -1)
    {
        return -1;
    }*/

    // jump to the return until the function is complete
    DisplayOnConsole("red","This function is not complete yet!",true);

    /*
    // begin to compare the files
    readFile original_settings;
    original_settings.openFile(runtime_system+directory_separator+"Settings - "+KERNEL_TYPE+".txt",0);
    readFile new_settings;

    original_settings.close();
    new_settings.close();
    */

    DisplayOnConsole("cyan","Overwipe the settings file instead? [y/N] > ",false);
    string fix_ask("");
    cin >> fix_ask;
    fix_ask = StringUpper(fix_ask);
    if (fix_ask == "Y")
    {
        make_settings(runtime_system+directory_separator+"Settings - "+KERNEL_TYPE+".txt");
        DisplayOnConsole("normal","Settings created, loading...",true);
        return -2;
    }

    DisplayOnConsole("normal","Cleaning up...",true);
    file_delete(temp_directory+directory_separator+"fixer settings.txt");
    DisplayOnConsole("cyan","Settings fixer end;",true);
    return 0;
}

int load_settings()
{
    if (file_exists(runtime_system+directory_separator+"Settings - "+KERNEL_TYPE+".txt") == true)
    {
        readConfig settings;
        if (!settings.setOpenFile(runtime_system+directory_separator+"Settings - "+KERNEL_TYPE+".txt","r"))
        {
            DisplayOnConsole("red","Error in function 'load_settings', the file could not be opened for reading!",true);
            return -1;
        }

        if (settings.keyExists("Version"))
        {
            float setting_version_temp = StringToFloat(settings.getValue("Version"));
            // replace the first char of the string with "" and turn it into a float

            if (setting_version_temp != settings_file_version)
            {
                DisplayOnConsole("red","The settings file version ("+FloatToString(setting_version_temp)+" and "+FloatToString(settings_file_version)+") do not match!",true);
                DisplayOnConsole("red","Some settings may be missing or loaded incorrectly.",true);
                DisplayOnConsole("normal","Would you like to analyse and attempt to fix the settings file? [y/N] > ",false);
                string fix_ask("");
                cin >> fix_ask;
                fix_ask = StringUpper(fix_ask);
                if (fix_ask == "Y")
                {
                    int fix_settings_return = fix_settings();
                    if (fix_settings_return == -2)
                    {
                        return load_settings();
                    }
                    else
                    {
                        return fix_settings_return;
                    }
                }
            }
        }
        else
        {
            DisplayOnConsole("red","Error: Missing setting 'Version'!");
        }

        if (settings.keyExists("ShowDebug"))
        {
            if (StringLower(settings.getValue("ShowDebug"))=="yes")
            {
                show_debug=true;
            }
            else
            {
                show_debug=false;
            }
        }
        else
        {
            DisplayOnConsole("red","Error: Missing setting 'ShowDebug'!");
        }

        if (settings.keyExists("AllowCleanup"))
        {
            if (StringLower(settings.getValue("AllowCleanup"))=="no")
            {
                DisplayOnConsole("red","[WARNING] Cleanup is disabled in the settings!",true);
                allow_cleanup=false;
            }
            else
            {
                allow_cleanup=true;
            }
        }
        else
        {
            DisplayOnConsole("red","Error: Missing setting 'AllowCleanup'!");
        }

        if (settings.keyExists("UseLocalTemp"))
        {
            if (StringLower(settings.getValue("UseLocalTemp"))=="no")
            {
                local_temp=false;
                #ifdef _WIN32
                    TCHAR buf [MAX_PATH];
                    if (GetTempPath (MAX_PATH, buf) != 0)
                    {
                        string sbuf;
                        sbuf = buf;
                        // the variable will return with a \ on the the end for some reason, remove it here...
                        sbuf = sbuf.replace((sbuf.end()-1),sbuf.end(),""); // replaces the last char in the string
                        DisplayOnConsole("purple", "Found temp_directory: "+sbuf,true);
                        temp_directory=sbuf; // set the temp_directory to the new variable
                    }
                    else
                    {
                        DisplayOnConsole("red","Unable to find the value for the temp directory!\
                        Defaulting to the local directory instead.",true);
                    }
                #else
                    int folder_array_len = 4;
                    string folders[] = { "TMPDIR","TEMPDIR","TEMP","TMP" };
                    string temp_dir = "";
                    string var_name = "";
                    //cout << "Begining for loop" << endl;

                    // run through all of the folders in the array

                    for (int i = 0; i < (folder_array_len-1); i = i+1)
                    {
                        var_name = folders[i];
                        //temp_dir = getenv(StringToChar(var_name));
                        const char * val = ::getenv( var_name.c_str() );
                        if ( val == 0 )
                        {
                            temp_dir = "";
                        }
                        else
                        {
                            temp_dir = val;
                        }

                        if ((temp_dir != "") && (directory_exists(temp_dir)))
                        {
                            break;
                        }
                    }
                    // If we get this far, we've either exited the for, or processed all of the folders
                    //cout << "exited for loop" << endl;

                    if (temp_dir == "")
                    {
                        // We didn't get a match
                        if (directory_exists("/tmp"))
                        {
                            temp_directory="/tmp";
                            // Set colours
                            DisplayOnConsole("yellow","[Notice] Could not find any temp directory variables, defaulting to /tmp!",true);
                        }
                        else
                        {
                            // the /tmp folder does not exist, drop back to local...
                            local_temp=true;
                            temp_directory=program_directory+"/TEMP";
                            DisplayOnConsole("yellow","[Notice] Could not find any temp directory variables and /tmp does not exist!",true);
                            DisplayOnConsole("yellow","Defaulting to the local directory.",true);
                        }
                    }
                    else
                    {
                        temp_directory=temp_dir;
                        // Set colours
                        if (show_debug){
                        DisplayOnConsole("purple","Found temp_directory: "+temp_dir+" in $"+var_name,true);}
                    }
                #endif
            }
            else
            {
                local_temp=true;
            }
        }
        else
        {
            DisplayOnConsole("red","Error: Missing setting 'UseLocalTemp'!");
        }

        if (settings.keyExists("FailPause"))
        {
            if (StringLower(settings.getValue("FailPause"))=="yes")
            {
                fail_pause=true;
                if (show_debug){
                DisplayOnConsole("yellow","[Notice] Failure pausing is active.",true);}
            }
            else
            {
                fail_pause=false;
            }
        }
        else
        {
            DisplayOnConsole("red","Error: Missing setting 'FailPause'!");
        }

        if (settings.keyExists("ConsoleColour"))
        {
            if (StringLower(settings.getValue("ConsoleColour"))=="yes")
            {
                allow_console_colour=true;
            }
            else
            {
                allow_console_colour=false;
            }
        }
        else
        {
            DisplayOnConsole("red","Error: Missing setting 'ConsoleColour'!");
        }

        if (settings.keyExists("VerboseIncludes"))
        {
            if (StringLower(settings.getValue("VerboseIncludes"))=="yes")
            {
                verbose_include_output=true;
            }
            else
            {
                verbose_include_output=false;
            }
        }
        else
        {
            DisplayOnConsole("red","Error: Missing setting 'VerboseIncludes'!");
        }

        if (settings.keyExists("VerboseParsing"))
        {
            if (StringLower(settings.getValue("VerboseParsing"))=="yes")
            {
                verbose_parse_output=true;
            }
            else
            {
                verbose_parse_output=false;
            }
        }
        else
        {
            DisplayOnConsole("red","Error: Missing setting 'VerboseParsing'!");
        }

        if (settings.keyExists("ShowIncludeText"))
        {
            if (StringLower(settings.getValue("ShowIncludeText"))=="yes")
            {
                show_include_text=true;
            }
            else
            {
                show_include_text=false;
            }
        }
        else
        {
            DisplayOnConsole("red","Error: Missing setting 'ShowIncludeText'!");
        }

        return 0;
    }
    else
    {
        // create settings file...
        DisplayOnConsole("normal","No settings file found, creating...",true);
        int make_settings_return = make_settings(runtime_system+directory_separator+"Settings - "+KERNEL_TYPE+".txt");
        cout << "Done." << endl;
        return make_settings_return;
    }
return 0;
}

int first_run()
{
    if (file_exists(runtime_system+directory_separator+"firstrun"))
    {
        return -1;
    }

    if (!file_exists(runtime_system+directory_separator+"Settings - "+KERNEL_TYPE+".txt"))
    {
        make_settings(runtime_system+directory_separator+"Settings - "+KERNEL_TYPE+".txt");
    }
    else
    {
        DisplayOnConsole("cyan","A settings file was already found, skipping making new settings...",true);
    }
    #ifdef _WIN32
        DisplayOnConsole("cyan","Add registry information for the runtime files? [y/N] ",false);
        string reg_ask("");
        cin >> reg_ask;
        if (StringUpper(reg_ask)=="Y")
        {
            readFile regfile;
            regfile.openFile(temp_directory+directory_separator+"ham.reg",readFile__write);
            regfile.writeLine("Windows Registry Editor Version 5.00");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\.ham]");
            regfile.writeLine("@=\"x-ham\"");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-ham]");
            regfile.writeLine("@=\"Executable HAM file\"");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-ham\\DefaultIcon]");
            regfile.writeLine("@=\"\\\""+replaceAll(program_directory+directory_separator+"HAM.ico","\\","\\\\")+"\\\"\"");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-ham\\shell]");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-ham\\shell\\open]");
            regfile.writeLine("@=\"Open with Ham Runtime\"");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-ham\\shell\\open\\command]");
            regfile.writeLine("@=\""+replaceAll(program_directory+directory_separator+HRE_executableName,"\\","\\\\")+" \\\"%1\\\"\"");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\.ha]");
            regfile.writeLine("@=\"x-ha\"");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-ha]");
            regfile.writeLine("@=\"Executable HAM archive\"");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-ha\\DefaultIcon]");
            regfile.writeLine("@=\"\\\"\\\"\"");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-ha\\shell]");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-ha\\shell\\open]");
            regfile.writeLine("@=\"Open with Ham Runtime\"");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-ha\\shell\\open\\command]");
            regfile.writeLine("@=\""+replaceAll(program_directory+directory_separator+HRE_executableName,"\\","\\\\")+" \\\"%1\\\"\"");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\.haminc]");
            regfile.writeLine("@=\"x-haminc\"");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-haminc]");
            regfile.writeLine("@=\"HAM include archive\"");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-haminc\\DefaultIcon]");
            regfile.writeLine("@=\"\\\"\\\"\"");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-haminc\\shell]");
            regfile.writeLine("");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-haminc\\shell\\open]");
            regfile.writeLine("@=\"Install as include\"");
            regfile.writeLine("[HKEY_CLASSES_ROOT\\x-haminc\\shell\\open\\command]");
            regfile.writeLine("@=\""+replaceAll(program_directory+directory_separator+HRE_executableName,"\\","\\\\")+" \\\"%1\\\"\"");
            regfile.close();
            system_call("regedit \""+temp_directory+directory_separator+"ham.reg"+"\"");
            file_delete(temp_directory+directory_separator+"ham.reg");
            DisplayOnConsole("red","A restart for explorer.exe is required to complete the setup!",true);
            cout << "Ok? [Y/n]: ";
            string explorerRestartAsk=getInput();
            cout << endl;
            if (StringLower(explorerRestartAsk)!="n")
            {
                system_call("taskkill /IM explorer.exe /F");
                wait(100);
                HWND hWnd = FindWindow(_T("Shell_TrayWnd"), NULL);
                if(SUCCEEDED(hWnd))
                {
                    PostMessage(hWnd, 0x5B4, 0, 0);
                    WinExec("C:\\Windows\\explorer.exe",SW_SHOWDEFAULT);
                    DisplayOnConsole("green","Restart complete!",true);
                }
                else
                {
                    DisplayOnConsole("red","The restart failed :( sorry!",true);
                }
            }
        }
    #endif

    #ifdef __linux__
        // dont run if on an arm device because it will be a mobile device which has no support for xdg-mime or .desktop files
        #ifndef __arm__
            DisplayOnConsole("cyan","Add a launcher file for the runtime? [y/N] ",false);
            string desktop_ask("");
            cin >> desktop_ask;
            if (StringUpper(desktop_ask)=="Y")
            {
                string usr_home("");
                const char * val = ::getenv( "HOME" );
                if ( val == 0 )
                {
                    DisplayOnConsole("red","Unable to find the users home directory!\nSkipping writing...",true);
                }
                else
                {
                    if (strcmp(val,"")==0)
                    {
                        DisplayOnConsole("red","The users home directory variable is blank! ($HOME)\nSkipping writing...",true);
                    }
                    else
                    {
                        usr_home = val;
                    }
                }

                if (usr_home!="")
                {
                    // check the directories that are meant to exist so there are no errors...
                    if (!directory_exists(usr_home+"/.local/share/applications"))
                    {
                        DirectoryCreate(usr_home+"/.local/share/applications");
                    }

                    if (!directory_exists(usr_home+"/.local/share/mime/packages"))
                    {
                        DirectoryCreate(usr_home+"/.local/share/mime/packages");
                    }

                    // create the .desktop file
                    readFile DesktopFile;
                    DesktopFile.openFile(usr_home+"/.local/share/applications/zaplots-"+HRE_version+".desktop",readFile__write);
                    DesktopFile.writeLine("[Desktop Entry]");
                    DesktopFile.writeLine("Encoding=UTF-8");
                    DesktopFile.writeLine("Version="+IntToString(version));
                    DesktopFile.writeLine("Type=Application");
                    DesktopFile.writeLine("Terminal=false");
                    DesktopFile.writeLine("Exec="+program_directory+directory_separator+HRE_executableName+" %f");
                    DesktopFile.writeLine("Name=Ham Runtime "+HRE_version);
                    DesktopFile.writeLine("Icon=zaplots-hamruntime-mainicon");
                    DesktopFile.writeLine("MimeType=application/x-ham;application/x-ha");
                    DesktopFile.close();
                    //system_call("mv \""+temp_directory+directory_separator+"zaplots-"+HRE_version+".desktop\" \""+usr_home+directory_separator+".local/share/applications/"+"zaplots-"+HRE_version+".desktop\"");
                    system_call("chmod +x \""+usr_home+directory_separator+".local/share/applications/"+"zaplots-"+HRE_version+".desktop\"");
                    system_call("update-mime-database ~/.local/share/mime");

                    if (file_exists(program_directory+directory_separator+"icon.png"))
                    {
                        system_call("xdg-icon-resource install --context apps --novendor --size 64 \""+program_directory+directory_separator+"icon.png\" zaplots-hamruntime-mainicon");
                    }
                    else
                    {
                        DisplayOnConsole("red","The file 'icon.png' does not exist in '"+program_directory+"'!",true);
                        DisplayOnConsole("red","Skipping the icon setting for the .desktop file.",true);
                    }

                    // set the mime type for the .ham and .ha files
                    readFile xmlmime;
                    xmlmime.openFile(temp_directory+directory_separator+"HRE6-ham.xml",readFile__write);
                    xmlmime.writeLine("<?xml version=\"1.0\"?>");
                    xmlmime.writeLine("<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>");
                    xmlmime.writeLine("\t<mime-type type=\"application/x-ham\">");
                    xmlmime.writeLine("\t\t<comment>Executable HAM File</comment>");
                    xmlmime.writeLine("\t\t<glob pattern=\"*.ham\"/>");
                    xmlmime.writeLine("\t</mime-type>");
                    xmlmime.writeLine("</mime-info>");
                    xmlmime.close();
                    system_call("xdg-mime install \""+temp_directory+directory_separator+"HRE6-ham.xml\"");
                    if (file_exists(program_directory+directory_separator+"HAM.png"))
                    {
                        system_call("xdg-icon-resource install --context mimetypes --size 48 --mode user --novendor \""+program_directory+directory_separator+"HAM.png"+"\" application-x-ham");
                    }
                    else
                    {
                        DisplayOnConsole("red","No ham icon file was found!",true);
                    }
                    file_delete(temp_directory+directory_separator+"HRE6-ham.xml");

                    xmlmime.openFile(temp_directory+directory_separator+"HRE6-ha.xml",readFile__write);
                    xmlmime.writeLine("<?xml version=\"1.0\"?>");
                    xmlmime.writeLine("<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>");
                    xmlmime.writeLine("\t<mime-type type=\"application/x-ha\">");
                    xmlmime.writeLine("\t\t<comment>Executable HAM Archive</comment>");
                    xmlmime.writeLine("\t\t<glob pattern=\"*.ha\"/>");
                    xmlmime.writeLine("\t</mime-type>");
                    xmlmime.writeLine("</mime-info>");
                    xmlmime.close();
                    system_call("xdg-mime install \""+temp_directory+directory_separator+"HRE6-ha.xml\"");
                    file_delete(temp_directory+directory_separator+"HRE6-ha.xml");

                    xmlmime.openFile(temp_directory+directory_separator+"HRE6-haminc.xml",readFile__write);
                    xmlmime.writeLine("<?xml version=\"1.0\"?>");
                    xmlmime.writeLine("<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>");
                    xmlmime.writeLine("\t<mime-type type=\"application/x-haminc\">");
                    xmlmime.writeLine("\t\t<comment>HAM Include Archive</comment>");
                    xmlmime.writeLine("\t\t<glob pattern=\"*.haminc\"/>");
                    xmlmime.writeLine("\t</mime-type>");
                    xmlmime.writeLine("</mime-info>");
                    xmlmime.close();
                    system_call("xdg-mime install \""+temp_directory+directory_separator+"HRE6-haminc.xml\"");
                    file_delete(temp_directory+directory_separator+"HRE6-haminc.xml");
                }
            }
            /*DisplayOnConsole("cyan","Add a link to the runtime in /usr/local/bin? [y/N] (requires root!): ",false);
            string link_ask("");
            cin >> link_ask;
            if (StringUpper(link_ask)=="Y")
            {
                system_call("sudo ln -s \""+program_directory+"/"+HRE_executableName+"\" /usr/local/bin/ham; sudo chmod 777 /usr/local/bin/ham");
            }*/
            DisplayOnConsole("cyan","Updating alternatives for ham-runtime...");
            system_call("sudo update-alternatives --install /usr/bin/ham ham-runtime \""+program_directory+"/"+HRE_executableName+"\" 900");
        #endif
    #endif

    readFile fr;
    fr.openFile(runtime_system+directory_separator+"firstrun",readFile__write);
    fr.close();

    DisplayOnConsole("green","First run setup complete!\n",true);
    return 0;
}

void sighandler(int sig)
{
    cout << endl;
    switch(sig)
    {
        case SIGINT:
        {
            DisplayOnConsole("red","Caught CTRL-C ("+IntToString(sig)+")!",true);
            quit.exit(EXIT_SUCCESS);
        }
        break;

        case SIGTERM:
        {
            DisplayOnConsole("red","Caught terminate signal!",true);
            quit.exit(EXIT_SUCCESS);
        }
        break;

        case SIGABRT:
        {
            DisplayOnConsole("red","Caught abort signal!",true);

            try
            {
                MY_THROW("Caught an abort signal!");
            }

            catch (ExceptionWithStack &e)
            {
                e.printStackTrace();
                // only use printf because Displayonconsole and cout /could/ be none functional after the crash
                printf("\nPlease post this stack trace as an error at https://github.com/VioletDarkKitty/hre6/issues\n");
                printf("If you are using a program, post the source too.\n\n");
            }
            con_pause();
            quit.exit(EXIT_SUCCESS);
        }
        break;

        case SIGHUP:
        {
            DisplayOnConsole("red","Controlling process terminated!",true);
            quit.exit(EXIT_SUCCESS);
        }
        break;

        default:
        {
            DisplayOnConsole("red","Caught unknown signal '"+IntToString(sig)+"'!",true);
        }
        break;
    }
}

void process_switches(int argc, char *argv[]);

int setup(int argc, char *argv[])
{
    quit.atexit(CleanupTemp);
    signal(SIGABRT, &sighandler);
	signal(SIGTERM, &sighandler);
	signal(SIGINT, &sighandler);
	signal(SIGHUP, &sighandler);
    prog_open=false;
    ChangeConsoleColour("normal");

    setTitle("Ham Runtime "+HRE_version+" - no program loaded.");
    program_directory = getProgramDirectory(CharToString(argv[0]));
    // environment setup
    string storage_root;
    if (file_exists(program_directory+directory_separator+".portable")){
        storage_root=program_directory;
    }
    else
    {
        #ifdef _WIN32
            TCHAR szPath[MAX_PATH];
            if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szPath)))
            {
                storage_root=szPath;
                storage_root+="\\ham";
            }
            else
            {
                storage_root=program_directory;
            }
        #elif __linux__
            struct passwd *pw = getpwuid(getuid());
            storage_root=CharToString(pw->pw_dir)+directory_separator+".ham";
        #else
            storage_root=CharToString(getenv("HOME"))+"/Library/Application Support/ham";
        #endif
        DirectoryCreate(storage_root);
    }
    temp_directory = storage_root+directory_separator+"TEMP";
    program_storage = storage_root+directory_separator+"STORAGE";
    runtime_system = storage_root+directory_separator+"SYSTEM";
    error_store = runtime_system+directory_separator+"ERROR";
    lib_store = runtime_system+directory_separator+"LIB";
    DirectoryCreate(temp_directory); // for storing temp data, changable in the settings
    DirectoryCreate(program_storage); // for storing program data in a place that does not get removed at close
    DirectoryCreate(runtime_system); // for storing librarys and runtime config files
    DirectoryCreate(error_store); // storage for error logs for programs loaded in the runtime
    DirectoryCreate(lib_store); // for storing external files (.so or .dll)

    // Runtime first run
    if (first_run()==0)
    {
        quit.exit(EXIT_SUCCESS);
    }

    int load_return;
    load_return=load_settings();
    if (load_return != 0)
    {
        DisplayOnConsole("red","Failed to load settings file!",true);
        con_pause();
        quit.exit(EXIT_FAILURE);
    }

    /*for (int i = 1; i < argc; ++i)
    {
        if (CharToString(argv[i]).length()!=0)
        {
            char temp[FILENAME_MAX];
            #ifdef WIN32
                _getcwd(temp,sizeof(temp));
            #else
                getcwd(temp,sizeof(temp));
            #endif
            //cout << i << endl;
            //cout << CharToString(temp)+directory_separator+argv[i] << endl;
            if (CharToString(argv[i]).at(0)=='.')
            {
                string path = CharToString(argv[i]);
                path.replace(0,1,"");
                string Stemp = CharToString(temp)+path;
                argv[i]=StringToChar(Stemp);
                //cout << argv[i] << endl;
            }
            else if (file_exists(CharToString(temp)+directory_separator+argv[i]))
            {
                argv[i]=StringToChar(CharToString(temp)+directory_separator+argv[i]);
            }
        }
    }*/

    #ifdef _WIN32
        HWND h = ::GetTopWindow(0 );
        DWORD pid=0;
        DWORD mypid = _getpid();
        //cout << mypid << endl;
        while ( h )
        {
            //cout << pid << endl;
            ::GetWindowThreadProcessId( h,&pid);
            if ( pid == mypid )
            {
                //cout << "matched!" << endl;
                HICON hIconSm;
                char* path = StringToChar(program_directory+directory_separator+"icon.ico");
                hIconSm = (HICON)LoadImage(NULL, path, IMAGE_ICON,
                32, 32, LR_LOADFROMFILE); delete[] path;
                HANDLE processHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
                SendMessage(h, WM_SETICON, ICON_SMALL, (LPARAM)hIconSm );
                //cout << h << endl;
                CloseHandle(processHandle);
                break;
            }
            h = ::GetNextWindow( h , GW_HWNDNEXT);

        }
    #endif

    process_switches(argc,argv);

    if (show_debug){
    cout << "Working in: " << program_directory << endl;}

    return 1;
}

int main(int argc, char *argv[])
{
    try {
        #ifdef __linux__
            // dont run on arm linux because it does not have the terminals we are searching for...
            #ifndef __arm__
                if (!isatty(0))
                {
                    bool open_terminal=true;
                    // the program is not open in a terminal
                    const char* args[argc+3], **it=args;
                    string term_name("");
                    if (file_exists("/usr/bin/gnome-terminal")){
                    *it++ = "gnome-terminal";
                    *it++ = "-x";
                    term_name="gnome-terminal";}else if (file_exists("/usr/bin/xterm")){
                    *it++ = "xterm";
                    *it++ = "-e";
                    term_name="xterm";}else if (file_exists("/usr/bin/lxterminal")){
                    *it++ = "lxterminal";
                    *it++ = "-e";
                    }else{open_terminal=false;}
                    if (open_terminal)
                    {
                        it = std::copy(argv, argv+argc, it);
                        *it++ = 0;

                        char* name = StringToChar(term_name);
                        if (-1 == execvp(name, (char* const*) &args[0]))
                            perror("exec");
                        delete[] name;
                    }
                }
            #endif
        #endif

        if (setup(argc, argv) != 1)
        {
            DisplayOnConsole("red","Error in function 'setup', the function returned a value other than 1!",true);
            con_pause(); // Renamed to avoid conflict with system pause
            exit(EXIT_FAILURE); // Causes application to terminate with error status
        }
        if (show_debug){
        DisplayOnConsole("green","Setup returned ok.",true);}

        switch(loader(argc,argv))
        {
            case 0:
                // fail
                DisplayOnConsole("red","Error in function 'loader', the function returned a fatal error value!",true);
                if (fail_pause == true)
                {
                    con_pause();
                }
                quit.exit(EXIT_FAILURE);
            break;

            case 1:
                // unsupported
                DisplayOnConsole("red","The file passed to the loader is currently unsupported!",true);
                if (fail_pause == true)
                {
                    con_pause();
                }
                quit.exit(EXIT_FAILURE);
            break;

            case 2:
                // unknown + unloadable
                DisplayOnConsole("red","Unknown and subsequently unloadable filetype '"+CharToString(argv[1])+"'!",true);
                if (fail_pause == true)
                {
                    con_pause();
                }
                quit.exit(EXIT_FAILURE);
            break;

            case 3:
                // the program is supported and recognised and *has* been run
                // we do not want to pause here, it is up to the program that has been run to pause
                // because we do not have any errors that need showing.
                quit.exit(EXIT_SUCCESS);
            break;
        }
    } catch(exitHelper::exit_exception& e) {
        quit.call(); // call functions that were registered to be called on exit.
        return e.c; // calling return fixes all of the memory leak problems!
    }
}

