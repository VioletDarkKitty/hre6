#include <iostream>
#include <istream>
#include <fstream>
#include "functions.h"
#ifdef _WIN32
    #include "Shlwapi.h"
    #include <windows.h>
    #include <direct.h>
    // _mkdir for windows
    #include <process.h>
    #include <errno.h>
#else
    #include <sys/stat.h>
    // mkdir for unix
    #include <sys/wait.h>
#endif
#include <cctype>
#include <sstream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iomanip>
#include <cstdio>
#include <algorithm>
#include <dirent.h>
#include "globals.h"
#include <limits.h>
#include <vector>

using namespace std;

void con_pause()
{
    cin.clear();
	//cout << "Press ENTER to continue." << endl;
	//cin.ignore();
	cout << endl << "Press ENTER to continue...";
	//cin.ignore(numeric_limits<streamsize>::max(), '\n');
    cin.ignore(cin.rdbuf()->in_avail()+1);
    //cin.get();
}

bool file_exists(string filename)
{
    /*ifstream Ifile(StringToChar(filename));
    if (Ifile.is_open())
    {
        Ifile.close();
        return true;
    }
    Ifile.close();
    return false;
    */
    #ifdef _WIN32
        DWORD fileAtt = GetFileAttributesA(StringToChar(filename));
        return ( ( fileAtt & FILE_ATTRIBUTE_DIRECTORY ) == 0 );
    #else
        struct stat buf;
        return (stat(filename.c_str(), &buf) == 0);
    #endif
}

int file_delete(string f)
{
    if (file_exists(f))
    {
        char* path = StringToChar(f);
        remove(path);
        delete[] path;
        return 1;
    }
return 0;
}

bool directory_exists(string dirname)
{
    /*string dirF(dirname+directory_separator+"(0)");
    ofstream Ofile(StringToChar(dirF));
    Ofile.close();
    if (file_exists(dirF)==true)
    {
        file_delete(dirF);
        return true;
    }
    file_delete(dirF);
    return false;*/
    #ifdef _WIN32
        DIR *pDir;
        bool bExists = false;

        pDir = opendir (StringToChar(dirname));

        if (pDir != NULL)
        {
            bExists = true;
            (void) closedir (pDir);
        }
        return bExists;
    #else
        struct stat st;
        char* path = StringToChar(dirname);
        if(stat(path,&st) == (0))
        {
            delete[] path;
            if((st.st_mode & S_IFDIR) != (0)){return true;}
        }
        else
        {
            delete[] path;
        }
        return false;
    #endif
}

void setTitle(string titleMsg)
{
#ifdef _WIN32
    SetConsoleTitle(StringToChar(titleMsg));
#else
    char esc_start[] = { 0x1b, ']', '0', ';', 0 };
    char esc_end[] = { 0x07, 0 };
    cout << esc_start << titleMsg << esc_end;
#endif
}

string GetFileExtension(string Fpath)
{
    return Fpath.substr(Fpath.find_last_of(".") + 1);
}

string GetFileName(string Fpath)
{
    return Fpath.substr(Fpath.find_last_of(directory_separator)+1);
}

string GetPath(string Fpath)
{
    if (Fpath!="")
    {
        return Fpath.replace((Fpath.end()-(GetFileName(Fpath).length()+1)),Fpath.end(),"");
    }
    else
    {
        return "";
    }
}

string ChangeFileExtention(string str,string RPL)
{
    if (str!="")
    {
        return str.replace((str.end()-(GetFileExtension(str).length()+1)),str.end(),RPL);
    }
return "";
}

string StringUpper(string s)
{
    if (s.length()==1)
    {
        if (s.at(0)==' ')
        {
            return " ";
        }
    }
    transform(s.begin(), s.end(), s.begin(), ::toupper);
    return s;
}

string StringLower(string s)
{
    if (s.length()==1)
    {
        if (s.at(0)==' ')
        {
            return " ";
        }
    }
    transform(s.begin(), s.end(), s.begin(), ::tolower);
    return s;
}

char* StringToChar(string s)
{
    char *a=new char[s.size()+1];
    a[s.size()]=0;
    memcpy(a,s.c_str(),s.size());
    return a;
}

string CharToString(char c)
{
    stringstream ss;
    string s;
    ss << c;
    ss >> s;
    return s;
}

string CharToString(char* c)
{
    if (c != NULL)
    {
        string returnstring;
        returnstring = c;
        return returnstring;
    }
return "";
}

string IntToString(int n)
{
    stringstream ss;
    string s;
    ss << n;
    ss >> s;
    return s;
}

string FloatToString(float n)
{
    stringstream ss;
    string s;
    ss << n;
    ss >> s;
    return s;
}

string DoubleToString(double n)
{
    std::ostringstream strs;
    strs << n;
    return strs.str();
}

int StringToInt(string str)
{
    int value = atoi(str.c_str());
    return value;
}

float StringToFloat(string str)
{
    float value = atof(str.c_str());
    return value;
}

double StringToDouble( const std::string& s )
 {
   std::istringstream i(s);
   double x;
   if (!(i >> x))
     return 0;
   return x;
 }

 double IntToDouble(int a)
 {
    string b(IntToString(a));
    double c = StringToDouble(b);
    return c;
 }

void ChangeConsoleColour(string col)
{
    #ifdef _WIN32
        //int BLACK = 0;
        int BLUE = 1;
        int GREEN = 2;
        int CYAN = 3;
        int RED = 4;
        int MAGENTA = 5;
        //int BROWN = 6;
        //int LIGHTGREY = 7;
        //int DARKGREY = 8;
        //int LIGHTBLUE = 9;
        //int LIGHTGREEN = 10;
        //int LIGHTCYAN = 11;
        //int LIGHTRED = 12;
        //int LIGHTMAGENTA = 13;
        int YELLOW = 14;
        int WHITE = 15;

        col = StringLower(col);
        if (col == "red")
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), RED);
        }
        else if (col == "blue")
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), BLUE);
        }
        else if (col == "white")
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), WHITE);
        }
        else if (col == "green")
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), GREEN);
        }
        else if (col == "yellow")
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), YELLOW);
        }
        else if (col == "purple")
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), MAGENTA);
        }
        else if (col == "cyan")
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), CYAN);
        }
        else if (col == "normal")
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), WHITE);
        }
        else
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), WHITE);
        }
    #else
        std::string KNRM("\x1B[0m"); // normal
        std::string KRED("\x1B[31m"); // red
        std::string KGRN("\x1B[32m"); // green
        std::string KYEL("\x1B[33m"); // yellow
        std::string KBLU("\x1B[34m"); // blue
        std::string KMAG("\x1B[35m"); // magenta (purple)
        std::string KCYN("\x1B[36m"); // cyan (light blue)
        std::string KWHT("\x1B[37m"); // white (normal)

        col = StringLower(col);
        if (col == "red")
        {
            cout << KRED;
        }
        else if (col == "blue")
        {
            cout << KBLU;
        }
        else if (col == "white")
        {
            cout << KWHT;
        }
        else if (col == "green")
        {
            cout << KGRN;
        }
        else if (col == "yellow")
        {
            cout << KYEL;
        }
        else if (col == "purple")
        {
            cout << KMAG;
        }
        else if (col == "cyan")
        {
            cout << KCYN;
        }
        else if (col == "normal")
        {
            cout << KNRM;
        }
        else
        {
            cout << KNRM;
        }
    #endif
}

void DirectoryCreate(string dir)
{
    char* path = StringToChar(dir);
    #ifdef _WIN32
        _mkdir(path);
    #else
        mkdir(path,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    #endif
    delete[] path;
}

int directory_remove(string d)
{
    #ifdef _WIN32
        if (directory_exists(d))
        {
            RemoveDirectory(StringToChar(d));
            return 1;
        }
        return 0;
    #else
        if (directory_exists(d))
        {
            char* path = StringToChar(d);
            remove(path);
            delete[] path;
            return 1;
        }
        return 0;
    #endif
}

void con_clear()
{
    #ifdef _WIN32
        system_call("cls");
    #else
        printf("\033[0m\033[2J\033[H");
    #endif
}

void wait(int ms)
{
    #ifdef _WIN32
        Sleep(ms);
    #else
        usleep( ms * 1000 );
    #endif
}

#ifdef NOEXEC
void execute_program(char* programPath, bool finish_wait)
{
#ifdef WIN32
    SHELLEXECUTEINFO shExecInfo;

    shExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);

    shExecInfo.hwnd = NULL;
    //shExecInfo.lpVerb = L"runas";
    shExecInfo.lpVerb = NULL;
    shExecInfo.lpFile = programPath;
    shExecInfo.lpParameters = NULL;
    shExecInfo.lpDirectory = NULL;
    shExecInfo.nShow = SW_NORMAL;
    shExecInfo.hInstApp = NULL;

    ShellExecuteEx(&shExecInfo);
#else
    pid_t pid = fork(); /* Create a child process */

    switch (pid)
    {
        case -1: /* Error */
            std::cerr << "Error: fork() failed.\n";
            exit(1);
        case 0: /* Child process */
            execl(programPath, "", NULL); /* Execute the program */
            std::cerr << "Error: execl() failed!"; /* execl doesn't return unless there's an error */
            exit(1);
        default: /* Parent process */
            std::cout << "Process created with pid " << pid << "\n";
            int* status;

            if (finish_wait == true)
            {
                while (!WIFEXITED(status))
                {
                    waitpid(pid, status, 0); /* Wait for the process to complete */
                }
                std::cout << "Process exited with " << WEXITSTATUS(status) << "\n";
            }
    }
#endif
}
#endif


bool directory_delete_recursive(string dir)
{
    //cout << "deleting " << dir << endl;
    // delete all the files and folders in dir;
    struct dirent *de=NULL;
    DIR *d=NULL;
    char* path = StringToChar(dir);
    d=opendir(path);
    delete[] path;
    if(d == NULL)
    {
        return false;
    }
    else
    {
        // Loop while not NULL
        while( (de = readdir(d)) )
        {
            //printf("%s\n",de->d_name);
            string d_name("");
            d_name = d_name + de->d_name;
            //cout << d_name << endl;
            if ( d_name == "." or d_name == "..")
            {
                // do nothing, we found the directory above!
                // . = current dir; .. = above dir;
            }
            else
            {
                if (directory_exists(dir+directory_separator+d_name))
                {
                    //cout << "found dir " << d_name << endl;
                    // it is a directory... delete
                    directory_delete_recursive(dir+directory_separator+d_name);
                    directory_remove(dir+directory_separator+d_name);
                }
                else if (file_exists(dir+directory_separator+d_name))
                {
                    //cout << "is a file " << endl;
                    file_delete(dir+directory_separator+d_name);
                }
                else
                {
                    //cout << "is nothing" << endl;
                    directory_remove(dir);
                    return false;
                }
            }
        }
        closedir(d);
        directory_remove(dir);
        return true;
    }
}

/*int CurlDownload(char *url, std::string filename)
{
    #define CURL_STATICLIB
    #include <curl/curl.h>
    #include <curl/easy.h>
    ///---------///
    size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream)
    {
        size_t written;
        written = fwrite(ptr, size, nmemb, stream);
        return written;
    }
    ///---------///
    CURL *curl;
    FILE *fp;
    CURLcode res;
    char outfilename = StringToChar(filename);
    curl = curl_easy_init();
    if (curl) {
        fp = fopen(outfilename,"wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        fclose(fp);
    }
    /// end of download
    int result = res;
    return result;
}
*/

std::string replaceAll(std::string subject, const std::string& search, const std::string& replace)
{
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
    return subject;
}

string replaceString(string subject, const string& search, const string& replace)
{
    size_t pos = subject.find(search, 0);
    if (pos==string::npos){return subject;}
    subject.replace(pos,search.length(),replace);
    return subject;
}

string replaceString(string subject, const unsigned int pos1, const unsigned int pos2, const string& replace)
{
    if ((pos1 < pos2)||(pos1 > subject.length())||(pos2 > subject.length())){return subject;}
    subject.replace(pos1,pos2-pos1,replace);
    return subject;
}

bool hasEnding (std::string const &fullString, std::string const &ending)
{
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

void pack_file(readFile &FilePointer, string IncludeFile)
{
    // write a delimiter for the open of a file
    FilePointer.writeLine("**Fopen_DELIM**");

    // begin to write the entire file into the file
    readFile IFO;
    IFO.openFile(StringToChar(IncludeFile),0);
    string Iline;
    while(IFO.eof()!=true)
    {
        Iline = IFO.readLine();
        //if (!IFO.eof())
        //{
            FilePointer.writeLine(Iline);
        //}
    }
    IFO.close();

    // write a delimiter for the closing of a file
    FilePointer.writeLine("**Fclose_DELIM**");
}

int unpack_file(readFile &FilePointer, string OutputFile)
{
    // skip over the demimiter for the open of the file, return -1 if it is not a delimiter
    string Iline;
    Iline = FilePointer.readLine();
    if (Iline != "**Fopen_DELIM**")
    {
        return -1;
    }

    // read the entire file into the output file
    readFile IFO;
    IFO.openFile(OutputFile,1);
    int OpenCount = 0;
    while(Iline != "**Fclose_DELIM**")
    {
        Iline = FilePointer.readLine();
        if (Iline == "**Fopen_DELIM**")
        {
            OpenCount++;
        }
        if (Iline == "**Fclose_DELIM**" && OpenCount > 0)
        {
            OpenCount--;
            IFO.writeLine(Iline);
        }
        if (Iline != "**Fclose_DELIM**")
        {
            IFO.writeLine(Iline);
        }
    }
    IFO.close();
    if (OpenCount > 0)
    {
        return -1;
    }
    else
    {
        return 0;
    }
}

string atPlace(string &inputString, int place)
{
    int signed maxplace = inputString.length()-1;
    if (place > maxplace)
    {
        cout << "Error in atPlace, " << place << " is bigger than the length (" << (inputString.length()) << ")!" << endl;
        exit(EXIT_FAILURE);
    }
    else
    {
        return CharToString(inputString.at(place));
    }
return "";
}

bool isAnyOf(string input, vector<string> &check)
{
    //cout << input << endl;
    if (input.length() == 0) { return false; }
    if (check.size() == 0) { return false; }
    unsigned int total_matches_found=0;
    for (int i=0; (unsigned)i != input.length(); i++)
    {
        unsigned int total_matches_found_old = total_matches_found;
        for (unsigned int ii=0; ii != check.size(); ii++)
        {
            //cout << total_matches_found << "/" << input.length() << "|" << input.at(i) << "|" << check[ii] << endl;
            if (CharToString(input.at(i)) == check[ii]){
            total_matches_found++; /*(cout << "Break because a match was found!" << endl;*/ break;}
        }
        if (total_matches_found == total_matches_found_old) { /*cout << "Break because no match!" << endl;*/ break; }
    }

if (total_matches_found!=input.length()){
        return false;
    }
return true;
}

bool isNumber(string in)
{
    vector<string> number_vector;
    number_vector.push_back("0");
    number_vector.push_back("1");
    number_vector.push_back("2");
    number_vector.push_back("3");
    number_vector.push_back("4");
    number_vector.push_back("5");
    number_vector.push_back("6");
    number_vector.push_back("7");
    number_vector.push_back("8");
    number_vector.push_back("9");
    number_vector.push_back("(");
    number_vector.push_back(")");
    number_vector.push_back(".");
    number_vector.push_back(CharToString(' '));
    number_vector.push_back("+");
    number_vector.push_back("-");
    number_vector.push_back("*");
    number_vector.push_back("/");

    if (isAnyOf(in,number_vector))
    {
        return true;
    }
return false;
}

void DisplayOnConsole(string colour, string text, bool withNewline)
{
    if (allow_console_colour==true)
    {
        ChangeConsoleColour(colour);
    }
    cout << text;
    if (withNewline == true)
    {
        cout << endl;
    }
    if (allow_console_colour==true)
    {
        ChangeConsoleColour("normal");
    }
}

/*std::string trim(const std::string& str,
                 const std::string& whitespace = "\t")
{
    const int strBegin = str.find_first_not_of(whitespace);
    if ((unsigned)strBegin == std::string::npos)
        return ""; // no content

    const int strEnd = str.find_last_not_of(whitespace);
    const int strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}*/
// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

string removeLeadingWhitespace(string Iline)
{
    //if (show_debug) {cout << "Removing leading whitespace for '" << Iline << "'" << endl;}
    /*if (Iline.length() == 0){return "";}
    if (Iline.length() == 1){return Iline;}
    if (Iline==" "){return "";}
    if (Iline=="\t"){return "";}
    if (Iline=="~"){return "";}

    Iline.erase(Iline.begin(), std::find_if(Iline.begin(), Iline.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    Iline=trim(Iline);
    Iline.erase(remove(Iline.begin(), Iline.end(), '\t'), Iline.end());*/
    ltrim(Iline);
return Iline;
}

string removeTrailingWhitespace(string Iline)
{
    //if (show_debug) {cout << "Removing trailing whitespace for '" << Iline << "'" << endl;}
    /*if (Iline.length() == 0){return "";}
    if (Iline.length() == 1){return Iline;}
    if (Iline==" "){return "";}
    if (Iline=="\t"){return "";}
    if (Iline=="~"){return "";}

    Iline.erase(std::find_if(Iline.rbegin(), Iline.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), Iline.end());
    Iline=trim(Iline);*/
    rtrim(Iline);
return Iline;
}

#ifdef _WIN32
    bool WinIs64()
    {
        if (directory_exists("C:\\Program Files (x86)"))
        {
            return true;
        }
    return false;
    }

    bool IsWow64()
    {
        #ifdef _WIN64
            return true;
        #else
            return false;
        #endif
    }
#endif

void system_call(string __command)
{
    /*#ifdef WIN32
        intptr_t systemRet = _execvpe(StringToChar(__command), NULL, NULL);
        switch (systemRet)
        {
            case ENOENT:
            {
                DisplayOnConsole("red","File or path not found for: '"+__command+"'!.");
                exit(EXIT_FAILURE);
                break;
            }

            case ENOEXEC:
            {
                DisplayOnConsole("red","File not executable or invalid executable format for: '"+__command+"'!.");
                exit(EXIT_FAILURE);
                break;
            }

            case EACCES:
            {
                DisplayOnConsole("red","Sharing violation for: '"+__command+"'!.");
                exit(EXIT_FAILURE);
                break;
            }
        }
    #else
        if (clearenv() != 0) {
            DisplayOnConsole("red","Failed to sanitise the environment!");
            exit(EXIT_FAILURE);
        }*/
        int systemRet = system(StringToChar(__command));

        if (systemRet == -1)
        {
            // failed to call
            DisplayOnConsole("red","Failed to execute system call: '"+__command+"'!");
            exit(EXIT_FAILURE);
        }
    //#endif
}

void file_copy(string source, string dest)
{
    #ifdef _WIN32
        system_call("copy \""+source+"\" \""+dest+"\"");
    #else
        system_call("cp \""+source+"\" \""+dest+"\"");
    #endif
}

int GetCount(string s, string of) {
  if (s.length()==0){return 0;}
  int count = 0;

  for (unsigned int i = 0; i < s.size(); i++)
    if (CharToString(s[i]) == of) count++;

  return count;
}

string getInput()
{
    int declare_length = CHAR_MAX;
    char input_temp[declare_length];
    fgets(input_temp,declare_length,stdin); strncpy(strstr(input_temp,"\n"),"",2);
    return CharToString(input_temp);
}

// http://www.infernodevelopment.com/perfect-c-string-explode-split
void StringExplode(string str, string separator, vector<string>* results)
{
    int unsigned found;
    found = str.find_first_of(separator);
    while(found != string::npos){
        if(found > 0){
            results->push_back(str.substr(0,found));
        }
        str = str.substr(found+1);
        found = str.find_first_of(separator);
    }
    if(str.length() > 0){
        results->push_back(str);
    }
}

void StringExplode(string str, string separator, vector<string>& results)
{
    int unsigned found;
    found = str.find_first_of(separator);
    while(found != string::npos){
        if(found > 0){
            results.push_back(str.substr(0,found));
        }
        str = str.substr(found+1);
        found = str.find_first_of(separator);
    }
    if(str.length() > 0){
        results.push_back(str);
    }
}

string StringImplode(vector<string> input, string seperator)
{
    string output("");
    for(int unsigned i=0; i!=input.size(); i++)
    {
        if (output!=""){output+=seperator;}
        output+=input[i];
    }
    return output;
}

bool dirEmpty(string path)
{
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (StringToChar(path))) != NULL)
    {
        string S_ent("");
        readFile archiveOut;
        while ((ent = readdir (dir)) != NULL)
        {
            S_ent = ent->d_name;
            if (S_ent != ".")
            {
                if (S_ent != "..")
                {
                    return false;
                }
            }
        }
        closedir(dir);
    }
    return true;
}
