#ifndef HAM_VARS_H
#define HAM_VARS_H

#include <map>
#include <string>
#include <vector>
#include "../readFile.h"
#include <iostream>
using namespace std;

class ham_vars
{
    public:
        ham_vars();
        virtual ~ham_vars();
        enum typeBitmasks
        {
            _stringI = 1 << 0,
            _stringIliteral = 1 << 1,
            _realI = 1 << 2,
            _numericalLiteral = 1 << 3,
            _prealI = 1 << 4,
            _fileI = 1 << 5,
            _functionI = 1 << 6,
            _boolI = 1 << 7
        };
        typedef map <string, readFile*> FileStorageMap;
        FileStorageMap parse_files;
        typedef std::map<string, std::pair<string, std::pair<string,
        std::pair<bool, std::pair<int, std::pair<bool, std::string> > > > > > VariableStorageMap;
        VariableStorageMap parse_variables;
        typedef map <string, std::pair<vector <string>, map<string, string> > > FunctionsStorageMap;
        FunctionsStorageMap parse_functions;

        void create(string name, string type, string value, bool global, int brace_level, bool _const, string defineInformation="system");
        void destroy(string varname, int line_number, string filename);
        void cleanScope(int brace_level, int line_number, string filename);
        bool exists(string varname);
        string typeof_(string varname);
        bool isType(string varname, int type, int line_number, string filename);
        void expectType(string varname, int type, int line_number, string filename);
        string value(string varname);
        string getDefineLocation(string name);
        void stripLiteralQuotes(string &varname);
        bool isConst(string varname);
        void set(string varname, string value);
        void expectDirectional(string input, int line_number, string filename);
        void setType(string name, string type);
        void setConst(string name, bool _const);
        bool isGlobal(string name);
        int getDeclaredBrace(string name);
        //// functions below need a rewrite ////
        void parse_functions_setInfo(string name, int line_number, string filename);
        void parse_functions_add(string name, string value);
    protected:
    private:
        void parse_variables_setValue(string name, string value);
        int parse_variables_whereDeclared(string name);
        void parse_variables_setWhereDeclared(string name, int line_number);
        void parse_variables_setGlobal(string name, bool global);
        void parse_variables_setDefineLocation(string name, string defined);
};

#endif // HAM_VARS_H
