#ifndef CONDITIONAL_H
#define CONDITIONAL_H
#include <vector>
#include <string>
#include "ham_vars.h"
extern ham_vars varmap;

using namespace std;
class conditional
{
    public:
        conditional();
        virtual ~conditional();
        bool check(vector<string> condition, int line_number, string filename, string command);
    protected:
    private:
};

#endif // CONDITIONAL_H
