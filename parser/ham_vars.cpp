#include "ham_vars.h"
#include "common.h"
#include "../parseLine.h"
using namespace std;

ham_vars::ham_vars()
{
    //ctor
}

ham_vars::~ham_vars()
{
    //dtor
}

void ham_vars::parse_variables_setValue(string name, string value)
{
    parse_variables[name].second.first=value;
}

void ham_vars::parse_variables_setGlobal(string name, bool global)
{
    parse_variables[name].second.second.first=global;
}

bool ham_vars::isGlobal(string name)
{
    return parse_variables[name].second.second.first;
}

int ham_vars::getDeclaredBrace(string name)
{
    return parse_variables[name].second.second.second.first;
}

void ham_vars::parse_variables_setWhereDeclared(string name, int line_number)
{
    parse_variables[name].second.second.second.first=line_number;
}

void ham_vars::parse_variables_setDefineLocation(string name, string defined)
{
    parse_variables[name].second.second.second.second.second=defined;
}

bool ham_vars::exists(string varname)
{
    return (parse_variables.count(varname)==1);
}

string ham_vars::typeof_(string varname)
{
    return parse_variables[varname].first;
}

bool ham_vars::isType(string varname, int type, int line_number, string filename)
{
    // expect only one type...
    if (type==ham_vars::_stringI)
    {
        if (parse_is_variable(varname,line_number,filename))
        {
            if (typeof_(varname)=="string")
            {
                return true;
            }
        }
    return false;
    }
    else if (type==ham_vars::_realI)
    {
        if (varname.length()!=0)
        {

            if (varname.at(0)=='(')
            {
                string working = varname;
                working.replace(0,1,"");
                working.replace(working.length()-1,working.length(),"");
                //cout << working << endl;

                vector<string>new_line = parseLine(working,line_number,filename);
                bool is_all_real=true;
                for (unsigned int i=0; i!=new_line.size(); i++)
                {
                    if (new_line[i]=="") { continue; }
                    if (parse_is_variable(new_line[i],line_number,filename))
                    {
                        if (!isType(new_line[i],ham_vars::_realI,line_number,filename))
                        {
                            is_all_real=false;
                        }
                    }
                    else
                    {
                        is_all_real=false;
                    }
                    if (!is_all_real) {break;}
                }
                if (is_all_real){return true;}else{return false;}
            }
            else
            {
                if (parse_is_variable(varname,line_number,filename))
                {
                    if (typeof_(varname)=="real")
                    {
                        return true;
                    }
                }
            }

        }
    return false;
    }
    else if (type==ham_vars::_prealI)
    {
        if (parse_is_variable(varname,line_number,filename))
        {
            if (typeof_(varname)=="preal")
            {
                return true;
            }
        }
    return false;
    }
    else if (type==ham_vars::_fileI)
    {
        if (parse_is_variable(varname,line_number,filename))
        {
            if (typeof_(varname)=="file")
            {
                return true;
            }
        }
    return false;
    }
    else if (type==ham_vars::_functionI)
    {
        if (parse_is_variable(varname,line_number,filename))
        {
            if (typeof_(varname)=="function")
            {
                return true;
            }
        }
    return false;
    }
    else if (type==ham_vars::_boolI)
    {
        if (parse_is_variable(varname,line_number,filename))
        {
            if (typeof_(varname)=="bool")
            {
                return true;
            }
        }
    return false;
    }

    return false;
}

// http://stackoverflow.com/questions/18841113/c-assigning-enums-explicit-values-using-bit-shifting
void ham_vars::expectType(string varname, int type, int line_number, string filename)
{
    vector<string> expected;
    bool pass=false; // set to true if ANY of the requested types are the same as the type of the var
    bool nonDefineError=false;

    if (type & ham_vars::_stringI)
    {
        expected.push_back("string");
        if (exists(varname)){
            if(isType(varname,ham_vars::_stringI,line_number,filename)){pass=true;}
        }else{nonDefineError=true;}
    }

    if (type & ham_vars::_stringIliteral)
    {
        expected.push_back("quoted literal");
        if (!parse_is_variable(varname,line_number,filename)){
            if (parse_is_quoted(varname)){pass=true;}
        }
    }

    if (type & ham_vars::_realI)
    {
        expected.push_back("real");
        if (exists(varname)){
            if(isType(varname,ham_vars::_realI,line_number,filename)){pass=true;}
        }else{nonDefineError=true;}
    }

    if (type & ham_vars::_numericalLiteral)
    {
        expected.push_back("numberical literal");
        if (!parse_is_variable(varname,line_number,filename)){
            if(isNumber(varname)){pass=true;}
        }
    }

    if (type & ham_vars::_prealI)
    {
        expected.push_back("preal");
        if (exists(varname)){
            if(isType(varname,ham_vars::_prealI,line_number,filename)){pass=true;}
        }else{nonDefineError=true;}
    }

    if (type & ham_vars::_fileI)
    {
        expected.push_back("file");
        if (exists(varname)){
            if(isType(varname,ham_vars::_fileI,line_number,filename)){pass=true;}
        }else{nonDefineError=true;}
    }

    if (type & ham_vars::_functionI)
    {
        expected.push_back("preal");
        if (exists(varname)){
            if(isType(varname,ham_vars::_functionI,line_number,filename)){pass=true;}
        }else{nonDefineError=true;}
    }

    if (type & ham_vars::_boolI)
    {
        expected.push_back("preal");
        if (exists(varname)){
            if(isType(varname,ham_vars::_boolI,line_number,filename)){pass=true;}
        }else{nonDefineError=true;}
    }

    if (show_debug){
        cout << "Varname: " << varname << endl << "Exists: " << exists(varname) << endl << "nonDefineError flag: " << nonDefineError
        << endl << "pass flag: " << pass << endl;
    }

    if (!pass){
        // build expectedTypes list
        string expectedTypes;
        for(unsigned int i=0; i!=expected.size(); i++)
        {
            string delim;
            if (i==expected.size()-2){delim=" or ";}else if (i==expected.size()-1){delim="";}else{delim=", ";}
            expectedTypes+=expected.at(i)+delim;
        }

        if (nonDefineError){
            parse_error("Error: "+varname+" is not defined as a variable! Expected as one of the following: "+expectedTypes+"!",line_number,filename);
        }
        else
        {
            if (exists(varname)){
                parse_error("Error: "+varname+" was expected to be one of the following types "+expectedTypes+"! Actual type is "+typeof_(varname),line_number,filename);
            }
        }
    }
}

string ham_vars::value(string varname)
{
    return parse_variables[varname].second.first;
}

string ham_vars::getDefineLocation(string name)
{
    return parse_variables[name].second.second.second.second.second;
}

void ham_vars::stripLiteralQuotes(string &varname)
{
    /* Strip the quotes from a var if it has one otherwise return the value of the variable */
    if (varmap.exists(varname)){varname=varmap.value(varname);}
    else{if(parseHasBeginQuote(varname)&&parseHasEndQuote(varname)) {varname=remove_quotes(varname);}}
}

bool ham_vars::isConst(string varname)
{
    return parse_variables[varname].second.second.second.second.first;
}

void ham_vars::set(string varname, string value)
{
    parse_variables_setValue(varname, value);
}

void ham_vars::expectDirectional(string input, int line_number, string filename)
{
    if (input!="->")
    {
        parse_error("Error: Expected directional operator!",line_number,filename);
    }
}

void ham_vars::setType(string name, string type)
{
    parse_variables[name].first=type;
}

void ham_vars::setConst(string name, bool _const)
{
    parse_variables[name].second.second.second.second.first=_const;
}

void ham_vars::destroy(string varname, int line_number, string filename)
{
    // destroy the variable
    ham_vars::VariableStorageMap::iterator itr(parse_variables.find(varname));
    if (itr==parse_variables.end()){
        parse_error("Error: Destruction iterator reached the end of the stack before finding the key!",line_number,filename);
    }

    if (typeof_(varname)=="file")
    {
        readFile *ptr = parse_files[varname];
        ptr->close(); parse_files[varname]=ptr;
        delete parse_files[varname];
        parse_files.erase(varname);
    }
    else if (typeof_(varname)=="function")
    {
        parse_error("Error: Functions may not be destroyed!",line_number,filename);
    }
    parse_variables.erase(itr);
    if (show_debug) {
        if (parse_variables.count(varname)==0){
            cout << "Destruction successful." << endl;
        }
        else
        {
            cout << "Destruction failed!" << endl;
        }
    }
}

//////////////////////////////////////
//////////////////////////////////////
//////////////////////////////////////

void ham_vars::parse_functions_setInfo(string name, int line_number, string filename)
{
    parse_functions[name].second["ln"]=IntToString(line_number);
    parse_functions[name].second["fn"]=filename;
}

void ham_vars::parse_functions_add(string name, string value)
{
    //(parse_functions[name]).push_back(value);
    parse_functions[name].first.push_back(value);
}

void ham_vars::create(string name, string type, string value, bool global, int brace_level, bool _const, string defineInformation)
{
    setConst(name,_const);
    setType(name,type);
    parse_variables_setGlobal(name,global);
    parse_variables_setValue(name,value);
    parse_variables_setWhereDeclared(name,brace_level);
    parse_variables_setDefineLocation(name,defineInformation);
    //cout << name << ": " << type << " " << value << " at BL: " << brace_level << endl;
}

void ham_vars::cleanScope(int brace_level, int line_number, string filename)
{
    // remove all variables from parse_var_typing which are declared in parse_var_declared that match the brace level
    for(VariableStorageMap::const_iterator i = parse_variables.begin(); i != parse_variables.end(); /*noinc*/)
    {
        string key = i->first;
        if ( (getDeclaredBrace(key)==brace_level) && (!isGlobal(key)) )
        {
            //remove from map
            if (show_debug){
            DisplayOnConsole("normal","Purging '"+key+"' from variables map for BL '"+IntToString(brace_level)+"'...");
            }
            i++;
            destroy(key,line_number,filename);
        }
        else
        {
            i++;
        }
    }
}

