#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

#include <string>
#include <vector>
#include <map>
#include "../readFile.h"
#include "../functions.h"
#include "../globals.h"
#include <cstdlib>
#include "../parseLine.h"
#include <iostream>
#include "../decimal.h"
using namespace dec;
#define prealDecimalCount 12
#include "ham_vars.h"
#include "conditional.h"

using namespace std;
//int read(string F_in, bool doBegin=true, bool fileIsInclude=false, string FilenameCallStack="");
//string CheckOK(string CK);

void parse_error(string error, int line_number, string filename);
void parse_warn(string warning, int line_number, string filename);
bool parseIsLegalVarDeclare(string &s);
string remove_quotes(string input);
bool parseIsBracketedExpression(string input, int line_number, string filename);
bool parseIsLegalVarDeclare(string &s);
bool isValidMathsOperator(string op);
string expandBracketedExpression(string exp, int line_number, string filename);
void outputRuntimeState();
bool isValidDataType(string type);
bool parseHasBeginQuote(string &p2);
bool parseHasEndQuote(string &p2);
bool parse_is_quoted(string &p2);
void parse_arg_check(vector<string> &line, int line_number, int unsigned expected_size, string command, string filename);
void parse_arg_check_atleast(vector<string> &line, int line_number, int unsigned expected_size, string command, string filename);
void parse_quote_check(string &p2, int line_number, string filename);
bool parse_is_variable(string name, int line_number, string filename); // rename this

extern ham_vars varmap;

#endif // COMMON_H_INCLUDED
