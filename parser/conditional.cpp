#include "conditional.h"
#include "../functions.h"
#include "common.h"

::conditional::conditional()
{
    //ctor
}

::conditional::~conditional()
{
    //dtor
}

bool ::conditional::check(vector<string> condition, int line_number, string filename, string command)
{
    // check to see if the command is "if [arg] is defined" ...
    if (((condition[1]=="is") && (condition[2]=="defined")) || ((condition[1]=="is") && (condition[2]=="not") && (condition[3]=="defined")))
    {
        if(condition.size()!=3) {
            if(condition.size()!=4) {
                parse_error("Wrong number of arguments to function in '"+command+"'!\
                \nExpected 3 or 4 arguments, got "+IntToString(condition.size()),line_number,filename);
            }
        }

        if (!parse_is_quoted(condition[0]))
        {
            parse_error("Error: Expected a quoted string!",line_number,filename);
        }
        condition[0]=remove_quotes(condition[0]);

        if (condition[2]=="not")
        {
            if (!varmap.exists(condition[0]))
            {
                return true;
            }
        }
        else
        {
            if (varmap.exists(condition[0]))
            {
                return true;
            }
        }
    }
    else if (((condition[1]=="is") && (condition[2]=="file")) || ((condition[1]=="is") && (condition[2]=="not") && (condition[3]=="file")))
    {
        if(condition.size()!=3) {
            if(condition.size()!=4) {
                parse_error("Wrong number of arguments to function in '"+command+"'!\
                \nExpected 3 or 4 arguments, got "+IntToString(condition.size()),line_number,filename);
            }
        }

        varmap.expectType(condition[0],varmap._stringI|varmap._stringIliteral,line_number,filename);
        if (varmap.exists(condition[0])){condition[0]=varmap.value(condition[0]);}
        condition[0]=remove_quotes(condition[0]);

        if (condition[2]=="not")
        {
            if (!file_exists(condition[0])){
                return true;
            }
        }
        else
        {
            if (file_exists(condition[0])){
                return true;
            }
        }
    }
    else if (((condition[1]=="is") && (condition[2]=="installed")) || ((condition[1]=="is") && (condition[2]=="not") && (condition[3]=="installed")))
    {
        if(condition.size()!=4) {
            if(condition.size()!=5) {
                parse_error("Wrong number of arguments to function in '"+command+"'!\
                \nExpected "+IntToString(4-1)+" or "+IntToString(5-1)+" arguments, got "+IntToString(condition.size()-1),line_number,filename);
            }
        }

        varmap.expectType(condition[0],varmap._stringI|varmap._stringIliteral,line_number,filename);
        if (varmap.exists(condition[0])){condition[0]=varmap.value(condition[0]);}
        condition[0]=remove_quotes(condition[0]);

        if (condition[2]=="not")
        {
            if (!file_exists(lib_store+directory_separator+condition[0])){
                return true;
            }
        }
        else
        {
            if (file_exists(lib_store+directory_separator+condition[0])){
                return true;
            }
        }
    }
    else
    {
        bool run_block;
        parse_arg_check(condition,line_number,3,command,filename);
        // check var types to change the way they are compared...

        string check1_type("");
        string check2_type("");

        if ((condition[0].at(0) == '"') || (isNumber(condition[0])) || (condition[0] == "true") || (condition[0] == "false"))
        {
            // check 1 is NOT a variable
            if (isNumber(condition[0]))
            {
                check1_type = "real";
            }
            else if ( (condition[0] == "true") || (condition[0] == "false") )
            {
                check1_type = "bool";
            }
            else
            {
                check1_type = "string";
            }
        }
        else
        {
            // check 1 is a variable, check if it is defined
            if(!varmap.exists(condition[0])){parse_error("Error: "+condition[0]+" is not a variable!",line_number,filename);}
            check1_type = varmap.typeof_(condition[0]);
        }

        if ((condition[2].at(0) == '"') || (isNumber(condition[2])) || (condition[2] == "true") || (condition[2] == "false"))
        {
            // check 2 is NOT a variable
            if (isNumber(condition[2]))
            {
                check2_type = "real";
            }
            else if ( (condition[2] == "true") || (condition[2] == "false") )
            {
                check2_type = "bool";
            }
            else
            {
                check2_type = "string";
            }
        }
        else
        {
            // check 2 is a variable, check if it is defined
            if(!varmap.exists(condition[2])){parse_error("Error: "+condition[2]+" is not a variable!",line_number,filename);}
            check2_type = varmap.typeof_(condition[2]);
        }

        if (check1_type != check2_type)
        {
            parse_error("Error: Comparison type mismatch! ("+check1_type+"|"+check2_type+")",line_number,filename);
        }

        if (condition[1]=="eq"){condition[1]="==";} if (condition[1]=="ne"){condition[1]="!=";}
        if(parse_is_quoted(condition[0])){condition[0]=remove_quotes(condition[0]);}else
            {if (varmap.exists(condition[0])){condition[0]=varmap.value(condition[0]);}}
        if(parse_is_quoted(condition[2])){condition[2]=remove_quotes(condition[2]);}else
            {if (varmap.exists(condition[2])){condition[2]=varmap.value(condition[2]);}}

        if (show_debug){
            cout << "Comparing '" << condition[0] << "' to '" << condition[2] << "'...";
        }

        if (check1_type == "string")
        {
            if (condition[1]=="==")
            {
                if (condition[0]==condition[2]){
                    run_block=true;
                }
                else
                {
                    run_block=false;
                }
            }
            else if (condition[1]=="!=")
            {
                if (condition[0]!=condition[2]){
                    run_block=true;
                }
                else
                {
                    run_block=false;
                }
            }
            else
            {
                parse_error("Error: Unknown comparison '"+condition[1]+"'!\
                \nExpected '==', '!=', 'eq' or 'ne'!",line_number,filename);
            }
        }
        else if (check1_type == "real")
        {
            if (condition[1]=="==")
            {
                if (condition[0]==condition[2]){
                    run_block=true;
                }
                else
                {
                    run_block=false;
                }
            }
            else if (condition[1]=="!=")
            {
                if (condition[0]!=condition[2]){
                    run_block=true;
                }
                else
                {
                    run_block=false;
                }
            }
            else if (condition[1]=="<")
            {
                if (StringToFloat(condition[0])<StringToFloat(condition[2])){
                    run_block=true;
                }
                else
                {
                    run_block=false;
                }
            }
            else if (condition[1]==">")
            {
                if (StringToFloat(condition[0])>StringToFloat(condition[2])){
                    run_block=true;
                }
                else
                {
                    run_block=false;
                }
            }
            else
            {
                parse_error("Error: Unknown comparison '"+condition[1]+"'!\
                \nExpected '==', '!=', '<', '>', 'eq' or 'ne'!",line_number,filename);
            }
        }
        else if (check1_type == "bool")
        {
            if (condition[1]=="==")
            {
                if (condition[0]==condition[2]){
                    run_block=true;
                }
                else
                {
                    run_block=false;
                }
            }
            else if (condition[1]=="!=")
            {
                if (condition[0]!=condition[2]){
                    run_block=true;
                }
                else
                {
                    run_block=false;
                }
            }
            else
            {
                parse_error("Error: Unknown comparison '"+condition[1]+"'!\
                \nExpected '==', '!=', 'eq' or 'ne'!",line_number,filename);
            }
        }
        else
        {
            parse_error("Error: Unknown data type '"+check1_type+"'!\
            \nExpected 'string', 'real' or 'bool'",line_number,filename);
        }

        if (command=="unless")
        {
            if (show_debug){cout << " (result: " << (!run_block? "true":"false") << ")" << endl;}
            return !run_block;
        }
        else
        {
            if (show_debug){cout << " (result: " << (run_block? "true":"false") << ")" << endl;}
            return run_block;
        }
    }
    return false;
}
