#include "common.h"
#include "../globals.h"
using namespace std;
ham_vars varmap;

void parse_error(string error, int line_number, string filename)
{
    DisplayOnConsole("red",error,true);
    DisplayOnConsole("red","Parsing failed in '"+filename+"' (ln: "+IntToString(line_number)+")",true);
    if (fail_pause == true) { con_pause(); }
    quit.exit(EXIT_FAILURE);
}

void parse_warn(string warning, int line_number, string filename)
{
    DisplayOnConsole("yellow",warning,true);
    DisplayOnConsole("yellow","Parsing warned in '"+filename+"' (ln: "+IntToString(line_number)+")",true);
}

bool parseIsBracketedExpression(string input, int line_number, string filename)
{
    if (input.length()==0){return false;}
    if (input.at(0)!='('){return false;}

    input.replace(0,1,"");
    input.replace(input.length()-1,input.length(),"");
    vector<string> new_line = parseLine(input,line_number,filename);
    bool is_ok=true;
    for (unsigned int i=0; i != new_line.size(); i++)
    {
        //new_line[i]=removeLeadingWhitespace(new_line[i]);
        //cout << "'" << new_line[i] << "'" << endl;
        if (new_line[i].length() < 1){continue;}
        if (new_line[i].at(0)=='(')
        {
            is_ok=parseIsBracketedExpression(new_line[i],line_number,filename);
        }
        if (!parse_is_variable(new_line[i],line_number,filename))
        {
            if (!isNumber(new_line[i]))
            {
                if (!StringToChar(new_line[i])==' ')
                {
                    is_ok=false;
                }
            }
        }
    }
return is_ok;
}

bool parseIsLegalVarDeclare(string &s)
{
if (s.length()==0){return false;}
if (isNumber(CharToString(s.at(0)))){return false;}
    if ((GetCount(s,CharToString('\"'))==0) && (GetCount(s,"!")==0) && (GetCount(s,"£")==0) && (GetCount(s,"$")==0)
    && (GetCount(s,"%")==0) && (GetCount(s,"^")==0) && (GetCount(s,"&")==0) && (GetCount(s,"*")==0) && (GetCount(s,"#")==0)
    && (GetCount(s,"~")==0) && (GetCount(s,"`")==0) && (GetCount(s,"¬")==0) && (GetCount(s,"@")==0) && (GetCount(s,"{")==0)
    && (GetCount(s,"}")==0) && (GetCount(s,"[")==0) && (GetCount(s,"]")==0))
    {
        return true;
    }
return false;
}

string remove_quotes(string input)
{
    if (input.length()>1)
    {
        if (input.at(0)=='"')
        {
            input.erase(0,1);
        }
        if (input.at(input.length()-1)=='"')
        {
            input.erase(input.length()-1,1);
        }
        //cout << "removed as: " << input << endl;
        return input;
    }
    else
    {
        return "";
    }
}

bool isValidMathsOperator(string op)
{
    if ((op=="+") || (op=="-") || (op=="*") || (op=="/"))
    {
        return true;
    }
    return false;
}

string expandBracketedExpression(string exp, int line_number, string filename)
{
    if (!parseIsBracketedExpression(exp,line_number,filename))
    {
        parse_error("Error: not a bracketed expression!",line_number,filename);
    }

    exp=removeLeadingWhitespace(exp); exp=removeTrailingWhitespace(exp);
    exp.erase(exp.begin(),exp.begin()+1);
    exp.erase(exp.end()-1,exp.end());
    exp=removeLeadingWhitespace(exp); exp=removeTrailingWhitespace(exp);

    vector<string> line = parseLine(exp,line_number,filename);
    /*for (unsigned int i=0; i!=line.size(); i++)
    {
        cout << "[" << i << "]: " << line[i] << endl;
    }*/

    vector<string> MathStack;
    // push all of the operations to the stack
    for (unsigned int i=0; i!=line.size(); i++)
    {
        MathStack.push_back(line[i]);
    }

    // check if there are no operators at all...
    if (MathStack.size()==1){
        if (show_debug){cout << "No operator..." << endl;}
        return MathStack[0];
    }

    // find if there is more than 1 operation
    if (MathStack.size() < 4)
    {
        // 1 operator
        if (show_debug){cout << "Only 1 operator..." << endl;}

        // check they are valid...
        if (!isValidMathsOperator(MathStack[1]))
        {
            parse_error("Unknown math operation '"+MathStack[1]+"'!\
            \nOperations are: '+', '-', '*', '/'",line_number,filename);
        }
        // do the op...

        if (parse_is_variable(MathStack[0],line_number,filename))
        {
            if (!varmap.isType(MathStack[0],varmap._realI,line_number,filename))
            {
                if (!varmap.isType(MathStack[0],varmap._prealI,line_number,filename))
                {
                    parse_error("Error: '"+MathStack[0]+"' is not a real variable!",line_number,filename);
                }
            }
        }
        else
        {
            if (!parseIsBracketedExpression(MathStack[0],line_number,filename))
            {
                if (!isNumber(MathStack[0]))
                {
                    parse_error("Error: '"+MathStack[0]+"' is not a real!",line_number,filename);
                }
            }
        }

        if (parse_is_variable(MathStack[2],line_number,filename))
        {
            if (!varmap.isType(MathStack[2],varmap._realI,line_number,filename))
            {
                if (!varmap.isType(MathStack[2],varmap._prealI,line_number,filename))
                {
                    parse_error("Error: '"+MathStack[2]+"' is not a real variable!",line_number,filename);
                }
            }
        }
        else
        {
            if (!parseIsBracketedExpression(MathStack[2],line_number,filename))
            {
                if (!isNumber(MathStack[2]))
                {
                    parse_error("Error: '"+MathStack[2]+"' is not a real!",line_number,filename);
                }
            }
        }

        decimal<prealDecimalCount> temp(0);
        if (varmap.exists(MathStack[0])){MathStack[0]=varmap.value(MathStack[0]);}else
        if(parseIsBracketedExpression(MathStack[0],line_number,filename)){
            // do bracketed stuff...
            MathStack[0]=expandBracketedExpression(MathStack[0],line_number,filename);
        }
        if (varmap.exists(MathStack[2])){MathStack[2]=varmap.value(MathStack[2]);}else
        if(parseIsBracketedExpression(MathStack[2],line_number,filename)){
            // do bracketed stuff...
            MathStack[2]=expandBracketedExpression(MathStack[2],line_number,filename);
        }
        if (MathStack[1]=="+")
        {
            double res = StringToDouble(MathStack[0])+StringToDouble(MathStack[2]);
            temp.setAsDouble(res);
        }
        else if (MathStack[1]=="-")
        {
            double res = StringToDouble(MathStack[0])-StringToDouble(MathStack[2]);
            temp.setAsDouble(res);
        }
        else if (MathStack[1]=="*")
        {
            double res = StringToDouble(MathStack[0])*StringToDouble(MathStack[2]);
            temp.setAsDouble(res);
        }
        else if (MathStack[1]=="/")
        {
            if (StringToFloat(MathStack[2])==0){parse_error("Error: DIV 0",line_number,filename);}
            double res = StringToDouble(MathStack[0])/StringToDouble(MathStack[2]);
            temp.setAsDouble(res);
        }

        return toString(temp);
    }
    else
    {
        // more than 1, find the first * or /
        if (show_debug){cout<<"More than 1 operator..."<<endl;}
        while(MathStack.size()!=0)
        {
            bool foundPriorityOp=false;
            bool foundBracket=false;
            unsigned int priorityPos=0;
            unsigned int workingPos=0;
            //cout << "beginning loop" << endl;
            for (unsigned int i=0; i!=MathStack.size()-1; i++)
            {
                if ((foundPriorityOp)||(foundBracket)) {break;}if (foundPriorityOp) {break;}
                if (parseIsBracketedExpression(MathStack[i],line_number,filename))
                {
                    workingPos=i;
                    foundBracket=true;
                }
                else if ((MathStack[i]=="*")||(MathStack[i]=="/"))
                {
                    priorityPos=i;
                    foundPriorityOp=true;
                }
                else if ((MathStack[i]=="+")||(MathStack[i]=="-"))
                {
                    workingPos=i;
                }
            }
            if (foundPriorityOp)
            {
                // there is at least 1 / or *
                if (show_debug){cout << "P op" << endl;}
                if ((priorityPos-1<0)||(priorityPos+1>MathStack.size()))
                {
                    parse_error("Malformed expression!",line_number,filename);
                }
                decimal<prealDecimalCount> temp(0);
                if (varmap.exists(MathStack[priorityPos-1])){MathStack[priorityPos-1]=varmap.value(MathStack[priorityPos-1]);}
                if (varmap.exists(MathStack[priorityPos+1])){MathStack[priorityPos+1]=varmap.value(MathStack[priorityPos+1]);}

                if (MathStack[priorityPos]=="*")
                {
                    double res = StringToDouble(MathStack[priorityPos-1])*StringToDouble(MathStack[priorityPos+1]);
                    temp.setAsDouble(res);
                }
                else if (MathStack[priorityPos]=="/")
                {
                    if (StringToFloat(MathStack[priorityPos+1])==0){parse_error("Error: DIV 0",line_number,filename);}
                    double res = StringToDouble(MathStack[priorityPos-1])/StringToDouble(MathStack[priorityPos+1]);
                    temp.setAsDouble(res);
                }
                // cleanup...
                /*for (unsigned int t; t!=MathStack.size(); t++)
                {
                    cout << "[" << t << "]: " << MathStack[t] << endl;
                }*/
                MathStack[priorityPos-1]=toString(temp);
                MathStack.erase(MathStack.begin()+(priorityPos),MathStack.begin()+priorityPos+2);
                priorityPos=0;
                foundPriorityOp=false;
                /*for (unsigned int t; t!=MathStack.size(); t++)
                {
                    cout << "[" << t << "]: " << MathStack[t] << endl;
                }*/
            }
            else if (foundBracket)
            {
                // do bracketed expression...
                if (show_debug){cout<<"Expanding bracketed expression..."<<endl;}
                string expanded=expandBracketedExpression(MathStack[workingPos],line_number,filename);
                // do some cleanup...
                MathStack[workingPos]=expanded;
                workingPos=0;
                foundBracket=false;
            }
            else
            {
                // no * or /, do L -> R...
                if (show_debug){cout<<"L->R mode..."<<endl;}
                if (MathStack.size() < 5){
                        parse_error("Error: Malformed expression!",line_number,filename);
                    }
                if (isValidMathsOperator(MathStack[workingPos]))
                {
                    decimal<prealDecimalCount> temp(0);
                    if (varmap.exists(MathStack[workingPos-1])){MathStack[workingPos-1]=varmap.value(MathStack[workingPos-1]);}
                    if (varmap.exists(MathStack[workingPos+1])){MathStack[workingPos+1]=varmap.value(MathStack[workingPos+1]);}
                    if (MathStack[workingPos]=="+")
                    {
                        double res = StringToDouble(MathStack[workingPos-1])+StringToDouble(MathStack[workingPos+1]);
                        temp.setAsDouble(temp.getAsDouble()+res);
                    }
                    else if (MathStack[workingPos]=="-")
                    {
                        double res = StringToDouble(MathStack[workingPos-1])-StringToDouble(MathStack[workingPos+1]);
                        temp.setAsDouble(temp.getAsDouble()+res);
                    }
                    // do some cleanup...
                    /*for (unsigned int t; t!=MathStack.size(); t++)
                    {
                        cout << "[" << t << "]: " << MathStack[t] << endl;
                    }*/
                    MathStack[workingPos-1]=toString(temp);
                    MathStack.erase(MathStack.begin()+(workingPos),MathStack.begin()+workingPos+2);
                    /*for (unsigned int t; t!=MathStack.size(); t++)
                    {
                        cout << "[" << t << "]: " << MathStack[t] << endl;
                    }*/
                }
                else
                {
                    parse_error("Unknown math operation '"+MathStack[2]+"'!\
                    \nOperations are: '+', '-', '*', '/'",line_number,filename);
                }
            }
            if (MathStack.size()==3)
            {
                // no ops left, push into -> []
                isValidMathsOperator(MathStack[1]);
                if (varmap.isConst(MathStack[2]))
                {
                    parse_error("Error: Unable to set '"+MathStack[2]+"' as it has been declared as a constant!",line_number,filename);
                }
                decimal<prealDecimalCount> temp(0);
                temp.setAsDouble(StringToDouble(MathStack[0]));
                return toString(temp);
            }
        }
    }
    return "0";
}

bool isValidDataType(string type)
{
    if (type!="string"){
        if (type!="real") {
            if (type!="preal") {
                if (type!="function") {
                    if (type!="file") {
                        if (type!="bool") {
                            return false;
                        }
                    }
                }
            }
        }
    }
    return true;
}

bool parseHasBeginQuote(string &p2)
{
    if(p2.length()!=0){
        if(p2.at(0) =='\"')
        {
            return true;
        }
    }
return false;
}

bool parseHasEndQuote(string &p2)
{
    if(p2.length()!=0){
        if(p2.at(p2.size()-1)=='\"')
        {
            return true;
        }
    }
return false;
}

bool parse_is_quoted(string &p2)
{
    if (varmap.exists(p2))
    {
        return false;
    }
    if((parseHasBeginQuote(p2)) && (parseHasEndQuote(p2)))
    {
        return true;
    }
return false;
}

void parse_arg_check(vector<string> &line, int line_number, int unsigned expected_size, string command, string filename)
{
    if(line.size()!=expected_size) {
        string arg("");
        if (expected_size-1 == 1)
        {
            arg="argument";
        }
        else
        {
            arg="arguments";
        }
        parse_error("Wrong number of arguments to function in '"+command+"'!\
        \nExpected "+IntToString(expected_size-1)+" "+arg+", got "+IntToString(line.size()-1),line_number,filename);
    }
}

void parse_arg_check_atleast(vector<string> &line, int line_number, int unsigned expected_size, string command, string filename)
{
    if(line.size() < expected_size) {
        string arg("");
        if (expected_size-1 == 1)
        {
            arg="argument";
        }
        else
        {
            arg="arguments";
        }
        parse_error("Wrong number of arguments to function in '"+command+"'!\
        \nExpected at least "+IntToString(expected_size-1)+" "+arg+", got "+IntToString(line.size()-1),line_number,filename);
    }
}

void parse_quote_check(string &p2, int line_number, string filename)
{
    if((p2.at(0) =='\"') && (p2.at(p2.size()-1)!='\"') )  {
        parse_error("Error: Opening quote but no matching closing quote",line_number,filename);
    }
    if((p2.at(0) !='\"') && (p2.at(p2.size()-1)=='\"') )  {
        parse_error("Error: Closing quote but no matching opening quote",line_number,filename);
    }
}

bool parse_is_variable(string name, int line_number, string filename)
{
    if (name.at(0)=='(')
    {
        string working = name;
        working.replace(0,1,"");
        working.replace(working.length()-1,working.length(),"");
        vector<string> new_line = parseLine(working,line_number,filename);
        bool is_all_var=true;
        for (unsigned int i=0; i != new_line.size(); i++)
        {
            if (!parse_is_variable(new_line[i],line_number,filename))
            {
                is_all_var=false;
            }
        }
        return is_all_var;
    }
    else
    {
        if (isNumber(name))
        {
            return false;
        }

        if ((name.at(0)=='\"') || (name.at(name.size()-1)=='\"'))
        {
            return false;
        }
    }

return true;
}
