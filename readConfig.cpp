#include "readConfig.h"
#include "functions.h"
#include <iostream>

readConfig::readConfig()
{
    //ctor
    isOpen=false;
}

readConfig::~readConfig()
{
    //dtor
}

bool readConfig::parseOpenFile()
{
    if (isOpen==false){return false;}
    readFile configFile;
    if (!configFile.openFile(openFile,readFile__read)){return false;}
    while(!configFile.eof())
    {
        string temp=configFile.readLine();
        if (temp=="") { continue; }
        // the form should be [key]:[value]
        // check the syntax is correct
        size_t space=temp.find_first_of(":");
        //cout << space << endl;
        if ((space!=string::npos) && (temp.length()!=0)) {
            string key, value;
            key=temp.substr(0,space);
            value=temp.substr(space+1,temp.length());
            if (configData.count(key)==0)
            {
                configData[key]=value;
            }
            else
            {
                cout << "readConfig: Duplicate key for '"+key+"'!" << endl;
            }
            //cout << "'" << key << "':'" << value << "';" << endl;
        }
        else
        {
            cout << "readConfig: Malformed key-value pair: '"+temp+"'!" << endl;
            continue;
        }
    }
    configFile.close();
return true;
}

bool readConfig::setOpenFile(string file, string mode="r")
{
    if (isOpen==true){return false;}
    if (mode=="r")
    {
        if (!file_exists(file))
        {
            return false;
        }
    }
    openFile=file;
    isOpen=true;
    configData.clear();
    if (mode=="r")
    {
        parseOpenFile();
    }
return true;
}

void readConfig::closeOpenFile()
{
    isOpen=false;
    openFile="";
    configData.clear();
}

bool readConfig::keyExists(string key)
{
    if (isOpen==false){return false;}
    if (configData.count(key)==0)
    {
        return false;
    }
return true;
}

string readConfig::getValue(string key)
{
    if (configData.count(key)==0)
    {
        return "";
    }
return configData[key];
}

bool readConfig::addPair(string key, string value)
{
    if (isOpen==false){return false;}
    if (configData.count(key)!=0)
    {
        return false;
    }
    configData[key]=value;
return true;
}

bool readConfig::removePair(string key)
{
    if (isOpen==false){return false;}
    std::map<string,string>::iterator it;
    it=configData.find(key);
    if (it==configData.end()){return false;}
    configData.erase(it);
return true;
}

bool readConfig::saveConfig()
{
    if (isOpen==false){return false;}
    readFile configFile;
    if (!configFile.openFile(openFile,readFile__write)){return false;}
    // write all of the key-value pairs into the file...
    map<string, string>::const_iterator itr;
    for(itr = configData.begin(); itr != configData.end(); ++itr){
        string data=((*itr).first)+":"+((*itr).second);
        configFile.writeLine(data);
    }
    configFile.close();
return true;
}
