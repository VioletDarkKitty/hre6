#ifndef LOADER_H_INCLUDED
#define LOADER_H_INCLUDED

int loader(int argc, char *argv[]);
void print_usage();
string CheckOK(string CK);

#endif // LOADER_H_INCLUDED
