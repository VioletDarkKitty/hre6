#ifndef GLOBALS_H_INCLUDED
#define GLOBALS_H_INCLUDED

#include <vector>
#include "exitHelper.h"
// extern tells the compiler this variable is declared elsewhere

// these are the constant variables
extern float version;
extern std::string release_type;
extern std::string HRE_version;
extern std::string HRE_copyright;
extern std::string HRE_CompileDate;
extern std::string runtime_update_url;
extern std::string KERNEL_TYPE;
extern float settings_file_version;
extern int KERNEL_TYPE_I;
extern std::string HRE_executableName;

// these are modifiable variables
extern std::string current_program;
extern std::string current_program_orig;
extern std::string program_directory;
extern std::string temp_directory;
extern std::string program_storage;
extern std::string current_program_dir;
extern std::string program_open_dir;
extern std::string runtime_system;
extern std::string error_store;
extern std::string lib_store;
extern std::string cache_directory;
extern bool prog_open;
extern bool allow_cleanup;
extern bool local_temp;
extern std::string gcc_location;
extern bool fail_pause;
extern bool allow_console_colour;
extern bool verbose_include_output;
extern bool verbose_parse_output;
extern bool show_debug;
extern bool show_include_text;
extern std::vector<int> forcedFixes;
extern exitHelper quit;

#endif // GLOBALS_H_INCLUDED
