HRE6
====

Ham Runtime Environment 6 (released under GPL3)
See the COPYRIGHT file in the git repo for full details.

Compiling
=========

To build hre6, a C++ compiler will be required.
There is a codeblocks project file included in the repo and some make files which may also be used.

Easy build guide
----------------

1. Get a C++ compiler. MinGW is recommended.
	* An easier way to do this would be to install [codeblocks](http://www.codeblocks.org/downloads) and build the project using that.
2. Build the project with either codeblocks or make.
	* If you are using codeblocks, it would be a better idea to build the release version by changing the drop down menu on the build target.
3. Get the compiled files from the bin folder.
	* These will be inside another folder depending on which OS you are building on.
4. Place the executable files into a folder that you have write access to. If you are unsure, the documents folder on windows would be ok.
	* The executable should then be run to setup the environment. This will write to your user directory (or %programdata% on windows)
	* The runtime will then attempt to setup some information. Just follow the instructions in the console.

Extra information
-----------------

On OSX before 'Lion', the compiler flag `-arch i386` may be required to build.
The compiler flag needs to be added in codeblocks as a build option like the following:
![Compiler flag](md/images/Lion_arch.png)

External work
=============

This section lists all of the code written by other people - that may or may not want copyright recognition - and why it is needed.

[Dirent compatability layer for Windows](http://www.softagalleria.net/download/dirent/), version 1.13. Written by Toni Ronkko.
The header is a standard on Unix systems but is missing on Windows.

[MD5 hasher](http://www.zedwood.com/article/121/cpp-md5-function), converted to C++ class by Frank Thilo (thilo@unix-ag.org)
The header and cpp file are used to genarate an MD5 hash of a file or a string.

[Exception with stacktrace](http://www.eyt.ca/blog/item/108/), written by Eric Y. Theriault.
Prints a stack trace on error.

[SHA256](http://www.zedwood.com/article/cpp-sha256-function), header and C code to make a SHA256 hash.

[Decimal Class](https://github.com/vpiotr/decimal_for_cpp), header for storing precise decimal numbers. 

[Tclap](http://tclap.sourceforge.net/), command line parser library.

