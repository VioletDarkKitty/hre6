#ifndef READCONFIG_H
#define READCONFIG_H

#include <string>
#include <map>
#include "readFile.h"

using namespace std;
class readConfig
{
    public:
        readConfig();
        virtual ~readConfig();
        bool setOpenFile(string file, string mode);
        void closeOpenFile();
        bool keyExists(string key);
        string getValue(string key);
        bool addPair(string key, string value);
        bool saveConfig();
        bool removePair(string key);
    protected:
    private:
        string openFile;
        bool isOpen;
        map<string,string> configData;
        bool parseOpenFile();
};

#endif // READCONFIG_H
