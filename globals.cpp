#include <string>
float version = 1;
std::string release_type("DEV"); // BE - Bleading edge build (May only compile and needs serious testing)
                                // DEV - Development build (Works partly but may still need fixing, more stable than BE)
                                // RB - Release build (Works fully with no serious errors)
std::string HRE_version("HRE6");
std::string HRE_copyright("VioletDarkKitty and Funky Duck LTD (2015) under GPL3; See license.txt for details.");
std::string HRE_CompileDate(__DATE__);
std::string runtime_update_url("null");
float settings_file_version = 1.51;
#ifdef _WIN32
    std::string HRE_executableName="ham.exe";
#else
    std::string HRE_executableName="ham";
#endif
// these are for programs that are run in the runtime to check so that they can load the right external files or refuse to run because they are
// on the wrong environment and cannot find the external files they need to run
#ifdef _WIN32
    std::string KERNEL_TYPE("WINDOWS"); // Kernel name
    int KERNEL_TYPE_I = 1; // Kernel id number for programs to identify
#elif __linux__
    std::string KERNEL_TYPE("LINUX");
    int KERNEL_TYPE_I = 2;
#elif __APPLE__ && __MACH__
    std::string KERNEL_TYPE("MAC");
    int KERNEL_TYPE_I = 3;
#else
    std::string KERNEL_TYPE("UNKNOWN");
    int KERNEL_TYPE_I = -1;
#endif
