#ifndef READFILE_H
#define READFILE_H

#include <string>
#include <fstream>

// define some globals
int const readFile__read = 0;
int const readFile__write = 1;
int const readFile__append = 2;
int const readFile__binWrite = 3;

using namespace std;
class readFile
{
    public:
        readFile();
        virtual ~readFile();
        bool openFile(const char* fName, int mode);
        bool openFile(string fName, int mode);
        string readLine();
        bool eof();
        void writeLine(string data);
        void close();
        string whatFile();
    protected:
    private:
        fstream file;
        string openFileName;
};

#endif // READFILE_H
