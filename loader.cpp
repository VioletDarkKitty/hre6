#include <iostream>
#include "globals.h"
#include "functions.h"
#include "readFile.h"
#include "loader.h"
#include "parse.h"

#ifdef _WIN32
    #include <windows.h>
    #include <tchar.h>
#else
    #include <dlfcn.h> // for linking .so files
#endif

#ifdef _WIN32
    #include "dirent.h"
     #include <unistd.h>
#else
    #include <dirent.h>
    #include <sys/stat.h>
#endif

#include <cstring>
#include "md5.h"
#include <sys/time.h>
#include "readConfig.h"
#include "parseLine.h"
#include <tclap/CmdLine.h>
using namespace TCLAP;

#include "parser/parser.h"
using namespace std;

extern int brace_level;
extern int pass_arg_count;
extern string pass_arg_list[255];
#include "parser/ham_vars.h"
extern ham_vars varmap;
string manifest_author("");

void cleanupOpenParserFiles()
{
    readFile *ptr;
    for(ham_vars::FileStorageMap::iterator it=varmap.parse_files.begin(); it != varmap.parse_files.end(); it++) {
        ptr=it->second;
        ptr->close();
        delete ptr;
    }
}

void read_begin()
{
    /* KERNEL_TYPE */
    varmap.create("KERNEL_TYPE","string",KERNEL_TYPE,true,-1,true);
    /* KERNEL_TYPE_I */
    varmap.create("KERNEL_TYPE_I","real",IntToString(KERNEL_TYPE_I),true,-1,true);

    if (KERNEL_TYPE=="WINDOWS")
    {
        varmap.create("_WINDOWS","bool","true",true,-1,true);
        varmap.create("_UNIX","bool","false",true,-1,true);
        varmap.create("_LINUX","bool","false",true,-1,true);
        varmap.create("_MAC","bool","false",true,-1,true);
    }
    else if (KERNEL_TYPE=="LINUX")
    {
        varmap.create("_WINDOWS","bool","false",true,-1,true);
        varmap.create("_UNIX","bool","true",true,-1,true);
        varmap.create("_LINUX","bool","true",true,-1,true);
        varmap.create("_MAC","bool","false",true,-1,true);
    }
    else if (KERNEL_TYPE=="MAC")
    {
        varmap.create("_WINDOWS","bool","false",true,-1,true);
        varmap.create("_UNIX","bool","true",true,-1,true);
        varmap.create("_LINUX","bool","false",true,-1,true);
        varmap.create("_MAC","bool","true",true,-1,true);
    }
    /* HRE_ENV_VERSION */
    varmap.create("HRE_ENV_VERSION","string",HRE_version,true,-1,true);
    /* HRE_VERSION */
    varmap.create("HRE_VERSION","real",FloatToString(version),true,-1,true);
    /* temp_directory */
    varmap.create("temp_directory","string",current_program_dir,true,-1,true);
    /* program_storage */
    varmap.create("program_storage","string",program_storage,true,-1,true);
    /* working_directory */
    varmap.create("working_directory","string",program_open_dir,true,-1,true);

    // prevent memory errors due to file pointers existing at exit...
    quit.atexit(cleanupOpenParserFiles);
}

int read(string F_in, bool doBegin=true, bool fileIsInclude=false, string FilenameCallStack="")
{
    if (doBegin)
    {
        read_begin();
    }

    string filename = GetFileName(F_in);
    readFile Ifile;
    bool IRT = Ifile.openFile(F_in,readFile__read);
    string Iline;
    if (IRT != true)
    {
        DisplayOnConsole("red","Error in function 'read', the file could not be opened for reading!",true);
        return 0;
    }
    string parse_return;
    bool skip_code = false;
    while(!Ifile.eof())
    {
        Iline = Ifile.readLine();
        Iline=removeLeadingWhitespace(Iline);
        Iline=removeTrailingWhitespace(Iline);
        if ((Iline.length() != 0))
        {
            if (((CharToString(Iline.at(0)) == "~")||(Iline=="~")) || ((hasEnding(Iline,"~")) && (skip_code)))
            {
                if (skip_code == false) { skip_code = true;
                    if (show_debug){
                        DisplayOnConsole("yellow","Begining to skip code...");
                    }
                }
                else
                { skip_code = false;  Iline = Ifile.readLine(); line_number = line_number+1;
                    if (show_debug){
                        DisplayOnConsole("yellow","Finished skipping code");
                    }
                }
            }
        }

        if (skip_code != true)
        {
            if (verbose_parse_output)
            {
                DisplayOnConsole("normal","Parsing line: '"+Iline+"'...");
            }

            if (fileIsInclude){
                parse_return=parse(Iline,line_number,Ifile,true,FilenameCallStack+" -> "+filename,true);
            }
            else
            {
                parse_return=parse(Iline,line_number,Ifile,true,filename,false);
            }

            if (verbose_parse_output)
            {
                DisplayOnConsole("normal","Got: '"+parse_return+"'",true);
            }
            if (parse_return != "")
            {
                if (parse_return.length() > 0)
                {
                    if (parse_return=="**DIE**")
                    {
                        DisplayOnConsole("red","Error: The program forced an exit with a bad error code!");
                        return 0;
                    }
                    else if (parse_return=="**END**")
                    {
                        return 1;
                    }
                //cout << parse_return << endl;
                //con_pause();
                }
            }
        }
        line_number++;
    }
    Ifile.close();

    if (brace_level > 0)
    {
        parse_error("Error: Brace level count mismatch!",line_number,filename);
    }

    return 1;
}

string CheckOK(string path)
{
    // make sure a directory does not exist with the same name and if it does, return a new unique name...
    string name = GetFileName(path);
    string base = replaceString(path,name,"");
    if (name.length()==0){return CheckOK(path+"_");}
    while((directory_exists(path))||(file_exists(path)))
    {
        string name = GetFileName(path);
        string CK=name;
        int i=0;
        size_t posA = CK.find_first_of("(",0);

        if (posA!=string::npos)
        {
            CK.replace(0,posA+1,"");
            size_t posB = CK.find_first_of(")",0);
            if (posB!=string::npos)
            {
                CK.replace(posB,CK.length()-1,"");
                i=StringToInt(CK);
                i++;
            }
        }
        name = replaceString(name,"("+CK+")","");
        path=base+name+"("+IntToString(i)+")";
    }
return path;
}

int loader_ham()
{
    program_open_dir = GetPath(current_program);
    //cout << "The program load path is: " << program_open_dir << endl;
    setTitle("Ham Runtime "+HRE_version+" - "+GetFileName(current_program));
    prog_open=true;
    if (show_debug){
    DisplayOnConsole("purple","Loading program into memory...",true);
    DisplayOnConsole("cyan","Program begin:",true);}
    int program_return = read(current_program,true,false,current_program_orig);
    if (show_debug){
    DisplayOnConsole("cyan","Program end;");}fflush(stdout);
    if (program_return==1)
    {
        return 3; // success
    }
    else
    {
        return 0; // fail
    }
}

void pack_directory(string dirname, readFile &f)
{
    DIR *dir;
    struct dirent *ent;
    char* dirC = StringToChar(dirname);
    if ((dir = opendir(dirC)) != NULL)
    {
        if (show_debug){
        DisplayOnConsole("","Packing directory '"+dirname+"'",true);}
        f.writeLine("**DIR_PACK_BEGIN**");
        f.writeLine(GetFileName(dirname));
        string S_ent("");
        while ((ent = readdir (dir)) != NULL)
        {
            S_ent = ent->d_name;
            if (S_ent != ".")
            {
                if (S_ent != "..")
                {
                    if (!hasEnding(S_ent,"~"))
                    {
                        if (directory_exists(dirname+directory_separator+S_ent))
                        {
                            pack_directory(dirname+directory_separator+S_ent,f);
                        }
                        else
                        {
                            f.writeLine(GetFileName(S_ent));
                            pack_file(f, dirname+directory_separator+S_ent);
                            if (show_debug){
                            DisplayOnConsole("green","Packed file '"+S_ent+"'",true);}
                        }
                    }
                    else
                    {
                        if (show_debug){
                        DisplayOnConsole("red","Not packing file '"+S_ent+"'",true);}
                    }
                }
            }
        }
        if (show_debug){
        DisplayOnConsole("","Finished packing directory '"+dirname+"'");}
        f.writeLine("**DIR_PACK_END**");
        closedir (dir);
        delete dirC;
    }
    else
    {
        /* could not open directory */
        perror ("");
        delete dirC;
        quit.exit(EXIT_FAILURE);
    }
}

int unpack_directory(readFile &FilePointer, string RelativePath)
{
    string dirname;
    dirname = FilePointer.readLine();
    DirectoryCreate(RelativePath+directory_separator+dirname);
    //cout << "Found directory '"+dirname+"'" << endl;
    string Iline;
    int OpenCount = 0;
    while(Iline != "**DIR_PACK_END**")
    {
        Iline = FilePointer.readLine();
        //cout << "Input: '"+Iline+"'" << endl;
        if (Iline == "**DIR_PACK_BEGIN**")
        {
            // unpack it
            unpack_directory(FilePointer,RelativePath);
        }
        else if (Iline == "**DIR_PACK_END**" && OpenCount > 0)
        {
            OpenCount--;
        }
        else if (Iline != "**DIR_PACK_END**")
        {
            //cout << "Found file '"+Iline+"'" << endl;
            unpack_file(FilePointer,RelativePath+directory_separator+dirname+directory_separator+Iline);
        }
    }
    if (OpenCount > 0)
    {
        return -1;
    }
    else
    {
        return 0;
    }
}

void make_md5(string build_path, string archive_name, readFile &md5_make, string dir_suffix)
{
    DIR *dir;
    struct dirent *ent;
    if (dir_suffix!=""){ build_path+=directory_separator+dir_suffix; }
    char* dirC = StringToChar(build_path);
    if ((dir = opendir(dirC)) != NULL)
    {
        string S_ent("");
        readFile archiveOut;
        while ((ent = readdir (dir)) != NULL)
        {
            S_ent = ent->d_name;
            if (S_ent != ".")
            {
                if (S_ent != "..")
                {
                    if (!hasEnding(S_ent,"~"))
                    {
                        if ((S_ent != archive_name+".ha") && (S_ent != archive_name+".haminc"))
                        {
                            if (directory_exists(build_path+directory_separator+S_ent))
                            {
                                if (S_ent == "manifest")
                                {
                                    if (show_debug){
                                    DisplayOnConsole("red","Not making MD5 for the manifest '"+S_ent+"'!",true);}
                                }
                                else
                                {
                                    if (show_debug){
                                    DisplayOnConsole("cyan","Found directory '"+S_ent+"'",true);}
                                    make_md5(build_path,archive_name,md5_make,S_ent);
                                }
                            }
                            else
                            {
                                if (show_debug){
                                DisplayOnConsole("cyan","Making MD5 for "+S_ent+"...",true);}
                                MD5 m;
                                readFile rf;
                                rf.openFile(build_path+directory_separator+S_ent,readFile__read);
                                while(!rf.eof())
                                {
                                   string line=rf.readLine();
                                   //cout << line << endl;
                                   m.update(line);
                                }

                                m.finalize();

                                if (dir_suffix==""){md5_make.writeLine(S_ent);}else
                                {md5_make.writeLine("DIR: "+dir_suffix); md5_make.writeLine(S_ent);}
                                md5_make.writeLine(m.hexdigest());
                                if (show_debug){
                                DisplayOnConsole("green","MD5 made for file '"+S_ent+"'",true);}
                                rf.close();
                            }
                        }
                        else
                        {
                            if (show_debug){
                            DisplayOnConsole("red","Not making MD5 for self!",true);}
                        }
                    }
                    else
                    {
                        if (show_debug){
                        DisplayOnConsole("red","Not making MD5 for file '"+S_ent+"'",true);}
                    }
                }
                else
                {
                    if (show_debug){
                    DisplayOnConsole("red","Not making MD5 for file '..'",true);}
                }
            }
            else
            {
                if (show_debug){
                DisplayOnConsole("red","Not making MD5 for file '.'",true);}
            }
        }
        closedir (dir);
    }
    delete dirC;
}

int extract_ham_archive(string arg1, string current_program_dir)
{
    if (file_exists(arg1))
    {
        if (show_debug){
        DisplayOnConsole("cyan","Unpacking "+GetFileName(arg1)+"...",true);}
        readFile archive;
        archive.openFile(arg1,readFile__read);
        string archive_file_name;
        DirectoryCreate(current_program_dir);
        if (show_debug){
        DisplayOnConsole("cyan","Unpacking to "+current_program_dir+"...",true);}
        while(!archive.eof())
        {
            // read the name of the file...
            archive_file_name = archive.readLine();
            if (!archive.eof())
            {
                // output what was found...
                if (archive_file_name != "**Fopen_DELIM**")
                {
                    if (archive_file_name != "**Fclose_DELIM**")
                    {
                        if (archive_file_name == "**DIR_PACK_BEGIN**")
                        {
                            // found a directory
                            unpack_directory(archive, current_program_dir);
                        }
                        else
                        {
                            // found a file
                            // read the file data...
                            unpack_file(archive, current_program_dir+directory_separator+archive_file_name);
                            //DisplayOnConsole("normal","File found in archive: '"+archive_file_name+"'");
                            if (!file_exists(current_program_dir+directory_separator+archive_file_name))
                            {
                                DisplayOnConsole("red","Error extracting '"+archive_file_name+"': The extracted file does not exist!",true);
                            }
                        }
                    }
                }
            }
        }
        archive.close();
        if (file_exists(current_program_dir+directory_separator+"manifest"+directory_separator+"md5"))
        {
            file_delete(current_program_dir+directory_separator+"manifest"+directory_separator+"md5");
        }
        return 0;
    }
    else
    {
        DisplayOnConsole("red",arg1+" does not exist!",true);
        return -1;
    }
}

int remove_ham(string f, bool Ask=true)
{
    if (file_exists(runtime_system+directory_separator+"catalogue"+directory_separator+f+".catalogue"))
    {
        readFile catalogue;
        catalogue.openFile(runtime_system+directory_separator+"catalogue"+directory_separator+f+".catalogue",readFile__read);
        // skip over the version and file definition header...
        catalogue.readLine(); catalogue.readLine();

        vector<string> FileList;
        while(!catalogue.eof())
        {
            string file = catalogue.readLine();
            if (file!="")
            {
                FileList.push_back(file);
            }
        }
        catalogue.close();

        string removeAsk("Y");
        if (Ask)
        {
            DisplayOnConsole("normal","The following files will be removed:");
            for(unsigned int i=0; i!=FileList.size(); i++)
            {
                DisplayOnConsole("normal",FileList[i]);
            }
            cout << endl << "Remove? [y/N]: ";
            removeAsk=getInput();
            cout << endl;
        }

        if (StringLower(removeAsk)=="y")
        {
            // remove the library/include file
            for(unsigned int i=0; i!=FileList.size(); i++)
            {
                if (FileList[i].length()!=0)
                {
                    if (FileList[i].at(0)=='[')
                    {
                        // found a dir...
                        if (show_debug){cout << "Found a dir" << endl;}
                        FileList[i].replace(0,1,""); // replace the first [ char
                        size_t posB = FileList[i].find_first_of("]",0);
                        if (posB == string::npos){
                            DisplayOnConsole("red","Error deleting file '"+FileList[i]+"', malformed tag!");
                        }
                        FileList[i].replace(posB,2,"");

                        string dirName, fname;
                        posB = FileList[i].find_first_of(" ",0);
                        dirName = FileList[i]; dirName.replace(posB,dirName.length(),""); dirName.replace(0,1,"");
                        fname = FileList[i]; fname.replace(0,posB+1,"");

                        string newPath = lib_store+directory_separator+dirName+directory_separator+fname;
                        if (show_debug){cout << "new path is " << newPath << endl;}
                        if (file_exists(newPath))
                        {
                            file_delete(newPath);
                            DisplayOnConsole("normal","Removed '"+dirName+directory_separator+fname+"'");
                            if (show_debug){cout << "Deleted file in folder" << endl;}
                            string newPath = lib_store+directory_separator+dirName;
                            if (dirEmpty(newPath)){directory_remove(newPath); if (show_debug){cout << "Dir removed '" << newPath << "'" << endl;}}
                            else{if(show_debug){cout << "Dir not blank" << endl;}}
                        }
                        else
                        {
                            DisplayOnConsole("red","The file '"+dirName+directory_separator+fname+"' does not exist!");
                        }
                    }
                    else
                    {
                        if (file_exists(lib_store+directory_separator+FileList[i]))
                        {
                            file_delete(lib_store+directory_separator+FileList[i]);
                            DisplayOnConsole("normal","Removed '"+FileList[i]+"'");
                        }
                        else
                        {
                            DisplayOnConsole("red","The file '"+FileList[i]+"' does not exist!");
                        }
                    }
                }
            }
            file_delete(runtime_system+directory_separator+"catalogue"+directory_separator+f+".catalogue");
            DisplayOnConsole("green","Removal completed!",true);
            if (Ask){quit.exit(EXIT_SUCCESS);}
        }
        else
        {
            DisplayOnConsole("normal","No files removed");
            if (Ask){quit.exit(EXIT_SUCCESS);}
        }
    }
    else
    {
        DisplayOnConsole("red","Error: '"+f+"' is not installed!",true);
        if (fail_pause){con_pause();}
        quit.exit(EXIT_FAILURE);
    }
    return 0;
}

void install_ham_dir_search(string working, vector<string>& FileList, bool IsDir=false, string dirStack="")
{
    DIR *dir;
    struct dirent *ent;
    // begin looking for files
    char* dirC = StringToChar(working);
    if ((dir = opendir(dirC)) != NULL)
    {
        string S_ent("");
        while ((ent = readdir (dir)) != NULL)
        {
            S_ent = ent->d_name;
            if (S_ent != ".")
            {
                if (S_ent != "..")
                {
                    if (!directory_exists(working+directory_separator+S_ent)){
                        if (IsDir){
                            if (!directory_exists(lib_store+directory_separator+dirStack)){
                                DirectoryCreate(lib_store+directory_separator+dirStack);
                            }
                            file_copy(working+directory_separator+S_ent,
                            lib_store+directory_separator+dirStack+directory_separator+GetFileName(S_ent));
                            FileList.push_back("["+dirStack+"]: "+GetFileName(S_ent));
                        }
                        else
                        {
                            file_copy(working+directory_separator+S_ent,lib_store+directory_separator+GetFileName(S_ent));
                            FileList.push_back(GetFileName(S_ent));
                        }
                        if (show_debug){
                            if (IsDir){cout << "In a dir" << endl;}
                            cout << "Added file '" << GetFileName(S_ent) << "'" << endl;
                        }
                    }
                    else
                    {
                        if (GetFileName(S_ent)!="manifest")
                        {
                            // there is a directory...
                            if (show_debug){
                                cout << "Found dir, searching..." << endl;
                            }
                            install_ham_dir_search(working+directory_separator+GetFileName(S_ent),FileList,true,
                            dirStack+directory_separator+GetFileName(S_ent));
                        }
                    }
                }
            }
        }
        closedir (dir);
        delete dirC;
    }
    else
    {
        /* could not open directory */
        perror ("");
        delete dirC;
        quit.exit(EXIT_FAILURE);
    }
}

int install_ham(string f)
{
    // extract the file...
    string working = CheckOK(temp_directory+directory_separator+ChangeFileExtention(GetFileName(f),""));
    extract_ham_archive(f,working);
    readFile temp;
    string package_name, package_description, package_version;

    if (file_exists(working+directory_separator+"manifest"+directory_separator+"package_name"))
    {
        temp.openFile(working+directory_separator+"manifest"+directory_separator+"package_name",readFile__read);
        package_name=removeLeadingWhitespace(temp.readLine());
        temp.close();
    }

    if (file_exists(working+directory_separator+"manifest"+directory_separator+"package_description"))
    {
        temp.openFile(working+directory_separator+"manifest"+directory_separator+"package_description",readFile__read);
        package_description=removeLeadingWhitespace(temp.readLine());
        temp.close();
    }

    if (file_exists(working+directory_separator+"manifest"+directory_separator+"package_version"))
    {
        temp.openFile(working+directory_separator+"manifest"+directory_separator+"package_version",readFile__read);
        package_version=removeLeadingWhitespace(temp.readLine());
        temp.close();
    }


    // check the md5s...
    if (file_exists(working+directory_separator+"manifest"+directory_separator+"md5"))
    {
        if (show_debug){
        DisplayOnConsole("cyan","Checking md5 signitures...",true);}
        readFile md5_check;
        md5_check.openFile(working+directory_separator+"manifest"+directory_separator+"md5",readFile__read);
        while(!md5_check.eof())
        {
            //cout << "loop" << endl;
            if (!md5_check.eof())
            {
                string md5_filename;
                md5_filename = md5_check.readLine();
                if (md5_filename == ""){break;}
                // look for file
                if ((md5_filename.at(0)=='D') && (md5_filename.at(1)=='I') && (md5_filename.at(2)=='R')
                && (md5_filename.at(3)==':') && md5_filename.at(4)==' ')
                {
                    // found what looks like a directory, check it isnt corrupted!
                    if (md5_filename.length() < 4)
                    {
                        DisplayOnConsole("red","There are missing characters on a directory definition!",true);
                        DisplayOnConsole("red","This could indicate corruption of the file.",true);
                        con_pause();
                        quit.exit(EXIT_FAILURE);
                    }

                    string md5_directoryname(md5_filename.replace(0,5,""));
                    md5_filename=md5_check.readLine();
                    //cout << md5_directoryname << " " << md5_filename;

                    if (file_exists(working+directory_separator+md5_directoryname+directory_separator+md5_filename))
                    {
                        string md5_key;
                        md5_key = md5_check.readLine();
                        // make md5 of file to check against...
                        MD5 m;
                        readFile rf;
                        rf.openFile(working+directory_separator+md5_directoryname+directory_separator+md5_filename,readFile__read);
                        while(!rf.eof())
                        {
                           string line=rf.readLine();
                           m.update(line);
                        }

                        m.finalize();
                        string md5_key2 = m.hexdigest();
                        if (md5_key != md5_key2)
                        {
                            DisplayOnConsole("red","Error: MD5 keys '"+md5_key+"' and '"+md5_key2+"' for file '"+md5_filename+"' do not match!",true);
                            DisplayOnConsole("red","This could mean that the archive is corrupt or has been modified!",true);
                            md5_check.close();
                            rf.close();
                            if (fail_pause){con_pause();}
                            quit.exit(EXIT_FAILURE);
                        }
                        if (show_debug){
                        DisplayOnConsole("green","MD5 for '"+md5_filename+"' (in '"+md5_directoryname+"') is OK.",true);}
                    }
                    else
                    {
                        DisplayOnConsole("red","Error: MD5 for file '"+md5_filename+"' (in '"+md5_directoryname+"') found but the file does not exist in the archive!",true);
                        // skip over the md5 key
                        md5_check.readLine();
                    }
                }
                else
                {
                    if (file_exists(working+directory_separator+md5_filename))
                    {
                        string md5_key;
                        md5_key = md5_check.readLine();
                        // make md5 of file to check against...
                        MD5 m;
                        readFile rf;
                        rf.openFile(working+directory_separator+md5_filename,readFile__read);
                        while(!rf.eof())
                        {
                           string line=rf.readLine();
                           m.update(line);
                        }

                        m.finalize();
                        string md5_key2 = m.hexdigest();
                        if (md5_key != md5_key2)
                        {
                            DisplayOnConsole("red","Error: MD5 keys '"+md5_key+"' and '"+md5_key2+"' for file '"+md5_filename+"' do not match!",true);
                            DisplayOnConsole("red","This could mean that the archive is corrupt of has been modified!",true);
                            md5_check.close();
                            rf.close();
                            con_pause();
                            quit.exit(EXIT_FAILURE);
                        }
                        if (show_debug){
                        DisplayOnConsole("green","MD5 for '"+md5_filename+"' is OK.",true);}
                    }
                    else
                    {
                        DisplayOnConsole("red","Error: MD5 for file '"+md5_filename+"' found but the file does not exist in the archive!",true);
                        // skip over the md5 key
                        md5_check.readLine();
                    }
                }
            }
        }
    }

    DisplayOnConsole("purple","Package contents:\nName: "+package_name+"\nDescription: "+package_description+"\nVersion: "+package_version+"\n");

    string state="none";
    string install_ask;
    if (file_exists(runtime_system+directory_separator+"catalogue"+directory_separator+package_name+".catalogue"))
    {
        readFile catalogue;
        catalogue.openFile(runtime_system+directory_separator+"catalogue"+directory_separator+package_name+".catalogue",readFile__read);
        string installed_version = catalogue.readLine();
        catalogue.close();

        if (StringToFloat(package_version) > StringToFloat(installed_version))
        {
            // the package can be upgraded...
            DisplayOnConsole("red","The package '"+package_name+"' is already installed as version "+installed_version);
            DisplayOnConsole("purple","Upgrade? [y/N]: ",false);
            string upgrade_ask=getInput();
            if (StringUpper(upgrade_ask)=="Y")
            {
                state="upgrade";
                DisplayOnConsole("green","The package '"+package_name+"' will be upgraded to version "+package_version);
                remove_ham(package_name,false);
            }
        }
        else
        {
            directory_delete_recursive(working);
            DisplayOnConsole("red","The package '"+package_name+"' is already installed as version "+installed_version+", please remove it first!");
            if (fail_pause){con_pause();}
            quit.exit(EXIT_FAILURE);
        }
    }
    else
    {
        DisplayOnConsole("purple","Install? [y/N]: ",false);
        cin >> install_ask;
    }

    if ((StringUpper(install_ask)=="Y")||(state=="upgrade"))
    {
        // install the library/include file
        vector<string> FileList;

        install_ham_dir_search(working,FileList);

        if (FileList.size()==0)
        {
            directory_delete_recursive(working);
            DisplayOnConsole("red","Error: No files were found in the archive!");
            if (fail_pause){con_pause();}
            quit.exit(EXIT_FAILURE);
        }

        // add the information to the catalogue...
        if (!directory_exists(runtime_system+directory_separator+"catalogue"))
        {
            DirectoryCreate(runtime_system+directory_separator+"catalogue");
        }
        readFile catalogue;
        catalogue.openFile(runtime_system+directory_separator+"catalogue"+directory_separator+package_name+".catalogue",readFile__write);
        catalogue.writeLine(package_version);
        catalogue.writeLine("FILES:");
        for(unsigned int i=0; i!=FileList.size(); i++)
        {
            catalogue.writeLine(FileList[i]);
        }
        catalogue.close();

        DisplayOnConsole("green","Install completed!",true);
        directory_delete_recursive(working);
        return 1;
    }
    else
    {
        directory_delete_recursive(working);
        return 0;
    }
}

void list_ham()
{
    // list out all of the catalogue data for installed HAMINC files and then exit(1)
    DIR *dir;
    struct dirent *ent;
    // begin looking for files
    string working=runtime_system+directory_separator+"catalogue";
    char* dirC = StringToChar(working);
    int package_count=0;
    if ((dir = opendir(dirC)) != NULL)
    {
        string S_ent("");
        while ((ent = readdir (dir)) != NULL)
        {
            S_ent = ent->d_name;
            if (S_ent != ".")
            {
                if (S_ent != "..")
                {
                    if (!directory_exists(working+directory_separator+S_ent)){
                        package_count++;
                        // read the catalogue file...
                        string file_name=GetFileName(S_ent);
                        readFile catalogue;
                        catalogue.openFile(runtime_system+directory_separator+"catalogue"+directory_separator+file_name,readFile__read);
                        string installed_version = catalogue.readLine();
                        catalogue.readLine(); // skip a line because this should always read "FILES:"
                        /// TODO: list how many files are in a package
                        catalogue.close();
                        size_t ext_location = file_name.find_last_of(GetFileExtension(file_name),0);
                        cout << "[" << package_count << "]: " << replaceString(file_name,ext_location,file_name.length(),"")
                        << " v" << installed_version << endl;
                    }
                    else
                    {
                        // directory, ignore...
                    }
                }
            }
        }
        closedir (dir);
        delete dirC;
        cout << "Found " << package_count << " packages." << endl;
    }
    else
    {
        /* could not open directory */
        perror ("");
        delete dirC;
        quit.exit(EXIT_FAILURE);
    }
    if (fail_pause){con_pause();}
    quit.exit(EXIT_SUCCESS);
}

void print_settings()
{
    cout << "Unknown setting name! (Capitalisation matters)" << endl << endl
    << "Settings are:" << endl
    << "AllowCleanup" << endl
    << "ConsoleColour" << endl
    << "FailPause" << endl
    << "ShowDebug" << endl
    << "ShowIncludeText" << endl
    << "UseLocalTemp" << endl
    << "VerboseIncludes" << endl
    << "VerboseParsing" << endl;
}

void limitArgs(MultiArg<string> &input,unsigned int limit)
{
    string plural=(limit==1)? "":"s";
    if(input.getValue().size()!=limit)
    {
        DisplayOnConsole("red","Error: "+IntToString(limit)+" argument"+plural+" must be used for "+input.getName()+"!");
        quit.exit(EXIT_FAILURE);
    }
}

void process_switches(int argc, char *argv[])
{
    try {
        string commandlineTitle("Ham Runtime "+HRE_version+" version "+FloatToString(version)+" ("+release_type+")\n"+
        "Copyright: "+HRE_copyright+"\n"+"Compile date: "+HRE_CompileDate+"\n"+"Compiled for: "+KERNEL_TYPE);
        #ifdef _WIN32
            #ifdef _WIN64
                commandlineTitle+=" x64";
            #elif WIN64
                commandlineTitle+=" x64";
            #else
                commandlineTitle+=" x86";
            #endif
        #endif
        commandlineTitle+=" ("+IntToString(KERNEL_TYPE_I)+")";
        CmdLine cmd(commandlineTitle,' ', HRE_version+" version "+FloatToString(version)+" ("+release_type+")");
        SwitchArg NoCol("o","NoCol", "Disables console colour", cmd, false);
        SwitchArg NoCleanup("c","NoCleanup", "Disables cleaning of the temp folder", cmd, false);
        MultiArg<string> Build("b","Build","Builds a HA file",false,"HA filename> <Build directory",cmd);
        MultiArg<string> BuildCommon("m","BuildCommon","Builds a HAMINC file",false,"HAMINC filename> <Build directory",cmd);
        SwitchArg Uninstall("u","Uninstall", "Removes installed runtime data", cmd, false);
        ValueArg<string> InstallHam("i","Install","Installs a HAMINC file",false,"","String",cmd);
        ValueArg<string> RemoveHam("r","Remove","Uninstalls a HAMINC file",false,"","String",cmd);
        SwitchArg ListHam("l","List", "Lists the catalogue of installed HAMINC files", cmd, false);
        SwitchArg Debug("d","Debug", "Enables debug mode", cmd, false);
        MultiArg<string> Setting("s","Setting","Modifies a setting",false,"Setting name> <Value",cmd);
        MultiArg<int> ForceFix("f","fix","Forces a runtime hotfix to be applied",false,"Fix ID",cmd);
        UnlabeledValueArg<string> openFilename("filename","The file to run",false,"","filename",cmd);

        cmd.parse( argc, argv );
        if(Debug.isSet()){show_debug=true;}
        if (NoCol.getValue()){
            allow_console_colour=false;
            DisplayOnConsole("yellow","[Notice] Disabled console colour",true);
        }
        if (NoCleanup.getValue()){
            DisplayOnConsole("yellow","[Notice] Cleanup disabled!",true);
            allow_cleanup=false;
        }
        if (Build.isSet()){
            if (show_debug){
            cout << "Begining to build archive file..." << endl;}
            // build the current folder into an archive file with the name after -B


            string archive_name = Build.getValue()[0];
            string build_path = Build.getValue()[1];
            if (!directory_exists(build_path))
            {
                DisplayOnConsole("red","The directory '"+build_path+"' does not exist!",true);
                quit.exit(EXIT_FAILURE);
            }
            cout << "Building to: " << build_path << directory_separator << archive_name << ".ha" << endl;
            cout << "Building from: " << build_path << endl;
            DIR *dir;
            struct dirent *ent;
            bool remove_md5file=false;
            bool remove_manifest=false;
            if (!directory_exists(build_path+directory_separator+"manifest"))
            {
                DirectoryCreate(build_path+directory_separator+"manifest");
                remove_manifest=true;
            }
            if (file_exists(build_path+directory_separator+"manifest"+directory_separator+"md5"))
            {
                if (show_debug){
                DisplayOnConsole("red","Found md5 file in manifest, removing...",true);}
                file_delete(build_path+directory_separator+"manifest"+directory_separator+"md5");
            }

            // dont ask to make md5 keys!
            remove_md5file=true;
            readFile md5_make;
            md5_make.openFile(build_path+directory_separator+"manifest"+directory_separator+"md5",readFile__write);
            make_md5(build_path,archive_name,md5_make,"");
            md5_make.close();

            // begin looking for files
            char* dirC = StringToChar(build_path);
            if ((dir = opendir(dirC)) != NULL)
            {
                string S_ent("");
                readFile archiveOut;
                archiveOut.openFile(build_path+directory_separator+archive_name+".ha",readFile__binWrite); // dont convert line endings
                while ((ent = readdir (dir)) != NULL)
                {
                    S_ent = ent->d_name;
                    if (S_ent != ".")
                    {
                        if (S_ent != "..")
                        {
                            if (!hasEnding(S_ent,"~"))
                            {
                                if (directory_exists(build_path+directory_separator+S_ent))
                                {
                                    pack_directory(build_path+directory_separator+S_ent,archiveOut);
                                }
                                else
                                {
                                    if (S_ent != archive_name+".ha")
                                    {
                                        archiveOut.writeLine(GetFileName(S_ent));
                                        pack_file(archiveOut, build_path+directory_separator+S_ent);
                                        if (show_debug){
                                        DisplayOnConsole("green","Packed file '"+S_ent+"'",true);}
                                    }
                                    else
                                    {
                                        if (show_debug){
                                        DisplayOnConsole("red","Not packing self!",true);}
                                    }
                                }
                            }
                            else
                            {
                                if (show_debug){
                                DisplayOnConsole("red","Not packing file '"+S_ent+"'",true);}
                            }
                        }
                        else
                        {
                            if (show_debug){
                            DisplayOnConsole("red","Not packing file '..'",true);}
                        }
                    }
                    else
                    {
                        if (show_debug){
                        DisplayOnConsole("red","Not packing file '.'",true);}
                    }
                }
                closedir (dir);
                archiveOut.close();
                DisplayOnConsole("cyan","Packing complete.",true);
                delete dirC;
            }
            else
            {
              /* could not open directory */
              perror ("");
              delete dirC;
              quit.exit(EXIT_FAILURE);
            }
            if (remove_md5file)
            {
                file_delete(build_path+directory_separator+"manifest"+directory_separator+"md5");
            }
            if (remove_manifest)
            {
                directory_remove(build_path+directory_separator+"manifest");
            }

            exit(EXIT_SUCCESS); // dont allow executing because the program name is actually a directory!
        }
        if (BuildCommon.isSet()){
            limitArgs(BuildCommon,2);
            if (show_debug){
            cout << "Begining to build archive file..." << endl;}
            // build the current folder into an archive file with the name after -BC
            string archive_name = BuildCommon.getValue()[0];
            string build_path = BuildCommon.getValue()[1];
            if (!directory_exists(build_path))
            {
                DisplayOnConsole("red","The directory '"+build_path+"' does not exist!",true);
                quit.exit(EXIT_FAILURE);
            }
            cout << "Building to: " << build_path << directory_separator << archive_name << ".haminc" << endl;
            cout << "Building from: " << build_path << endl;
            DIR *dir;
            struct dirent *ent;
            bool remove_md5file=true;

            if (!directory_exists(build_path+directory_separator+"manifest"))
            {
                DirectoryCreate(build_path+directory_separator+"manifest");
            }

            // ask the archive details...
            string package_name="";
            string package_description="";
            string package_version="";
            DisplayOnConsole("purple","Please enter a name for the include package: ",false);
            //cin >> package_name; cout << "";
            package_name=getInput();
            DisplayOnConsole("purple","Please enter a description for the package: ",false);
            //cin >> package_description; cout << "";
            package_description=getInput();
            DisplayOnConsole("purple","Please enter a version for the package: ",false);
            //cin >> package_version; cout << "";
            package_version=getInput();

            // write the information to a temp file...
            readFile temp;
            temp.openFile(build_path+directory_separator+"manifest"+directory_separator+"package_name",readFile__write);
            temp.writeLine(package_name);
            temp.close();
            temp.openFile(build_path+directory_separator+"manifest"+directory_separator+"package_description",readFile__write);
            temp.writeLine(package_description);
            temp.close();
            temp.openFile(build_path+directory_separator+"manifest"+directory_separator+"package_version",readFile__write);
            temp.writeLine(package_version);
            temp.close();

            readFile md5_make;
            md5_make.openFile(build_path+directory_separator+"manifest"+directory_separator+"md5",readFile__write);
            make_md5(build_path,archive_name,md5_make,"");
            md5_make.close();

            // begin looking for files
            char* dirC = StringToChar(build_path);
            if ((dir = opendir(dirC)) != NULL)
            {
                string S_ent("");
                readFile archiveOut;
                archiveOut.openFile(build_path+directory_separator+archive_name+".haminc",readFile__binWrite); // dont convert line endings
                while ((ent = readdir (dir)) != NULL)
                {
                    S_ent = ent->d_name;
                    if (S_ent != ".")
                    {
                        if (S_ent != "..")
                        {
                            if (!hasEnding(S_ent,"~"))
                            {
                                if (directory_exists(build_path+directory_separator+S_ent))
                                {
                                    pack_directory(build_path+directory_separator+S_ent,archiveOut);
                                }
                                else
                                {
                                    if (S_ent != archive_name+".haminc")
                                    {
                                        archiveOut.writeLine(GetFileName(S_ent));
                                        pack_file(archiveOut, build_path+directory_separator+S_ent);
                                        if (show_debug){
                                        DisplayOnConsole("green","Packed file '"+S_ent+"'",true);}
                                    }
                                    else
                                    {
                                        if (show_debug){
                                        DisplayOnConsole("red","Not packing self!",true);}
                                    }
                                }
                            }
                            else
                            {
                                if (show_debug){
                                DisplayOnConsole("red","Not packing file '"+S_ent+"'",true);}
                            }
                        }
                        else
                        {
                            if (show_debug){
                            DisplayOnConsole("red","Not packing file '..'",true);}
                        }
                    }
                    else
                    {
                        if (show_debug){
                        DisplayOnConsole("red","Not packing file '.'",true);}
                    }
                }
                closedir (dir);
                archiveOut.close();
                DisplayOnConsole("cyan","Packing complete.",true);
                delete dirC;
            }
            else
            {
              /* could not open directory */
              perror ("");
              delete dirC;
              quit.exit(EXIT_FAILURE);
            }
            if (remove_md5file)
            {
                file_delete(build_path+directory_separator+"manifest"+directory_separator+"md5");
            }
            file_delete(build_path+directory_separator+"manifest"+directory_separator+"package_name");
            file_delete(build_path+directory_separator+"manifest"+directory_separator+"package_description");
            file_delete(build_path+directory_separator+"manifest"+directory_separator+"package_version");
            directory_remove(build_path+directory_separator+"manifest");
            quit.exit(EXIT_SUCCESS);
        }
        if (Uninstall.isSet()){
            // delete all the installed mime data and .desktop files
            DisplayOnConsole("cyan","Remove all runtime system data? [y/N] ",false);
            string remove_ask("");
            cin >> remove_ask;
            if (StringUpper(remove_ask)=="Y")
            {
                #ifdef __linux__
                    #ifndef __arm__
                        readFile xmlmime;
                        xmlmime.openFile(temp_directory+directory_separator+"HRE6-ham.xml",readFile__write);
                        xmlmime.writeLine("<?xml version=\"1.0\"?>");
                        xmlmime.writeLine("<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>");
                        xmlmime.writeLine("\t<mime-type type=\"application/x-ham\">");
                        xmlmime.writeLine("\t\t<comment>Executable HAM File</comment>");
                        xmlmime.writeLine("\t\t<glob pattern=\"*.ham\"/>");
                        xmlmime.writeLine("\t</mime-type>");
                        xmlmime.writeLine("</mime-info>");
                        xmlmime.close();
                        system_call("xdg-mime uninstall \""+temp_directory+directory_separator+"HRE6-ham.xml\"");
                        system_call("xdg-icon-resource uninstall --context mimetypes --size 48 --mode user --novendor application-x-ham");
                        file_delete(temp_directory+directory_separator+"HRE6-ham.xml");
                        DisplayOnConsole("green","Removed mime type application/x-ham",true);

                        xmlmime.openFile(temp_directory+directory_separator+"HRE6-ha.xml",readFile__write);
                        xmlmime.writeLine("<?xml version=\"1.0\"?>");
                        xmlmime.writeLine("<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>");
                        xmlmime.writeLine("\t<mime-type type=\"application/x-ha\">");
                        xmlmime.writeLine("\t\t<comment>Executable HAM Archive</comment>");
                        xmlmime.writeLine("\t\t<glob pattern=\"*.ha\"/>");
                        xmlmime.writeLine("\t</mime-type>");
                        xmlmime.writeLine("</mime-info>");
                        xmlmime.close();
                        system_call("xdg-mime uninstall \""+temp_directory+directory_separator+"HRE6-ha.xml\"");
                        file_delete(temp_directory+directory_separator+"HRE6-ha.xml");
                        DisplayOnConsole("green","Removed mime type application/x-ha",true);

                        xmlmime.openFile(temp_directory+directory_separator+"HRE6-haminc.xml",readFile__write);
                        xmlmime.writeLine("<?xml version=\"1.0\"?>");
                        xmlmime.writeLine("<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>");
                        xmlmime.writeLine("\t<mime-type type=\"application/x-haminc\">");
                        xmlmime.writeLine("\t\t<comment>HAM Include Archive</comment>");
                        xmlmime.writeLine("\t\t<glob pattern=\"*.haminc\"/>");
                        xmlmime.writeLine("\t</mime-type>");
                        xmlmime.writeLine("</mime-info>");
                        xmlmime.close();
                        system_call("xdg-mime uninstall \""+temp_directory+directory_separator+"HRE6-haminc.xml\"");
                        file_delete(temp_directory+directory_separator+"HRE6-haminc.xml");
                        DisplayOnConsole("green","Removed mime type application/x-haminc",true);

                        system_call("rm ~/.local/share/applications/zaplots-"+HRE_version+".desktop");
                        system_call("xdg-icon-resource uninstall --context apps --size 64 zaplots-hamruntime-mainicon");
                        DisplayOnConsole("green","Removed launcher icon",true);

                        /*if (file_exists("/usr/local/bin/ham"))
                        {
                            system_call("sudo rm \"/usr/local/bin/ham\"");
                            DisplayOnConsole("green","Removed link in /usr/local/bin");
                        }*/
                        DisplayOnConsole("cyan","Updating alternatives for ham-runtime...");
                        system_call("sudo update-alternatives --remove ham-runtime \""+program_directory+"/"+HRE_executableName+"\"");
                    #endif
                #endif

                #ifdef _WIN32
                    readFile regfile;
                    regfile.openFile(temp_directory+directory_separator+"ham.reg",readFile__write);
                    regfile.writeLine("Windows Registry Editor Version 5.00");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\x-ha\\shell\\open\\command]");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\x-ha\\shell\\open]");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\x-ha\\shell]");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\x-ha\\DefaultIcon]");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\x-ha]");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\.ha]");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\x-ham\\shell\\open\\command]");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\x-ham\\shell\\open]");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\x-ham\\shell]");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\x-ham\\DefaultIcon]");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\x-ham]");
                    regfile.writeLine("");
                    regfile.writeLine("[-HKEY_CLASSES_ROOT\\.ham]");
                    regfile.close();
                    system_call("regedit \""+temp_directory+directory_separator+"ham.reg"+"\"");
                    file_delete(temp_directory+directory_separator+"ham.reg");
                    DisplayOnConsole("red","A restart for explorer.exe is required to complete the removal!");
                    system_call("taskkill /IM explorer.exe /F");
                    wait(100);
                    HWND hWnd = FindWindow(_T("Shell_TrayWnd"), NULL);
                    if(SUCCEEDED(hWnd))
                    {
                        PostMessage(hWnd, 0x5B4, 0, 0);
                        WinExec("C:\\Windows\\explorer.exe",SW_SHOWDEFAULT);
                        DisplayOnConsole("green","Restart complete!",true);
                    }
                    else
                    {
                        DisplayOnConsole("red","The restart failed :( sorry!",true);
                    }
                #endif
                file_delete(runtime_system+directory_separator+"firstrun");

                DisplayOnConsole("cyan","Remove all runtime libraries and settings? (this will also remove all data stored by programs) [y/N] ",false);
                string remove_ask("");
                cin >> remove_ask;
                if (StringUpper(remove_ask)=="Y")
                {
                    directory_delete_recursive(temp_directory);
                    directory_delete_recursive(program_storage);
                    directory_delete_recursive(runtime_system);
                    directory_delete_recursive(error_store);
                    directory_delete_recursive(lib_store);
                }
            }
            quit.exit(EXIT_SUCCESS);
        }
        if(InstallHam.isSet()){
            if (file_exists(InstallHam.getValue()))
            {
                if (install_ham(InstallHam.getValue())==1)
                {
                    quit.exit(EXIT_SUCCESS);
                }
                else
                {
                    quit.exit(EXIT_FAILURE);
                }
            }
            else
            {
                DisplayOnConsole("red","Error: '"+InstallHam.getValue()+"' does not exist!",true);
                quit.exit(EXIT_FAILURE);
            }
        }
        if(RemoveHam.isSet()){
            remove_ham(RemoveHam.getValue());
        }
        if(ListHam.isSet()){
            list_ham();
        }
        if(Setting.isSet()){
            limitArgs(Setting,2);
            string setting=Setting.getValue()[0];
            string value=Setting.getValue()[1];
            readConfig settings;
            if (!settings.setOpenFile(runtime_system+directory_separator+"Settings - "+KERNEL_TYPE+".txt","r"))
            {
                DisplayOnConsole("red","Error in function 'load_settings', the file could not be opened for reading!",true);
                quit.exit(EXIT_FAILURE);
            }
            if (setting=="AllowCleanup")
            {
                if ((value=="yes") || (value=="no"))
                {
                    settings.removePair(setting);
                    settings.addPair(setting,value);
                    DisplayOnConsole("green","Changed "+setting+" to "+value);
                }
                else
                {
                    DisplayOnConsole("red","Error: Unknown value '"+value+"'!",true);
                    quit.exit(EXIT_FAILURE);
                }
            }
            else if (setting=="ConsoleColour")
            {
                if ((value=="yes") || (value=="no"))
                {
                    settings.removePair(setting);
                    settings.addPair(setting,value);
                    DisplayOnConsole("green","Changed "+setting+" to "+value);
                }
                else
                {
                    DisplayOnConsole("red","Error: Unknown value '"+value+"'!",true);
                    quit.exit(EXIT_FAILURE);
                }
            }
            else if (setting=="FailPause")
            {
                if ((value=="yes") || (value=="no"))
                {
                    settings.removePair(setting);
                    settings.addPair(setting,value);
                    DisplayOnConsole("green","Changed "+setting+" to "+value);
                }
                else
                {
                    DisplayOnConsole("red","Error: Unknown value '"+value+"'!",true);
                    quit.exit(EXIT_FAILURE);
                }
            }
            else if (setting=="ShowDebug")
            {
                if ((value=="yes") || (value=="no"))
                {
                    settings.removePair(setting);
                    settings.addPair(setting,value);
                    DisplayOnConsole("green","Changed "+setting+" to "+value);
                }
                else
                {
                    DisplayOnConsole("red","Error: Unknown value '"+value+"'!",true);
                    quit.exit(EXIT_FAILURE);
                }
            }
            else if (setting=="ShowIncludeText")
            {
                if ((value=="yes") || (value=="no"))
                {
                    settings.removePair(setting);
                    settings.addPair(setting,value);
                    DisplayOnConsole("green","Changed "+setting+" to "+value);
                }
                else
                {
                    DisplayOnConsole("red","Error: Unknown value '"+value+"'!",true);
                    quit.exit(EXIT_FAILURE);
                }
            }
            else if (setting=="UseLocalTemp")
            {
                if ((value=="yes") || (value=="no"))
                {
                    settings.removePair(setting);
                    settings.addPair(setting,value);
                    DisplayOnConsole("green","Changed "+setting+" to "+value);
                }
                else
                {
                    DisplayOnConsole("red","Error: Unknown value '"+value+"'!",true);
                    quit.exit(EXIT_FAILURE);
                }
            }
            else if (setting=="VerboseIncludes")
            {
                if ((value=="yes") || (value=="no"))
                {
                    settings.removePair(setting);
                    settings.addPair(setting,value);
                    DisplayOnConsole("green","Changed "+setting+" to "+value);
                }
                else
                {
                    DisplayOnConsole("red","Error: Unknown value '"+value+"'!",true);
                    quit.exit(EXIT_FAILURE);
                }
            }
            else if (setting=="VerboseParsing")
            {
                if ((value=="yes") || (value=="no"))
                {
                    settings.removePair(setting);
                    settings.addPair(setting,value);
                    DisplayOnConsole("green","Changed "+setting+" to "+value);
                }
                else
                {
                    DisplayOnConsole("red","Error: Unknown value '"+value+"'!",true);
                    quit.exit(EXIT_FAILURE);
                }
            }
            else
            {
                print_settings();
                quit.exit(EXIT_FAILURE);
            }

            if (!settings.saveConfig())
            {
                DisplayOnConsole("red","Failed to save settings file!");
                quit.exit(EXIT_FAILURE);
            }
            settings.closeOpenFile();
            quit.exit(EXIT_SUCCESS);
        }
        if(ForceFix.isSet()){forcedFixes=ForceFix.getValue();}
        if (openFilename.isSet()){
            current_program = openFilename.getValue();
        }
        else
        {
            quit.exit(EXIT_SUCCESS);
        }
    } catch ( ArgException& e )
	{ cout << "ERROR: " << e.error() << " " << e.argId() << endl; quit.exit(EXIT_FAILURE); }
}

int loader_HA()
{
    // the file is a compiled ham archive

    // begin to read the file into a temp folder...
    if (show_debug){
    DisplayOnConsole("normal","Program found: "+current_program,true);
    DisplayOnConsole("purple","Extracting archive...",true);}

    // get an extract location...
    current_program_dir = CheckOK(temp_directory+directory_separator+"ham.archive."+ChangeFileExtention(GetFileName(current_program),""));
    DirectoryCreate(current_program_dir);
    if (show_debug){
    DisplayOnConsole("purple","Extracting to: "+current_program_dir,true);}

    readFile archive;
    archive.openFile(current_program,readFile__read);
    string archive_file_name;
    while(!archive.eof())
    {
        // read the name of the file...
        archive_file_name = archive.readLine();
        if (!archive.eof())
        {
            // output what was found...
            if (archive_file_name != "**Fopen_DELIM**")
            {
                if (archive_file_name != "**Fclose_DELIM**")
                {
                    if (archive_file_name == "**DIR_PACK_BEGIN**")
                    {
                        // found a directory
                        unpack_directory(archive, current_program_dir);
                    }
                    else
                    {
                        // found a file
                        // read the file data...
                        unpack_file(archive, current_program_dir+directory_separator+archive_file_name);
                        //cout << "File found in archive: '" << archive_file_name << "'" << endl;
                    }
                }
            }
        }
    }
    archive.close();
    if (show_debug){
    DisplayOnConsole("purple","Extract complete.",true);}
    prog_open=true;
    program_open_dir = GetPath(current_program_dir);

    if (file_exists(current_program_dir+directory_separator+"manifest"+directory_separator+"md5"))
    {
        if (show_debug){
        DisplayOnConsole("cyan","Checking md5 signitures...",true);}
        readFile md5_check;
        md5_check.openFile(current_program_dir+directory_separator+"manifest"+directory_separator+"md5",readFile__read);
        while(!md5_check.eof())
        {
            //cout << "loop" << endl;
            if (!md5_check.eof())
            {
                string md5_filename;
                md5_filename = md5_check.readLine();
                if (md5_filename == ""){break;}
                // look for file
                if ((md5_filename.at(0)=='D') && (md5_filename.at(1)=='I') && (md5_filename.at(2)=='R')
                && (md5_filename.at(3)==':') && md5_filename.at(4)==' ')
                {
                    // found what looks like a directory, check it isnt corrupted!
                    if (md5_filename.length() < 4)
                    {
                        DisplayOnConsole("red","There are missing characters on a directory definition!",true);
                        DisplayOnConsole("red","This could indicate corruption of the file.",true);
                        con_pause();
                        quit.exit(EXIT_FAILURE);
                    }

                    string md5_directoryname(md5_filename.replace(0,5,""));
                    md5_filename=md5_check.readLine();
                    //cout << md5_directoryname << " " << md5_filename;

                    if (file_exists(current_program_dir+directory_separator+md5_directoryname+directory_separator+md5_filename))
                    {
                        string md5_key;
                        md5_key = md5_check.readLine();
                        // make md5 of file to check against...
                        MD5 m;
                        readFile rf;
                        rf.openFile(current_program_dir+directory_separator+md5_directoryname+directory_separator+md5_filename,readFile__read);
                        while(!rf.eof())
                        {
                           string line=rf.readLine();
                           m.update(line);
                        }

                        m.finalize();
                        string md5_key2 = m.hexdigest();
                        if (md5_key != md5_key2)
                        {
                            DisplayOnConsole("red","Error: MD5 keys '"+md5_key+"' and '"+md5_key2+"' for file '"+md5_filename+"' do not match!",true);
                            DisplayOnConsole("red","This could mean that the archive is corrupt or has been modified!",true);
                            md5_check.close();
                            rf.close();
                            con_pause();
                            quit.exit(EXIT_FAILURE);
                        }
                        if (show_debug){
                        DisplayOnConsole("green","MD5 for '"+md5_filename+"' (in '"+md5_directoryname+"') is OK.",true);}
                    }
                    else
                    {
                        DisplayOnConsole("red","Error: MD5 for file '"+md5_filename+"' (in '"+md5_directoryname+"') found but the file does not exist in the archive!",true);
                        // skip over the md5 key
                        md5_check.readLine();
                    }
                }
                else
                {
                    if (file_exists(current_program_dir+directory_separator+md5_filename))
                    {
                        string md5_key;
                        md5_key = md5_check.readLine();
                        // make md5 of file to check against...
                        MD5 m;
                        readFile rf;
                        rf.openFile(current_program_dir+directory_separator+md5_filename,readFile__read);
                        while(!rf.eof())
                        {
                           string line=rf.readLine();
                           m.update(line);
                        }

                        m.finalize();
                        string md5_key2 = m.hexdigest();
                        if (md5_key != md5_key2)
                        {
                            DisplayOnConsole("red","Error: MD5 keys '"+md5_key+"' and '"+md5_key2+"' for file '"+md5_filename+"' do not match!",true);
                            DisplayOnConsole("red","This could mean that the archive is corrupt of has been modified!",true);
                            md5_check.close();
                            rf.close();
                            con_pause();
                            quit.exit(EXIT_FAILURE);
                        }
                        if (show_debug){
                        DisplayOnConsole("green","MD5 for '"+md5_filename+"' is OK.",true);}
                    }
                    else
                    {
                        DisplayOnConsole("red","Error: MD5 for file '"+md5_filename+"' found but the file does not exist in the archive!",true);
                        // skip over the md5 key
                        md5_check.readLine();
                    }
                }
            }
        }
    }

    if (show_debug){
    DisplayOnConsole("purple","Begining to load program...",true);}

    string entry_point("");
    bool has_manifest = false;
    bool has_manifest_entrypoint = false;
    #ifdef _WIN32
        string manifest_icon("");
    #endif

    // look for a manifest with the main file listed
    if (directory_exists(current_program_dir+directory_separator+"manifest"))
    {
        has_manifest = true;
        // there is a manifest, look for the entry point define

        if (file_exists(current_program_dir+directory_separator+"manifest"+directory_separator+"entrypoint"))
        {
            readFile mf;
            mf.openFile(current_program_dir+directory_separator+"manifest"+directory_separator+"entrypoint",readFile__read);
            entry_point = mf.readLine();
            mf.close();
            if (entry_point == "")
            {
                DisplayOnConsole("red","The entry point define in the manifest was blank!",true);
                DisplayOnConsole("red","Looking for main.ham instead.",true);
                has_manifest_entrypoint = false;
            }
            else if (file_exists(current_program_dir+directory_separator+entry_point))
            {
                if (show_debug){
                DisplayOnConsole("green","Entry point set to "+entry_point,true);}
                has_manifest_entrypoint = true;
            }
            else
            {
                DisplayOnConsole("red","The entry point "+entry_point+" does not exist in the archive!",true);
                DisplayOnConsole("red","Unable to load program.",true);
                return 0;
            }
        }
        else
        {
            has_manifest_entrypoint = false;
        }

        // look for a dependance file...
        if (file_exists(current_program_dir+directory_separator+"manifest"+directory_separator+"depends"))
        {
            if (show_debug){
            DisplayOnConsole("cyan","Found depends file, reading...",true);}
            // read all of the depends from the file and see if they exist in the lib_store folder
            readFile depends;
            depends.openFile(current_program_dir+directory_separator+"manifest"+directory_separator+"depends",readFile__read);
            while(!depends.eof())
            {
                if (!depends.eof())
                {
                    string checktemp;
                    checktemp = depends.readLine();

                    if (checktemp!="")
                    {
                        vector<string> dependCheck = parseLine(checktemp,0,"INTERNAL",false);
                        if (dependCheck.size()!=3){
                            DisplayOnConsole("red","Malformed dependance '"+checktemp+"'!");
                            quit.exit(EXIT_FAILURE);
                        }
                        // the dependance should read as: [package name] [operator] [version]
                        // ie: mypackage >= 1

                        if (file_exists(runtime_system+directory_separator+"catalogue"+directory_separator+dependCheck[0]+".catalogue"))
                        {
                            // get the version of the package...
                            readFile catalogue;
                            catalogue.openFile(runtime_system+directory_separator+"catalogue"+directory_separator+dependCheck[0]+".catalogue",readFile__read);
                            string catVerS = catalogue.readLine();
                            catalogue.close();

                            float catVer, depVer;
                            catVer = StringToFloat(catVerS);
                            depVer = StringToFloat(dependCheck[2]);

                            // check the operator and then return if the dependance is met or not...
                            if (dependCheck[1]=="==")
                            {
                                if (catVer!=depVer){
                                    DisplayOnConsole("red","Dependance for '"+checktemp+"' not met!");
                                    quit.exit(EXIT_FAILURE);
                                }
                            }
                            else if (dependCheck[1]=="!=")
                            {
                                if (catVer==depVer){
                                    DisplayOnConsole("red","Dependance for '"+checktemp+"' not met!");
                                    quit.exit(EXIT_FAILURE);
                                }
                            }
                            else if (dependCheck[1]==">=")
                            {
                                if (catVer>depVer){
                                    DisplayOnConsole("red","Dependance for '"+checktemp+"' not met!");
                                    quit.exit(EXIT_FAILURE);
                                }
                            }
                            else if (dependCheck[1]=="<=")
                            {
                                if (catVer<depVer){
                                    DisplayOnConsole("red","Dependance for '"+checktemp+"' not met!");
                                    quit.exit(EXIT_FAILURE);
                                }
                            }
                            else if (dependCheck[1]=="<")
                            {
                                if (catVer>=depVer){
                                    DisplayOnConsole("red","Dependance for '"+checktemp+"' not met!");
                                    quit.exit(EXIT_FAILURE);
                                }
                            }
                            else if (dependCheck[1]==">")
                            {
                                if (catVer<=depVer){
                                    DisplayOnConsole("red","Dependance for '"+checktemp+"' not met!");
                                    quit.exit(EXIT_FAILURE);
                                }
                            }
                        }
                        else
                        {
                            DisplayOnConsole("red","The package '"+checktemp+"' is not installed!");
                            quit.exit(EXIT_FAILURE);
                        }
                    }
                }
            }
            depends.close();
            if (show_debug){
            DisplayOnConsole("cyan","Complete",true);}
        }

        // look for author infomation
        if (file_exists(current_program_dir+directory_separator+"manifest"+directory_separator+"author"))
        {
            readFile author;
            author.openFile(current_program_dir+directory_separator+"manifest"+directory_separator+"author",readFile__read);
            manifest_author = author.readLine();
            author.close();
        }

        #ifdef _WIN32
            if (file_exists(current_program_dir+directory_separator+"manifest"+directory_separator+"icon"))
            {
                readFile icon;
                icon.openFile(current_program_dir+directory_separator+"manifest"+directory_separator+"icon",readFile__read);
                manifest_icon = icon.readLine();
                icon.close();

                if (file_exists(current_program_dir+directory_separator+manifest_icon))
                {
                    if (StringUpper(GetFileExtension(manifest_icon))=="ICO")
                    {
                        HWND h = ::GetTopWindow(0 );
                        DWORD pid=0;
                        DWORD mypid = _getpid();
                        //cout << mypid << endl;
                        while ( h )
                        {
                            //cout << pid << endl;
                            ::GetWindowThreadProcessId( h,&pid);
                            if ( pid == mypid )
                            {
                                //cout << "matched!" << endl;
                                HICON hIconSm;
                                hIconSm = (HICON)LoadImage(NULL, StringToChar(current_program_dir+directory_separator+manifest_icon), IMAGE_ICON,
                                32, 32, LR_LOADFROMFILE);
                                HANDLE processHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
                                SendMessage(h, WM_SETICON, ICON_SMALL, (LPARAM)hIconSm );
                                //cout << h << endl;
                                CloseHandle(processHandle);
                                break;
                            }
                            h = ::GetNextWindow( h , GW_HWNDNEXT);

                        }
                    }
                    else
                    {
                        DisplayOnConsole("yellow","[Notice] The icon is not a .ico file!",true);
                    }
                }
                else
                {
                    DisplayOnConsole("yellow","[Notice] The icon specified does not exist!",true);
                }
            }
        #endif
    }
    else
    {
        DisplayOnConsole("yellow","[Notice] No manifest file was found in the archive!",true);
    }

    if (!file_exists(current_program_dir+directory_separator+"main.ham") && (!has_manifest_entrypoint))
    {
        DisplayOnConsole("red","Failed to find main ham file for entry point in the archive!",true);
        DisplayOnConsole("red","Unable to load program.",true);
        DisplayOnConsole("red","Please include a main.ham file or a manifest in the archive.",true);
        return 0;
    }
    else if ((has_manifest) && (has_manifest_entrypoint) && (!file_exists(current_program_dir+directory_separator+entry_point)))
    {
        DisplayOnConsole("red","Failed to find '"+entry_point+"' for entry point in the archive!",true);
        DisplayOnConsole("red","Unable to load program.",true);
        return 0;
    }
    else
    {
        if (!has_manifest_entrypoint)
        {
            entry_point = "main.ham";
            if (show_debug){
            DisplayOnConsole("green","Entry point set to main.ham",true);}
        }
    }

    current_program_orig = current_program;
    current_program = current_program_dir+directory_separator+entry_point;
    return loader_ham();
}

int loader(int argc, char *argv[])
{
    if (argc < 2)
    {
        // no variable found!
        return -1;
    }
    else
    {
        if (show_debug){
        cout << (argc-1) << " variable(s) passed to the loader" << endl;}

        // a variable exists and will be dealt with
        string EXT("");
        EXT = GetFileExtension(current_program);
        string EXTU("");
        EXTU = StringUpper(EXT);
        //cout << "The extension of the file passed into argv[1] is: " << EXTU << endl;
        string Fname("");
        Fname = GetFileName(current_program);
        //cout << "The filename of the file passed into argv[1] is: " << Fname << endl;

        /*if (GetPath(argv[1])==""){
            char* pPath;
            pPath = getenv ("CD");
            if (pPath==NULL)
            {
                DisplayOnConsole("red","Failed to get current directory!");
                exit(EXIT_FAILURE);
            }

            argv[1]=StringToChar(CharToString(pPath)+CharToString(argv[1]));
            cout << "NEW!: " << argv[1] << endl;
        }*/

        if (!file_exists(current_program))
        {
            DisplayOnConsole("red","The file passed to the loader does not exist!",true);
            DisplayOnConsole("red",current_program+" does not exist!",true);
            return 0;
        }

        if (EXTU != "HAM")
        {
            if (EXTU != "HA")
            {
                if (EXTU != "HAMINC")
                {
                    return 2; // unloadable file type!
                }
                else
                {
                    if (install_ham(current_program)!=0)
                    {
                        return 3;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else
            {
                return loader_HA();
            }
        }
        else
        {
            // the file is a ham file
            if (show_debug){
            DisplayOnConsole("normal","Program found: "+current_program,true);
            DisplayOnConsole("purple","Begining to load program...",true);}
            current_program_dir = CheckOK(temp_directory+directory_separator+"ham."+ChangeFileExtention(GetFileName(current_program),""));
            DirectoryCreate(current_program_dir);

            current_program_orig = current_program;
            return loader_ham();
        }
    }
return 0;
}
