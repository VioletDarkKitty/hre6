#ifndef EXITHELPER_H
#define EXITHELPER_H
#include <vector>

using namespace std;
class exitHelper
{
    public:
        exitHelper();
        virtual ~exitHelper();
        int atexit(void (*function)(void));
        void call();
        void exit(int error);
        struct exit_exception {
            int c;
            exit_exception(int c):c(c) { }
        };
    protected:
    private:
        typedef void (*func_type)(void);
		std::vector<exitHelper::func_type> exitFunctions;
};

#endif // EXITHELPER_H
