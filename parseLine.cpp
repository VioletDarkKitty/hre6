#include <iostream>
#include "functions.h"
#include "globals.h"
#include <cstdlib>

void parse_error(string error, int line_number, string filename);

vector <string> parseLine(string line, int line_number, string filename, bool allowError=true)
{
  // Cleanup the input line before using...
  line=removeLeadingWhitespace(line);
  line=removeTrailingWhitespace(line);

  if (show_debug)
  {
    //cout << "ParseLine: Parsing line '" << line << "'" << endl;
  }

  // Break the line into a series of vectors
  vector<string> result;

  string origLine = line;
  size_t space=line.find_first_of(" ");

  // Only one item on line - a single command
  if(space==string::npos)
  {
    result.push_back(line);
    return result;
  }

  // Put the command as the first item
  result.push_back(line.substr(0,space));
  line.erase(0,space);

  while(line.length()>0){
  	// Need to remove any leading spaces
  	while((line.at(0)==' ')||(line.at(0)=='\t')) {
    	line.erase(0,1);
    }
    if(line.length()==0) break;

    if(line.at(0)=='"') {
        // A string
        //cout << "looking for string..." << endl;
        space=1;
        space=line.find_first_of("\"",space);
        while((space!=string::npos)){
            if (verbose_parse_output==true){
                DisplayOnConsole("normal","Line length is: "+IntToString(line.length()));
            }
            if (show_debug){cout << "Char is at " << space << endl;}
            if (space!=1)
            {
                if (line.length()>4)
                {
                    if (line.at(space-2)!='\\'){
                        if (show_debug){ cout << "string is not double escaped (\\)" << endl; }
                        if (line.at(space-1)!='\\'){
                            if (show_debug){ cout << "string is not single escaped\nnot a \"" << endl; }
                            break;
                        }
                        else
                        {
                            if (show_debug){cout << "Single escaped char!" << endl;}
                            space=line.find_first_of("\"",space+1);
                            if (space==string::npos)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        // scan for a new " character
                        unsigned int space_old = space;
                        space=line.find_first_of("\"",space);

                        if (space == space_old)
                        {
                            // there must not be another " on the line, return as it is...
                            break;
                        }
                    }
                }
                else
                {
                    space=line.find_first_of("\"",space);
                    break;
                }
            }
            else
            {
                break;
            }
        }
        //cout << "found at " << space << endl;
        if (space==string::npos){
            // unable to find a closing quote!
            if (allowError){parse_error("Error: Missing closing quote!",line_number,filename);}
        }
        result.push_back(line.substr(0,space+1));
        line.erase(0,space+1);
    } else if (line.at(0)=='(') {
        space=1;

        if (GetCount(line,")") > 1)
        {
            space=line.find_last_of(")");
        }
        else
        {
            space=line.find_first_of(")",space);
        }

        if (space==string::npos){
            // unable to find a closing bracket!
            if(allowError){parse_error("Error: Missing closing bracket!",line_number,filename);}
        }
        result.push_back(line.substr(0,space+1));
        line.erase(0,space+1);
    } else {
    	space=line.find_first_of(" ");
    	if(space==string::npos) {
    		// Last one on the line
    		result.push_back(line);
    		line.clear();
    	} else {
    		result.push_back(line.substr(0,space));
    		line.erase(0,space);
    	}
    }
  }

  for(unsigned int i=1;i!=result.size()-1;i++)
  {
    //if (show_debug){cout << "replacing \\\" in '" << result[i] << "' with \"..." << endl;}
    result[i]=replaceAll(result[i],"\\\"","\"");
  }
  return result;
}
