#!/bin/bash

function pause()
{
	echo ""
	read -p "Press enter to continue..." nothing
}

function getworkingdir()
{
	BINDIR=$(dirname "$(readlink -fn "$0")")
	cd "$BINDIR"
}

# begin
PWD=$(pwd)  # save the previous location of the terminal
getworkingdir

make -f "Ham Runtime HRE6.cbp.mak.mac"

# end
cd "$PWD"  # restore the previous location
